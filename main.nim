import noptics/olimat
import noptics/olioptics
import noptics/oliquantumoptics
import noptics/oligraphs
import noptics/oliplotting

import complex
import math

import sequtils

proc Nopticsmain() =

  block Sparse_testing:
    let phase = Phase(PI/2, 0)
    echo phase

    let bs = BS(0.5, 0, 1)
    echo bs

    let mzi = MZI(0.5, PI/2, 0, 1)
    echo mzi

    let loss = Loss(0.5, 0)
    echo loss

    let gain = Gain(2.0, 0)
    echo gain

    var state = vectorc(10)
    state[0] = complex(1.0)

    state.apply mzi
    echo state

    state.apply loss
    echo state

    state.apply gain
    echo state

  for i in 0 ..< 1000:
    block matrix_decomp:
      echo " matrix decomp"

      const dim = 10
      let m = randommatrix[float](dim, dim)

      var state = constantvector[float](dim, 1.0)
      echo "State in\n", state
      echo "m\n", m
      echo "m * state\n", m * state

      var output = optical_matrix(state, m)
      echo "output\n", output

  for i in 0 ..< 1000:
    block cmplxmatrix_decomp:
      echo " cmplx matrix decomp"

      const dim = 10
      let m = randommatrix[Complex64](dim, dim)

      var state = constantvector[Complex64](dim, complex(1.0))
      echo "State in\n", state
      echo "m\n", m
      echo "m * state\n", m * state

      var output = optical_matrix(state, m)
      echo "output\n", output

  for i in 0 ..< 1000:
    block unitary_matrix_decomp:
      echo " unitary matrix decomp"

      const dim = 10
      let m = randomunitary[Complex64](dim)

      var state = constantvector[Complex64](dim, 1.0.complex)
      echo "State in\n", state
      echo "m\n", m
      echo "m * state\n", m * state

      var output = optical_matrix(state, m)
      echo "output\n", output


when isMainModule:
  # Nopticsmain()
  let m = randomunitary[Complex64](8)
  echo perm(m)

  const dim = 3
  let fm = makematrix(dim, dim, proc(i, j: int): float = (i*dim+j).float)
  echo fm

  let modes = toseq(0..<dim)
  let cin = @[1, 1, 0]
  var inclicks = Clickpattern(modes: modes, photons: cin)
  echo inclicks

  let cout = @[0, 1, 1]
  var outclicks = Clickpattern(modes: modes, photons: cout)
  echo outclicks

  let repeated = fm.repeat_rows_cols(outclicks, inclicks)
  echo repeated

  echo perm(repeated)


  ## Time varying optical circuits 
  echo "Time varying circuits\n"

  const nummodes = 2
  
  const numsteps = 10
  for angle in 0 ..< numsteps + 1:
    var state = vector[Complex64](nummodes)
    state[0] = 1.0.complex
    var a = angle.float / numsteps.float
    echo "a ", a
    state.apply BS(a, 0, 1)
    echo "", state

  
  # Graphsmain()




