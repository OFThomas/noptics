# Package
version       = "0.2.3"
author        = "Oliver Thomas"
description   = "Nim Linear algebra, classical and quantum optics simulation package"
license       = "Apache-2.0"
srcDir        = "src"


# Dependencies
requires "nim >= 1.6.10"
requires "timeit"
requires "weave"
requires "gnuplot"
