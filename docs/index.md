# Welcome to _Noptics_ the Nim linear algebra and optics library!

## Motivation 

_Noptics_ is closely related to QGot, a C++ quantum Gaussian optics
library hosted [here](https://gitlab.com/dualityqp/qgot_public). It is used to simulate quantum optics, mostly using the continuous variable formalism. It is maintained by Duality Quantum Optics.

After having used C++ for a while the long, compile times, notably for heavy
templated code, has become extremely annoying. Nim offers a similar
performance to native C++ (within an order of magnitude, I hope) and it is
much easier to get an idea coded up.

## Noptics 

Is a library that sits on top of the Nim standard libraries and
with no other dependencies. It aims to compile quickly and be
reasonably performant! The code is hosted on gitlab
[here](https://gitlab.com/OFThomas/noptics).

### Library features

The library is composed of a few parts:
 
1. A pure Nim linear algebra library
2. Optics simulation
3. Quantum optics simulation
4. Graphs (vertices and edges kind)
5. Plotting support using Gnuplot
6. Signal processing/analysis 

## Install 

Clone the repo and run nimble install
```
git clone https://gitlab.com/OFThomas/noptics.git
cd noptics
nimble install 
```
You're ready to go, make sure to checkout the examples in `/examples`.

### Requirements 

You need to have gnuplot installed to use `plotting.nim` or
`signals.nim`.
On Ubuntu/debian
```
sudo apt-get install gnuplot-x11
```

### To run 
For a program called `prog`, 
```
nim c -d:danger prog
```

# Overview

- We use our own linear algebra library [linalg](linalg.md) to increase support for complex numbers, with no dependencies on other libraries. There is also a file of useful functions for combinatorics. 
- The classical optics library [optics](optics.md), which describes state transformations with different optical elements.
- The quantum optics library [quantumoptics](quantumoptics.md), with support for Gaussian and Fock states, and boson sampling. Provides the following types: click pattern objects (photon number resolving and threshold detectors), quantum states (Fock and Gaussian).
- Quantum information processing in the qubit picture, [qubits](qubits.md). 
- The graph library, [graphs](graphs.md), for the mathematical structure comprising vertices and edges. 
- Plotting using Gnuplot, and a signal processing library, mainly for modulator impulse responses, [signals](signals.md).

# General notes on Nim

For maximum performance use `-d:danger` to compile with the highest
optimization.

## Link time optimization 
Using `-d:lto` can speed programs up by a factor of 2.

Actually for `quantum.nim` and threshold detector click probabilities,
including or using `-d:lto` for the `std/complex` Nim system module improves
performace by a factor of 12! 

