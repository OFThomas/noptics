# Optics

The classical optics library which consists of:

1. Transformations 
    - Beamsplitters 
    - Phases
    - MZI (Mach-Zehnder interferometer)
    - Loss (attenuation)
    - Gain (amplification)
2. Use of Vectors to model optical field propogation
    - The apply proc for evolving under transformations 
3. Optical matrix multiplication
    - For modeling photonic matrix-vector accelerators 

# Transformations

These are the most basic transformation matrices, they return either a 1x1
or 2x2 (complex) or real valued matrix.
```
phasematrix[ScalarT](angle: ScalarT): Matrix[Complex[ScalarT]]
```
$$ 
m = \begin{pmatrix}
\mathrm e^{i \, \mathrm{angle}}
\end{pmatrix}
$$
```
bsmatrix[ScalarT](reflec: ScalarT): Matrix[Complex[ScalarT]]
```
$$ 
m = \begin{pmatrix}
\sqrt{1 - \mathrm{reflec}} & i\sqrt{\mathrm{reflec}} \\
i\sqrt{\mathrm{reflec}} & \sqrt{1 - \mathrm{reflec}} 
\end{pmatrix}
$$

```
mzimatrix[ScalarT](refec, phase: ScalarT): Matrix[Complex[ScalarT]]
```
$$ 
m = 
\begin{pmatrix}
1 & 0 \\
0 &\mathrm e^{i \, \mathrm{phase}}
\end{pmatrix}
\begin{pmatrix}
\sqrt{1 - \mathrm{reflec}} & i\sqrt{\mathrm{reflec}} \\
i\sqrt{\mathrm{reflec}} & \sqrt{1 - \mathrm{reflec}} 
\end{pmatrix}
$$


```
lossmatrix[ScalarT](loss: ScalarT): Matrix[ScalarT]
```
$$ 
m = \begin{pmatrix}
\sqrt{\mathrm{loss}}
\end{pmatrix}
$$

```
gainmatrix[ScalarT](gain: ScalarT): Matrix[ScalarT]
```
$$ 
m = \begin{pmatrix}
\sqrt{\mathrm{gain}}
\end{pmatrix}
$$

## Examples
```
echo complex(1.0)

echo Identity[float](2)
echo Identity[Complex32](2)
echo Identity[Complex64](2)

echo randommatrix[float](2)
echo randommatrix[Complex32](2)
echo randommatrix[Complex64](2)

echo matrix(2, 2, @[7.0, 5.0, 3.0, 1.0])

echo matrixc(2,2, @[4.0,4.0,6.0,6.0])
```
```
echo phasematrix(PI / 2)
echo bsmatrix(0.5)

echo bsmatrix(0.5) * bsmatrix(0.5).H

echo phasematrix(PI/2) * phasematrix(PI/2)
echo phasematrix(PI/2) * phasematrix(PI/2).H
```


# Sparse transformations 

Most of the time however you don't actually want just the 1x1 or 2x2
matrix, in order to efficiently update the state we provide versions of
each of the transformations which return a `SparseMatrix` object instead of
a `Matrix`.

```
Phase[ScalarT](angle: ScalarT, m1: int): SparseMatrix[Complex[ScalarT]]
```

```
BS[ScalarT](reflect: ScalarT, m1, m2: int): SparseMatrix[Complex[ScalarT]]
```

```
MZI[ScalarT](reflect, phase: ScalarT, m1, m2: int): SparseMatrix[Complex[ScalarT]]
```

```
Loss[ScalarT](loss: ScalarT, m1: int): SparseMatrix[ScalarT] 
```

```
Gain[ScalarT](gain: ScalarT, m1: int): SparseMatrix[ScalarT] 
```
## Examples
Sparse matrix tests
```
let phase = Phase(PI/2, 0)
echo phase

let bs = BS(0.5, 0, 1)
echo bs

let mzi = MZI(0.5, PI/2, 0, 1)
echo mzi

let loss = Loss(0.5, 0)
echo loss

let gain = Gain(2.0, 0)
echo gain
```


# The `apply` proc

After creating transformation matrices we normally apply the
transformations by acting on a vector. 

## Transformation examples
```
var state = makevector[Complex64](4)
echo state

state[0] = complex(1.0)
echo state, "\n"

state.apply phasematrix(PI/2), 0
echo state, "\n"

state.apply bsmatrix(0.5), 0, 1
echo state, "\n"

state.apply bsmatrix(0.5), 0, 3
echo state, "\n"

echo Identityc(1).oplus Identityc(1), "\n"

echo mzimatrix(0.5, PI/2), "\n"
```

mzi test:
```
# state = randomvector[Complex64](4)
# echo state, "\n"

# echo state.subblock(1,2), "\n"

var state = makevector[Complex64](1_000_000)
state[0] = complex(1.0)
# const maxt = 10_000_000
const maxt = 10

for i in 0 ..< maxt:
  state.apply mzimatrix(0.5, PI/2), i mod state.len, (i+1) mod state.len
  # state.apply bsmatrix(0.5), i mod state.len, (i+1) mod state.len

# so that the compiler doesn't optimize out the loop 
echo state[0]


state = vectorc(2)
state[0] = complex(1.0)

# or
state = vectorc(@[1.0, 0.0])
``` 

## Sparse transformation examples
```
var state = vectorc(10)
state[0] = complex(1.0)

state.apply mzi
echo state

state.apply loss
echo state

state.apply gain
echo state
```

# Optical matrix multiplication

The 
```
optical_matrix[ScalarT](state: Vector[ScalarT], mat: Matrix[ScalarT]): Vector[ScalarT]
```

## Examples 
### For a real matrix 
```
echo "\n Optical matmul"
var v = state
let (U, S, V) = mat.svd
assert mat ~ U * S * V.T
v = U * S * V.T * state

# echo "U\n", U
# echo "S\n", S
# echo "V.T\n", V.T

v *= U * S * V.T

# echo "State\n", v

var localstate = state

let vs = unitary_decomp(V.T)
var vident = Identity[ScalarT](V.rows)
for idx in 0 ..< vs.len:
    multiply_reduced_left(vs[idx], vident)
    localstate.apply vs[idx]

assert V.T ~ vident

local_state *= S

let us = unitary_decomp(U)
var uident = Identity[ScalarT](U.rows)
for idx in 0 ..< us.len:
    multiply_reduced_left(us[idx], uident)
    localstate.apply us[idx]

return localstate
```


