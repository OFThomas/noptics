# linalg.nim

There exist two linear algebra libraries already in Nim
[Arraymancer](https://github.com/mratsim/Arraymancer) and [Neo](https://github.com/andreaferretti/neo), however they both require linking
to Lapack/Blas and support for complex numbers i.e. Complex[float32] and
Complex[float64] is lacking or non-existent, hence linalg.nim was born.

Maybe in the future when I understand nim better I will port the novel
features as a pull request the respective libraries, but for now a pure Nim
linear algebra library with no dependencies is the way to go.

We use the *ROW MAJOR* storage order, because the library is primarily
designed for optics simulations which are many matrix * vector operations
so storing the matrix elements contiguously row majored is optimal.

The linear algebra library [linalg](linalg.md) is supported for `SomeFloat`
and `Complex[SomeFloat]` types (float32, float64, Complex32, Complex64):

1. Matrix and Vector types
    - Many different constructors for different varieties of matrices
2. Different access operators
    - `[row, col]` for individual element access
    - `row(idx)` and `col(idx)` for row/column access
    - `[seq[int], seq[int]]` and `[low .. high, low .. high]` or any mixture of slices and sequences for arbitrary row/col, more efficient to use slices for contiguous block access. You can repeat indices in sequences to repeat rows and columns in the new matrix
3. Arithmetic overloads for common matrix and vector operations
    - Norms
    - Inner products
    - Outer products
    - Direct sum
4. Matrix decompositions 
    - Givens rotations
    - QR decomposition
    - Schur decomposition
    - SVD (Singular Value Decomposition)
    - Matrix Determinant 
    - Unitary decomposition, U(n) into U(2) matrices


Consists of three `object` types where `T` is `SomeNumber` or
`Complex[SomeFloat]`:
```
Matrix[T]
```
```
SparseMatrix[T]
```
```
Vector[T] 
```
types which are basically just thin wrappers around a nim `seq[T]`.
These types are `object` types rather than `Ref` types, so they behave as
expected i.e `let A = B` creates a copy of B rather than binds B to A (this
is not the same as numpy for example).

# Matrix and Vector constructors 
There are a range of constructors for both objects, procs denoted by a
lower-case first letter of the corresponding type, i.e.

```
let m = matrix[float](rows, cols: int)
```
Which returns a `rows` by `cols` matrix initialised to zero in the
corresponding type, in this case `float`.
Similarly the `vector` constructor:
```
let v = vector[float32](size: int)
```
creates a zero initialised vector of type `float32`.

As Nim is strongly statically typed there are some annoyances dealing with
Complex numbers. In C++ you can interchangeably use 
```
double x = 1.0;
std::complex<double> c_x = 4.0;
```
Where `c_x` is equal to a complex number with real part 4.0 and imaginary
part 0.0. In Nim you *CANNOT* do this and everything must explicitly be
casted to a complex version using `complex(SomeFloat)`, e.g.

```
let c = complex(4.0)
```
Hence the helper functions to create a complex valued matrix/vector from a
real valued `seq`, so that you don't need to put `complex()` around every
single element in the sequence! Example:

```
let complex_m = matrixc(2, @[1.0, 2.0, 3.0, 4.0])
echo $complex_m

#Prints: [ (1.0, 0.0)   (2.0, 0.0) ]
#        [ (3.0, 0.0)   (4.0, 0.0) ]
```
Although for the zero initialised ctors you can pass the type as the
generic parameter i.e.
```
let cm = matrix[Complex[float64]](rows, cols: int)
```

The other constructor functions can initialise a constant valued
matrix/vector:
```
var m = constantmatrix[T](rows, cols: int, val: T) 
```
And the random valued matrix/vector 
```
var m = randommatrix[T](rows, cols: int, max: T = 1.0)
```
Which by default uses random numbers in the range of `[0, 1.0]` but by
passing an optional value for `max` you can change the distribution to
between `[0, max]`.

## Examples

```python
echo Identity[float](2)
echo Identity[Complex32](2)
echo Identity[Complex64](2)

echo randommatrix[float](2)
echo randommatrix[Complex32](2)
echo randommatrix[Complex64](2)

let fm = makematrix(3, 3, proc(i, j: int): float = (i*3+j.float))
echo fm
#Prints [ 0.0   1.0    2.0 ]
#       [ 3.0   4.0    5.0 ]
#       [ 6.0   7.0    8.0 ]
```

# Accessing elements of matrices/vectors 

## Individual elements
The `[]` operator is provided for both types:
```
# The identity matrix,
let m = matrix(2,2, @[1.0, 0.0, 0.0, 1.0])
# m[row, col]
assert m[0,0] == 1.0
assert m[0,1] == 0.0
assert m[1,0] == 0.0
assert m[1,1] == 1.0
``` 
The same is true for vectors, although there is only one index for a vector.

## Row and Column operations
Often it's useful to access a row or a column of a matrix, the following
procs are provided: 
```python
let m = Identity[float](2) # m = @[1.0, 0.0, 0.0, 1.0]
# NOTE IN THIS VERSION OF THE LIBRARY .row returns a VECTOR
var row0: Vector[T] = m.row(0)
var row1: Vector[T] = m.row(1)
assert row0.data == @[1.0, 0.0]
assert row1.data == @[0.0, 1.0]
```
There is also a `col(idx: int)` proc which returns a column of a Matrix as
a Vector.

If you want to select multiple rows or columns there are corresponding procs:
```python
let m = Identity[float](3)
# m = @[
# 1.0, 0.0, 0.0,
# 0.0, 1.0, 0.0,
# 0.0, 0.0, 1.0
# ]
# NOTE IN THIS VERSION OF THE LIBRARY m[] returns a MATRIX object 
# subrows takes an openArray[int] which is an alias for either a seq[int]
# or Array[size, int]. You probably will use the seq[int] as an Array is
# compile time so you wont need to worry about this distinction.
var rows01 = m[[0,1], 0..<m.cols)
# row01 == matrix(2,3, @[
# 1.0, 0.0, 0.0, 
# 0.0, 1.0, 0.0
# ]
```
Selecting a subset of rows is very useful for sparse matrix multiplication, see
`multiply_reduced_left`.

Selecting subcolumns can be performed simillarly using 
```
let subcols = m[0..<m.rows, [0,2]] 
```
and returns a set of columns over all the rows in the matrix.

## Accessing subblocks of a matrix 

The `[]` operator for matrices can also be used to return sub blocks, or
sub sections of a matrix.
```python
let m = Identity[float](3)
# m = @[
# 1.0, 0.0, 0.0,
# 0.0, 1.0, 0.0,
# 0.0, 0.0, 1.0
# ]
# rows 0 to 1 or can use [0..<2] or [0..1]
var upperleft = m[0..<2, 0..<2]
# row01 == matrix(2,2, @[
# 1.0, 0.0, 
# 0.0, 1.0, 
# ]
```

Lastly on the subject of access, we don't need to take a contiguous block,
you can mix slices or sequences in the `[]` operator. For example, we return a
block of the matrix but it does not have to be from a block on the diagonal
like above,
```python
let m = matrix[float](2,2,@[
  1.0, 0.0, 0.0,
  0.0, 2.0, 0.0,
  0.0, 0.0, 3.0
  ])
# calling syntax is seq[rows], seq[cols] 
var upperleft_botright = m[[0,2], [0,2]]
# means keep elements from (0,0), (0,2), (2,0) and (2,2)
# row01 == matrix(2,2, @[
# 1.0, 0.0, 
# 0.0, 3.0, 
# ]
```

and we can mix the argument types, 
```python
let m = matrix[float](2,2,@[
  1.0, 0.0, 0.0,
  0.0, 2.0, 0.0,
  0.0, 0.0, 3.0
  ])
# calling syntax is seq[rows], seq[cols] 
var upperleft_botright = m[[0,2], 1..2]
# means keep elements from (0,1), (0,2), (2,1) and (2,2)
# row01 == matrix(2,2, @[
# 0.0, 0.0, 
# 0.0, 3.0, 
# ]
```

Sometimes it is also useful to repeat rows or columns:

```python
let v = vector[int](@[3, 4, 5, 6])
let elements = @[1, 1, 0, 2]
echo v[elements]

# Prints [ 4 ]
#        [ 4 ]
#        [ 3 ]
#        [ 5 ]
```

# Operations on Matrices

To transpose a matrix (swap its rows and columns) you can use the `T` proc.
**Note this copies the matrix and returns a new matrix** leaving the original
matrix unchanged:
```python
let m = randommatrix[float](3,4)
let mtranspose = m.T # or m.T()  
```
If you wish to modify the matrix directly and not return a copy you can use
the `Tinplace` proc.
```python
var m = randommatrix[float](5,6)
echo m
m.Tinplace()
echo m 
```
Note that as `Tinplace` modifies the matrix we have to declare it as a
`var`.

For complex matrices the adjoint or Hermitian conjugate which is transpose
and complex conjugation of the elements of the matrix, is provided using
the `H` and `Hinplace` procs as for real valued matrices.
```python
var m = randommatrix[Complex64](5,6)
# does not modify m
echo m.H()

# modifies m
m.Hinplace()
echo m 
```
And the complex conjugate only as `conj`
```python
var m = randommatrix[Complex64](5,6)
# does not modify m
echo m.conj()
```

There are also procs to calculate the trace of the matrix, the matrix raised to a power,
and the matrix with each element replaced by its absolute value.

The `chop` proc replaces all terms smaller than 1e-8 with 0.

## Matrix properties 

There are several functions to check that matrix properties:

- `is_diagonal`: all off-diagonal elements are zero.
- `is_upper_diagonal`: also called upper triangular: all elements below the diagonal are zero. 
- `is_orthogonal`: the matrix multiplied with its transpose is the identity.
- `is_unitary`: the matrix multiplied with its Hermitian conjugate is the identity.

These return a bool, and compare within a small margin of error.

## Arithmetic

We provide overloads for most of the common matrix, vector operations such
as addition, multiplication etc.

Notable mentions are the `oplus` proc which is the direct sum of two matrices,
and `kronecker_prod` proc which gives the tensor product of two matrices.
```python
let m1 = matrix[float](2, @[3.0, 4.0, 5.0, 6.0]) 
let m2 = matrix[float](3, @[0.0, 1.0, 2.0, 3.0, 4.0, 
                            5.0, 6.0, 7.0, 8.0]) 
echo oplus(m1, m2)
# [ 3.0    4.0	   0.0	   0.0	   0.0 ]
# [ 5.0    6.0	   0.0	   0.0	   0.0 ]
# [ 0.0    0.0	   0.0	   1.0	   2.0 ]
# [ 0.0	   0.0	   3.0	   4.0	   5.0 ]
# [ 0.0	   0.0	   6.0	   7.0	   8.0 ]

echo kronecker_prod(m1, m2)
# [ 0.0	    3.0	    6.0	    0.0	    4.0	    8.0 ]
# [ 9.0	    12.0	15.0	12.0	16.0	20.0 ]
# [ 18.0	21.0	24.0	24.0	28.0	32.0 ]
# [ 0.0	    5.0	    10.0	0.0	    6.0	    12.0 ]
# [ 15.0	20.0	25.0	18.0	24.0	30.0 ]
# [ 30.0	35.0	40.0	36.0	42.0	48.0 ]
 
```

# SparseMatrix type

We introduce a new matrix type to speed up matrix functions where not all modes are involved:

```python
Sparsematrix*[T] = object
  matrix*: Matrix[T]
  modes*: seq[int]
```

As well as the matrix, this stores the set of _modes_ that the matrix acts on. This comes from
terminology that will be useful in quantum optics usage, but essentially refers to the rows/columns
that will be used when the matrix is applied.

For example, note:

```python
let mat = randommatrix[float](2)
# Call this [a, b]
#           [c, d]
let sparse_mat = sparsematrix[float](mat, @[0, 1])
# The sparse matrix is the above matrix acting on modes 0 and 1
# It may be embedded in a larger vector space but we are only interested
# in the first 2 dimensions, e.g.:
let m1 = oplus(mat, matrix(1, @[1.0]))
# [a, b, 0]
# [c, d, 0]
# [0, 0, 1]
# This should be equivalent to our sparse matrix on a 3 dimensional system
var m2 = randommatrix[float](3)

let mout = m1*m2
multiply_reduced_left(sparse_mat, m2)
# Note that we multiply in place.

assert mout == m2
# Check that multiplying using the sparse matrix is the same as
# using the 3D matrix.
```


# Matrix decompositions

## Givens rotations 

*Note:* potential numerical errors when implementing with complex numbers.

This is a subroutine of the QR decomposition (see below). It performs
a rotation (in 2D) by some angle such that the bottom element of the rotated
vector is zero.
```
proc givens_rot[T](a, b: T): Matrix[T]
```

Following the notes [here](https://www.math.usm.edu/lambers/mat610/sum10/lecture9.pdf),
this proc takes the variables a11 and a21 (the first column of A)
and returns the matrix Q.

## QR decomposition
Returns (Q, R) such that m = Q * R where, for real numbers, Q is orthogonal 
Q * Q^T = 1, or for complex numbers, Q is unitary: Q * Q.H = 1.
R is upper diagonal, and we force the diagonal of R to be positive 
(and real for complex matrices).
```
proc QR[T](mat: Matrix[T]): (Matrix[T], Matrix[T])
```

Example:
```
var m57 = randommatrix[float](2,2)
m57 = matrix(2,2, @[1.0, 2.0, 3.0, 4.0])
echo "m57\n", m57

var (Q, R) = QR(m57)
echo "Q\n", Q
echo "R\n", R

echo "m57\n", m57
echo "Q * R\n", Q * R

assert m57 ~ Q*R
```

## Schur decomposition

Returns (U, A) such that m = U * A * U.T for real numbers where U is
orthogonal or m = U * A * U.H for complex numbers where U is unitary.
Uses the QR decomposition as a subroutine.
```
proc Schur[T](mat: Matrix[T], max: int = 100): (Matrix[T], Matrix[T])
```

## SVD (Singular value decomposition)
Returns (U, S, V) such that m = U * S * V.T for real numbers where U and V
are orthogonal or m = U * S * V.H for complex numbers where U and V are
then unitary. S are the singular values of the matrix m, S is diagonal with
positive real valued entries *always*.
```
proc svd[T](mat: Matrix[T]): (Matrix[T], Matrix[T], Matrix[T])
```

SVD
```python
var u = random_unitary[Complex64](2)
u = u + u.H
let (U, S, V) = svd(u)

echo "U\n", U
echo "S\n", S
echo "V\n", V

assert u ~ U*S*V.H

let (Q, R) = u.QR
echo "Q\n", Q
echo "R\n", R

assert u ~ Q*R

let (A, T) = u.Schur
echo "A\n", A
echo "T\n", T

assert u ~ A * T * A.H
```

## Unitary decomposition, U(n) into U(2) matrices

Unitary decomp
```python
let u = random_unitary[Complex64](2)
echo "u\n", u

let (gs, modes) = unitary_decomp(u)

var cident = Identityc(u.rows)
for idx in 0 ..< gs.len:
  # echo "gs idx\n", gs[idx], ", ", modes[idx], "\n"
  cident = multiply_reduced_left(gs[idx], cident, modes[idx][0], modes[idx][1])
  # echo "\n cident\n", cident
echo "cident\n", cident
assert u ~ cident
```

```python
let u = random_unitary[Complex64](2)
let (Q, R) = u.QR
# echo "Q\n", Q
# echo "R\n", R
# echo "Q QH\n", Q * Q.H
echo "Q is unitary ", Q.is_unitary

echo "Q * R\n", Q*R
assert u ~ Q*R

var rm = randommatrix[float](2,2)
rm += rm.T

# let rm = matrix(2, @[1.0, 0.0, 0.0, 1.0])
# let rm = matrixc(2, @[0.0, 1.0, 1.0, 0.0])
let (Uschur, Tschur) = rm.Schur

echo "rm\n", rm
echo "Uschur\n", Uschur
echo "Tschur\n", Tschur

echo "Uschur * T * Uschur\n", Uschur * Tschur * Uschur.T

assert rm ~ Uschur * Tschur * Uschur.T
```

# Matrix functions
- `sum` and `l1norm` are equivalent and sum the vector elements.
- `l2norm` sums the square of the vector (or matrix) elements.

## Eigenvectors and eigenvalues

The Jacobi method is used to get eigenvectors and eigenvalues.

Example:

```python
let m = randomunitary[Complex64](3)
echo eigenvalues(m)
let eigvects, eigvals = JacobiEigenDecomp(m)
echo eigvects
```

The Jacobi method is also used for matrix inversion, with the proc `inverse`.

## Determinant

There are a number of determinant procs, the fastest at the moment is 
```
chol_ldl_crout_determinant[T](mat: Matrix[Complex[T]]): T
```
which uses the Cholesky decomposition.

And a general method which will work for any type of matrix, but is slower
is
```
determinant[T](mat: Matrix[T]): T
```
which uses the `QR` decomposition.

## Hafnian

There are several methods implemented for the [hafnian](https://en.wikipedia.org/wiki/Hafnian) and loop hafnian.
- `hafnian_slow`: a naive algorithm
- `hafnian`: a recursive algorithm
- `hafnian_fast`: a more efficient algorithm, based on the one implemented in the Walrus. *Note*: this may not necessarily be the fastest algorithm for smaller matrices.

