# Quantum optics 
The quantum optics library [oliquantumoptics](quantumoptics.md):

1. Gaussian states 
    - Gaussian Boson Sampling
2. Fock states
    - Boson Sampling 

# Types 

## Click pattern objects 

In order to minimise confusion we provide a `ClickPattern` type which
consists of a sequence of (optical) modes and a sequence of photon numbers
in each of the corresponding modes. This is slightly verbose but it
eliminates any potential confusion over how many photons we wish to detect
in each modw, particularly when considering measurements on a subset of the
modes.
```python
type ClickPattern = object
    modes: seq[int]
    photons: seq[int]
```
You might use `ClickPattern` objects like so:
```
var clicks = clickpattern(4)
clicks.modes == [0, 1, 2, 3]
clicks.photons == [0, 0, 0, 0]

# sets the first 2 photons to the values of 3
clicks.setFirst(2, 3)
clicks.photons == [3, 3, 0, 0]

# as 3 + 3 + 0 + 0 = 6
clicks.numClicks == 6 

# the previous permutation of the photons over the modes 
clicks.prev
clicks.photons == [3, 0, 3, 0]

# as there are 3 photons in mode 0 and 3 photons in mode 2
clicks.getRepeatedModes == [0, 0, 0, 2, 2, 2]
```
There are some functions which query or modify `ClickPatterns`, 
```
setFirst(cp: var ClickPattern, n, val: int)
numClicks(cp: ClickPattern): int

next(cp: var ClickPattern): bool
prev(cp: var ClickPattern): bool

getRepeatedModes(cp: ClickPattern): seq[int]

getRepeatedRowsCols(rowPhotons, colPhotons: ClickPattern): (seq[int], seq[int])
repeatRowCols(m: Matrix, rowPhotons, colPhotons: ClickPattern): Matrix
```

## Threshold detector click patterns

In the current version (0.2) the threshold detector click pattern type is
different to the ClickPattern type, it does **not** contain the modes we
are considering as an optimization step. Also currently threshold detector
click patterns are limited to a maximum of 64 clicks as we pack each click
value into a bit of a 64bit integer. This is likely to change in future
versions to match or be replaced by the `ClickPattern` type.
    
```
type Threshold = object
    clicks: uint64
```

## Quantum states 
In the `quantumoptics.nim` module there are two types of quantum states:
```
type 
    FockState[T] = object
        dim: int
        input, output: ClickPattern
        matrix: Matrix[Complex[T]]

    GaussianState[T] = object
        dim: int
        covaraince: Matrix[Complex[T]]
        displacement: Vector[Complex[T]]
```

# Gaussian states

Gaussian states have a covariance matrix (complex valued) and a
displacement vector (complex valued).

## Constructors 

There is one constructor which takes a dimension and returns a
`GaussianState` object corresponding to the vacuum quantum state (i.e. no
light is present).

```
proc gaussianstate[T: SomeFloat](dim: int): GaussianState[T]
```

## Operators 

All of the classical optics operators are avaliable to be used on quantum
states, see [olioptics#sparse-transformations](/noptics/optics/#sparse-transformations). In addition we provide, 

```
smsqmatrix[T: SomeFloat](sqparam: T): Matrix[Complex[T]] 
```
$$
m = \begin{pmatrix}
\cosh(\mathrm{sqparam}) & \sinh(\mathrm{sqparam}) \\
\sinh(\mathrm{sqparam}) & \cosh(\mathrm{sqparam}) 
\end{pmatrix}
$$

```
tmsqmatrix[T: SomeFloat](sqparam: T): Matrix[Complex[T]] 
```
$$
m = \begin{pmatrix}
\cosh(\mathrm{sqparam}) & 0 & 0 & \sinh(\mathrm{sqparam}) \\
0 & \cosh(\mathrm{sqparam}) & \sinh(\mathrm{sqparam}) & 0 \\
0 & \sinh(\mathrm{sqparam}) & \cosh(\mathrm{sqparam}) & 0\\
\sinh(\mathrm{sqparam}) & 0 & 0 & \cosh(\mathrm{sqparam}) 
\end{pmatrix}
$$

### The operator form is 

To create a single mode squeezer pass a single (one) mode
```
squeezer[T: SomeFloat](sqparam: T, m: int): SparseMatrix[Complex[T]]
```

To create a two mode squeezer pass two modes
```
squeezer[T: SomeFloat](sqparam: T, m1, m2: int): SparseMatrix[Complex[T]]
```


## Measurements

There are three measurements on Gaussian states:
1. Threshold detector measurements
2. Photon number resolved measurements (PNR)
3. Correlation functions, number operator expectation values

The vacuum probability of a state is calculated using
```
vacuum_prob[T](state: GaussianState[T]): T
```
$$
p_\mathrm{vac}(\sigma_K) = \det(0.5 \sigma_K + 0.5)^{-1/2}
$$

### Threshold detectors 

The probability of a particular threshold detector click pattern is
```
threshold[T: SomeFloat](state: GaussianState[T], cp: Threshold): T
```
$$
p(\sigma,\mathrm{cp}) = \sum_{K \in 2^S} (-1)^{|K|} p_\mathrm{vac}(\sigma_K)
$$

For larger systems it is beneficial to run some of the measurements in parallel, using the associated suffix `_parallel` uses the parallelized version (if it exists).
```
threshold_parallel[T: SomeFloat](state: GaussianState[T], cp: Threshold): T
```
### Photon number resolved measurements 

use the `pnr` function which takes a `GaussianState` and a `ClickPattern`:
```
pnr[T: SomeFloat](state: GaussianState[T], cp: ClickPattern): Complex[T]
```

### Correlation functions and photon number expectation values
TODO


## Examples

```
const dim = 4
# creates the vacuum state in dim number of optical modes 
var state = gaussianstate[float](dim)

# apply single mode squeezers of squeezing parameter 1.0 to 
# each of the modes 
for i in 0 ..< state.dim:
    state.apply squeezer(1.0, i)

# vaccumprob returns a float 
let vacprob: float = state.vaccumProb

# make an empty threshold click pattern to use for measurements 
var thres: Threshold
# set the modes 0 to state.dim -1 to be clicks 
# this makes the [1, 1, 1, ..., 1] (all ones) click pattern
thres.clicks.setmask(0 ..< state.dim)

# get the probability of measuring the click pattern 
let thresholdprob: float = state.threshold(thres)


# We can also do photon number resolved probabilities 
# makes the zero photon number pattern over modes 0 to state.dim-1
var cp = clickpattern(dim)
# set the first mode to contain 2 photons 
cp.photons[0] = 2

let pnrprob: float = state.pnr(cp)
```



# Fock states

Bosonic states which use single photon inputs are used in the KLM scheme
for quantum computing using photonics or nearer term in Boson Sampling. 

## Constructors 

There is a single constructor which creates a `FockState` of size `dim`
modes and no photons are present:
```
fockstate[T](dim: int): FockState[T]
```

## Operators

Fock state objects have an `apply` function which takes a state and a
SparseMatrix which is the unitary transformation along with the modes it
acts on.
```
apply[T: SomeFloat](state: var FockState[T], mat: SparseMatrix[Complex[T]])
```

## Measurements

To calculate photon number resolved probablities you can use:
```
prob[T: SomeFloat](state: FockState[T], outclicks: ClickPattern): T
```
which takes a state (with the input ClickPattern set) and the output
click pattern we wish to calculate the probability of.


## Examples
```
const dim = 4
var fs = fockstate[float](dim)
# 50:50 beamsplitter on modes 0 and 1
fs.apply BS(0.5, 0, 1)
# phase on mode 1
fs.apply Phase(Pi/4, 1)
# MZI which is a beamsplitter (50:50 here) 
# then a phase (Pi/4 here, on the second mode!)
# the transformation acts on modes 0 and 1
fs.apply MZI(0.5, Pi/4, 0, 1)

# measurements 
# inject single photons into modes 1 and 3
fs.input.photons = @[0,1,0,1]

# the probability of the photons coming out of the same modes
# that we put them into
var outclicks: ClickPattern = fs.input
let pidentity = fs.prob(outclicks)

# or the prob of [1,1,0,0]
outclicks.photons = @[1,1,0,0]
echo fs.prob(outclicks)
# can pass just a sequence if the click pattern is over all of the 
# modes rather than making a ClickPattern object
echo fs.prob(@[1,1,0,0])

```

See `test_bosonsampling.nim`.


