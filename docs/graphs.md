# graphs.nim

Graphs are mathematical structures that consist of a 
set of vertices and a set of edges connecting them.
Graphs can be defined by an adjacency matrix - for an
undirected, unweighted graph with n vertices, this is a 
square n by n matrix with a 1 in the (i,j)th element if
vertices i and j are connected by an edge, and 0 otherwise.

## Types

The graph has a symmetric adjacency matrix, `adj`.

```
type
    Graph = object
        adj: Matrix[float]
```

## Operations on graphs

You can use the `randomgraph` proc to make a random graph.
This takes an `int`, the total number of vertices, and a 
`float`, the desired density. This uses the Erdos-Renyi method 
for creating random graphs - that is, each possible edge is 
inserted with a probability equal to the desired density.

The `subgraph` proc returns graph associated with some subset 
of the vertices of a given graph, and the edges between these 
vertices.

You can use the `getdegree` proc to find the degree of a vertex, 
which is the number of edges incident to that vertex.

The `getvertices` proc returns the vertices of a given graph.
