# See the docs
You can find the latest docs at [https://ofthomas.gitlab.io/noptics](https://ofthomas.gitlab.io/noptics).

# To install Noptics

Clone the repo and run nimble install 
``` 
git clone https://gitlab.com/OFThomas/noptics.git
cd noptics
nimble install 
```
That's it! You're ready to go, see some of the examples :).

### Nim install (unix)
```
curl https://nim-lang.org/choosenim/init.sh -sSf | sh
```

Add to path add your `~/.bashrc`
```
export PATH=~/.nimble/bin:$PATH
```

### To use nim

To compile to c (using c compiler):
```
nim c -r test.nim
```

To compile to c++ (using c++ compiler):
```
nim cpp -r test.nim
```

## Nim setup for vim 

```
https://github.com/BitR/ycm-nimsuggest
```

# Requires

```
nimble install timeit
nimble install weave
nimble install gnuplot
```
gnuplot i.e.
```
sudo apt-get install gnuplot-x11
```

# build options 

For Maximum Speed TM use
```
nim c -d:danger -d:lto quantum.nim
```

For debugging
```
nim c -d:danger --debugger:native --passC:-flto --passL:-flto --mm:orc quantum.nim
```

Profiling,
```
valgrind --tool=callgrind --dump-instr=yes --collect-jumps=yes ./quantum
```

Single thread 24clicks takes 60s with half_modes_gray_split_for 

Multithreaded 24clicks takes 11s with  double_prec_all_clicks_parallel(dim)
Multithreaded 26clicks takes 60s with  double_prec_all_clicks_parallel(dim)
