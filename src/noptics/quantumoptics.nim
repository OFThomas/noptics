import noptics/linalg
import noptics/combinatorics

# import noptics/optics
import noptics/graphs

import complex
import algorithm
import sequtils
import math
import bitops

import std/random

import timeit

import weave


type
  ClickPattern* = object
    modes*, photons*: seq[int]

type
  ClickPatternGroup* = object
    uniqueCPs*: seq[ClickPattern]
    tallies*: seq[int]

proc clickpattern*(dim: int): ClickPattern =
  ClickPattern(modes: toseq[0 ..< dim], photons: newSeq[int](dim))

proc clickpattern*(photons: openArray[int]): ClickPattern =
  ClickPattern(modes: toSeq[0..<photons.len], photons: photons.toSeq)

proc numClicks*(cp: ClickPattern): int {.inline.} =
  result = cp.photons.foldl(a + b)

type Threshold* = object
  clicks*: uint64

proc newThreshold*[T: openArray[int]](clicks: T): Threshold {.inline.} =
  for idx, val in clicks:
    if val > 0:
      result.clicks = result.clicks or (1'u64 shl idx)

proc numClicks*(thres: Threshold): int {.inline.} = 
  thres.clicks.countSetBits

proc next*(thres: var Threshold): bool {.inline.} = 
  if thres.clicks < uint64.high:
    thres.clicks += 1
    return true
  return false

proc prev*(thres: var Threshold): bool {.inline.} = 
  if thres.clicks >= uint64.low + 1:
    thres.clicks -= 1
    return true
  return false

proc getSetModes*(thres: Threshold): seq[int] = 
  result = newSeq[int](thres.numClicks)
  var i = 0
  var idx = 0
  while idx < result.len:
    if (thres.clicks and (1'u64 shl i)) != 0:
      result[idx] = i
      idx += 1
    i += 1

proc setFirst*(cp: var ClickPattern, n: int, val: int) =
  for i in 0 ..< n:
    cp.photons[i] = val

proc next*(cp: var ClickPattern): bool {.inline.} =
  cp.photons.nextPermutation

proc prev*(cp: var ClickPattern): bool {.inline.} =
  cp.photons.prevPermutation

proc getRepeatedModes*(cp: ClickPattern): seq[int] = 
  let numterms = cp.numclicks
  result = newSeqOfCap[int](numterms)
  for idx in 0 ..< cp.modes.len:
    for num_ps in 0 ..< cp.photons[idx]:
      result.add cp.modes[idx]

proc getRepeatedRowsCols*(rowPhotons, colPhotons: ClickPattern): (seq[int], seq[int]) =
  let ridx = rowPhotons.getRepeatedModes
  let cidx = colPhotons.getRepeatedModes

  return (ridx, cidx)

proc repeatRowsCols*[T: SomeNumber | Complex[SomeFloat]](m: Matrix[T], rowPhotons, colPhotons: ClickPattern): Matrix[T] =
  let ridx = rowPhotons.getRepeatedModes
  let cidx = colPhotons.getRepeatedModes

  result = m[ridx, cidx]

type FockState*[T] = object
  dim*: int
  input*: ClickPattern
  matrix*: Matrix[Complex[T]]

proc fockstate*[T: SomeNumber | Complex[SomeFloat]](dim: int): FockState[T] = 
  result = FockState[T](dim: dim,
  input: clickPattern(dim),
  matrix: Identity[Complex[T]](dim, dim)
  )

proc apply*[T: SomeFloat](state: var FockState, mat: SparseMatrix[Complex[T]]) = 
  # u = BS
  # Symplectic version is U \oplus U^*
  if mat.matrix.rows == mat.modes.len:
    state.matrix.multiply_sparse_left(mat)
  else:
    echo "ERR CANNOT IMPLEMENT THAT TRANSFORMATION ON FOCK STATES"
    quit(1)

# single threaded probabilities 
proc prob_single*[T: SomeFloat](state: FockState[T], outclicks: ClickPattern): T = 
  let usub = state.matrix.repeatRowsCols(outclicks, state.input)
  let p = perm(usub)

  var factor = 1.0.T
  for i in outclicks.photons:
    factor *= (1.0.T / fac(i).float)
  result = inline_abs2(p) * factor

# parallel probabilities 
proc prob_parallel*[T: SomeFloat](state: FockState[T], outclicks: ClickPattern): T = 
  let usub = state.matrix.repeatRowsCols(outclicks, state.input)
  let p = perm_parallel(usub)

  var factor = 1.0.T
  for i in outclicks.photons:
    factor *= (1.0.T / fac(i).float)
  result = inline_abs2(p) * factor

#### ADAPTIVE 
proc prob*[T: SomeFloat](state: FockState[T], outclicks: ClickPattern): T = 
  let usub = state.matrix.repeatRowsCols(outclicks, state.input)
  const parallel_switch = 13
  let p =
    if usub.rows <= parallel_switch: perm(usub)
    else: perm_parallel(usub)

  var factor = 1.0.T
  for i in outclicks.photons:
    factor *= (1.0.T / fac(i).float)
  result = inline_abs2(p) * factor

proc prob*[T: SomeFloat](state: FockState[T], outphotons: seq[int]): T = 
  var output = state.input
  output.photons = outphotons
  state.prob(output)

# takes a FockState and returns a number 
#
# 1. Calculate probabilities for 
# got input photons called X
# to calculate a k sized marginal take all k-sized subsets of the input, X
# e.g. [0, 1, 0, 1] photon occupations 
#
#
proc marginalprob*[T: SomeFloat](state: FockState[T], photonPositions: seq[int]): T =
  var inputPositions = state.input.getRepeatedModes
  var margord = photonPositions.len
  var inputord = inputPositions.len
  for i in 0 ..< inputPositions.numCombinations(margord):
    var sset = inputPositions.lexisubset(margord,i)
    var submat = state.matrix[sset, photonPositions]
    var pamp = perm(submat)
    result += inline_abs2(pamp)
  let norm =float(fac(inputord-margord))/float(fac(inputord))
  result*norm

proc vanillasample*[T: SomeFloat](state: FockState[T]): ClickPattern =
  let dim = state.dim
  result = clickpattern(dim)
  var norm = float(1)
  var zlist: seq[int] = @[]
  for j in 0 ..< state.input.numClicks:
    var probs: seq[float] = @[]
    randomize()
    for i in 0 ..< dim:
      var zlisttry = zlist & @[i]
      probs.add marginalprob(state, zlisttry)/norm
    var probssum: seq[float] = probs.cumsummed
    var outmode: int = sample(state.input.modes, probssum)
    zlist.add outmode
    norm = probssum[outmode]
  for it in zlist:
    result.photons[it] += 1

proc tallysamples*(cps: seq[ClickPattern]): ClickPatternGroup = 
  result.uniqueCPs = deduplicate(cps)
  for i in result.uniqueCPs:
    result.tallies.add count(cps, i)

type 
  GaussianState*[T] = object
    dim*: int
    covariance*: Matrix[Complex[T]]
    displacement*: Vector[Complex[T]]

proc gaussianstate*[T: SomeNumber | Complex[SomeFloat]](dim: int): GaussianState[T] =
  result = GaussianState[T](dim: dim,
  covariance: Identity[Complex[T]](2*dim,2*dim), 
  displacement: vector[Complex[T]](2*dim)
  )

proc checkValid*(state: GaussianState): bool =
  # check covariance is hermitian
  assert state.covariance.isHermitian


proc toState*(graph: Graph, scaling: float = 0.8): GaussianState[float] =
  var A = graph.adj

  # echo "A\n", A
  block: 
    let (U,S) = A.Schur
    # echo "U\n", U.chop
    # echo "S\n", S.chop, " s.isupperdiagonal ", S.isupperdiagonal
    # echo "s diag", S.getDiagonal

    let (J, D) = A.JacobiEigenDecomp
    # echo "J\n", J.chop
    # echo "D\n", D.chop, " D.isupperdiagonal ", D.isupperdiagonal

  # quit(0)

  # echo "Eigenvals ", A.eigenvalues
  let maxeigenval = A.eigenvalues.max
  let maxc = scaling * (1.0 / maxeigenval)
  A *= maxc

  let n = graph.dim
  var mathcalA = matrix[float](2 * n, 2 * n)

  # Option 3, pass a range for [rows, cols]
  mathcalA[0 ..< n, n ..< 2*n] = A
  mathcalA[n ..< 2*n, 0 ..< n] = A

  # echo "A\n", A
  # echo "MathcalA\n", mathcalA

  let fullident = Identity[float](2*n, 2*n)

  result = gaussianstate[float](n)
  result.covariance = 2.0 * (fullident - mathcalA).inverse - fullident

  # echo result

proc vacuum_prob*[T: SomeFloat](m: Matrix[Complex[T]]): T {.inline.} =
  # p = det( 0.5 * sigma + 0.5 ) ^{-1/2}
  # A = 0.5 * mat + 0.5
  # const half: T = 0.5
  # var A = half * m
  # for i in 0 ..< A.rows:
  #   A[i, i] += half
  return T(1.0) / sqrt(m.chol_ldl_crout_determinant)

proc vacuum_prob*[T: SomeFloat](state: GaussianState[T]): T {.inline.}  =
  vacuum_prob(state.covariance)

proc vacuum_prob*[T: SomeFloat](m: Matrix[Complex[T]], rows_cols: openArray[int]): T {.inline.} = 
  return T(1.0) / sqrt(m.chol_ldl_crout_determinant(rows_cols))

proc vacuum_prob*[T: SomeFloat, int_t: SomeInteger](m: Matrix[Complex[T]], rows_cols: openArray[int_t], spacedim: int_t): T {.inline.} = 
  return T(1.0) / sqrt(m.chol_ldl_crout_determinant_split_for(rows_cols, spacedim))

proc threshold*[T: SomeFloat](state: GaussianState[T], cp: Threshold): T =
  if cp.numClicks == 0: return state.vacuum_prob
  # 2 ^ n
  let num_terms = 1 shl cp.numClicks
  # echo "num terms ", num_terms
  let modes = get_set_modes(cp)
  result = T(0.0) 

  for i in countup(1, num_terms - 1, 2):
    let ps = lexi_powerset(modes, i.bintogray)
    let vac_proj = vacuum_prob(state.covariance, ps, state.dim)
    result -= vac_proj

  for i in countup(2, num_terms - 1, 2):
    let ps = lexi_powerset(modes, i.bintogray)
    let vac_proj = vacuum_prob(state.covariance, ps, state.dim)
    result += vac_proj

  return result + T(1.0)

proc threshold_parallel*[T: SomeFloat](state: GaussianState[T], cp: Threshold): T =
  if cp.numClicks == 0:
    return state.vacuum_prob
  # 2 ^ n
  let num_terms = 1 shl cp.numClicks
  # echo "num terms ", num_terms
  let modes = get_set_modes(cp)
  result = T(0.0) 

  let posOutput = newseq[T](num_terms div 2)
  let bufout = cast[ptr UncheckedArray[T]](posOutput[0].unsafeAddr)
  # posbufout.initialize(num_terms div 2)

  # the first term
  let ps = lexi_powerset(modes, 1.bintogray)
  let vac_proj = vacuum_prob(state.covariance, ps, state.dim)
  bufout[0] = -vac_proj

  init(Weave)
  parallelFor i in 1 ..< (num_terms div 2).int:
    captures: {modes, state, bufout}

    let ps2 = lexi_powerset(modes, (2*i).bintogray)
    let vac_proj2 = vacuum_prob(state.covariance, ps2, state.dim)
    bufout[i] = vac_proj2

    let ps = lexi_powerset(modes, (2*i + 1).bintogray)
    let vac_proj = vacuum_prob(state.covariance, ps, state.dim)
    bufout[i] -= vac_proj
  exit(Weave)

  # echo "pos \n"
  # for i in 0 ..< num_terms div 2:
  #   echo posbufout[i]

  for i in 0 ..< num_terms div 2:
    result += bufout[i]

  return result + T(1.0)

proc pnr*[T: SomeFloat](state: GaussianState[T], cp: ClickPattern): Complex[T] =
  if cp.numClicks == 0:
    return state.vacuumProb.complex

  let dim = state.dim
  # (sigma + 1) / 2
  var covq = 0.5.T * state.covariance
  # add out 1/2 identity
  for i in 0 ..< covq.rows:
    covq[i, i] += 0.5.T

  # need 1 - covq^(-1)
  var minusCovqInv = -(covq.inverse)
  for i in 0 ..< minusCovqInv.rows:
    minusCovqInv[i, i] += 1.0.T
  
  let A = minusCovqInv.swapRows
  var modes = cp.getRepeatedModes
  modes.add(modes.mapit(it + dim))
  let reducedA = A[modes, modes]

  let detfactor = covq.determinant.re.sqrt

  var factor: int = 1
  for photonnum in cp.photons:
    factor *= fac(photonnum)

  # possible loss of precision converting from int64 to float64...
  result = reducedA.hafnian / (factor.T * detFactor)

proc smsqmatrix*[T: SomeFloat](sqparam: T): Matrix[Complex[T]] =
  result = matrix(2, 2, @[
  cosh(sqparam), sinh(sqparam),
  sinh(sqparam), cosh(sqparam)])

proc tmsqmatrix*[T: SomeFloat](sqparam: T): Matrix[Complex[T]] =
  result = matrix(4, 4, @[
  cosh(sqparam), 0.0, 0.0,  sinh(sqparam),
  0.0, cosh(sqparam), sinh(sqparam), 0.0,
  0.0, sinh(sqparam), cosh(sqparam), 0.0,
  sinh(sqparam), 0.0, 0.0, cosh(sqparam)])

proc squeezer*[T: SomeFloat](sqparam: T, m: int): SparseMatrix[Complex[T]] =
  result = SparseMatrix[Complex[T]](matrix: smsqmatrix(sqparam), modes: @[m])

proc squeezer*[T: SomeFloat](sqparam: T, m1, m2: int): SparseMatrix[Complex[T]] =
  result = SparseMatrix[Complex[T]](matrix: smsqmatrix(sqparam), modes: @[m1, m2])

proc apply*[T: SomeFloat](state: var GaussianState, mat: SparseMatrix[Complex[T]]) =
  var m = mat
  # maps 0 to 0, 2 for example
  m.modes.add(m.modes.mapit(it + state.dim))
  # u = BS
  # Symplectic version is U \oplus U^*
  if mat.matrix.rows == mat.modes.len:
    # make U oplus U*
    m.matrix = m.matrix.oplus m.matrix.conj

  state.covariance.multiply_sparse_left(m)
  state.covariance.multiply_sparse_right(m.H)

proc apply*[T: SomeFloat](state: var GaussianState, mat: Matrix[Complex[T]]) =
  let m =
    if mat.rows == state.covariance.rows:         mat
    # Symplectic version is U \oplus U^
    elif 2 * mat.rows == state.covariance.rows: mat.oplus mat.conj
    else: quit(1)

  state.covariance = m * state.covariance * m.H

