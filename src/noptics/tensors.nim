import noptics/linalg
import std/sequtils

import complex

import std/macros

type Tensor*[T] = object
  data*: seq[T]
  dims*: seq[int]

proc tensor*[T](dims: varargs[int]): Tensor[T] =
  result.dims = dims.toSeq
  var size = 1
  for i in 0 ..< dims.len:
    size *= dims[i]
  result.data = newSeq[T](size)

proc constantTensor*[T](dims: varargs[int], val: T): Tensor[T] = 
  result.dims = dims.toSeq
  var size = 1
  for i in 0 ..< dims.len:
    size *= dims[i]
  result.data = newSeqWith(size, val)

proc identityTensor*[T](dims: varargs[int]): Tensor[T] =
  result.dims = dims.toSeq
  var size = 1
  for i in 0 ..< dims.len:
    size *= dims[i]
  result.data = newSeqWith(size, 0.0.T)
  for i,s in result:
    var diag = true
    for j in 0 ..< s.len div 2:
      if s[0] != s[2*j] or s[1] != s[2*j + 1]: 
        diag = false
        break
    if diag:
      result.data[i] = 1.0.T

converter toTensor*[T](m: Matrix[T]): Tensor[T] =
  result.data = m.data
  result.dims = @[m.rows, m.cols]

converter toTensor*[T](v: Vector[T]): Tensor[T] =
  result.data = v.data
  result.dims = @[v.len]

proc toTensor*[T](s: seq[T]): Tensor[T] =
  result.data = s
  result.dims = @[s.len]

proc len*(t: Tensor): int {.inline.}  =
  t.dims.foldl(a * b)

proc rank*(t: Tensor): int {.inline.} =
  t.dims.len

proc dim*(t: Tensor, idx: int): Slice[int] {.inline.} =
  result = 0 ..< t.dims[idx]

proc value*(t: Tensor): t.T {.inline.} =
  assert t.len == 1
  assert t.data.len == 1
  result = t.data[0]

proc abs*[T](t: Tensor[Complex[T]]): Tensor[T] = 
  result.dims = t.dims
  for i in 0 ..< t.len:
    result.data.add t.data[i].abs

# SuperSlice
type SliceType = enum
  all,
  some,
  single,
  Sarray

type SuperSlice* = object
  a*, b*: int
  T*: SliceType
  vals*: seq[int]

# takes ranges and max value (dimension) of each index
# i.e ranges = [0..1, 1..2, 3], dims = [2, 3, 1]
#
proc setDims*(ranges: varargs[SuperSlice], dims: seq[int]): seq[SuperSlice] =
  result = ranges.toSeq
  for i in 0 ..< result.len:
    if result[i].T == all:
      result[i].a = 0
      result[i].b = dims[i] - 1

# if we want to select all the indices in that position 
const `_`* = SuperSlice(T: all)

# ctor for single int to SuperSlice
converter `%`*(x: int): SuperSlice =
  result = SuperSlice(a: x, b: x, T: single)

# ctor for nim slice to superslice
converter `%`*(x: Slice[int]): SuperSlice =
  result = SuperSlice(a: x.a, b: x.b, T: some)

# ctor for nim seq or nim array to superslice
converter `%`*[Size: static[int]](x: array[Size, int]): SuperSlice = 
  result = SuperSlice(T: Sarray, vals: x.toSeq)

converter `%`*(x: openArray[int]): SuperSlice =
  result = SuperSlice(T: Sarray, vals: x.toSeq)

proc begin*(r: SuperSlice): int {.inline.} =
  result =
    if r.T == Sarray: r.vals[0]
    else: r.a

proc `end`*(r: SuperSlice): int {.inline.} =
  result =
    if r.T == Sarray: r.vals[^1]
    else: r.b

proc len*(r: SuperSlice): int {.inline.} =
  result =
    if r.T == Sarray: r.vals.len
    else: r.b - r.a + 1

iterator items*(r: SuperSlice): int {.inline.} =
  if r.T == Sarray: 
    for i in 0 ..< r.len:
      yield r.vals[i]
  else:
    for i in 0 ..< r.len:
      yield r.a + i

proc `[]`*(r: SuperSlice, i: int): int {.inline.} =
  if r.T == Sarray: 
    result = r.vals[i]
  else:
    result = r.a + i

# proc next*(r: var SuperSlice): int {.inline.} =
#   r.pos += 1
#   result = r[r.pos]



proc vargTest(x: varargs[SuperSlice]) = 
  echo "x ", x

# counts up in varying base, given a sequence and maximum
# value for each index, return the next sequence i.e. seq += 1
proc incrSeq*(s: var seq[int], dims: seq[int]) =
  for i in countdown(s.len - 1, 0):
    if s[i] == dims[i] - 1:
      s[i] = 0
    else:
      s[i] += 1
      break

# Counts up sequence over a seq of Superslices 
proc incrSeq*(s: var seq[int], ranges: seq[SuperSlice], pos: var seq[int]) =
  for i in countdown(s.len - 1, 0):
    # echo " i ", i
    # echo "s[i] ", s[i]
    # echo "r[i].end ", ranges[i].end
    # echo "r[i].begin ", ranges[i].begin
    # echo "ranges[i] ", ranges[i]

    if s[i] == ranges[i].end:
      s[i] = ranges[i].begin
      pos[i] = 0
    else:
      pos[i] += 1
      s[i] = ranges[i][pos[i]]
      break

# iterate through a tensor 
iterator items*(t: Tensor): seq[int] =
  var s = newSeq[int](t.rank)
  for i in 0 ..< t.len:
    yield s
    s.incrSeq(t.dims)

# iterator for index and seq[int] argument list 
iterator pairs*(t: Tensor): (int, seq[int]) =
  var s = newSeq[int](t.rank)
  for i in 0 ..< t.len:
    yield (i, s)
    s.incrSeq(t.dims)

# If we only want to iterate through a subset of the 
# elements of a tensor, we pass a seq of Superslices 
# which are the indices we want to iterate through
# returns index and a seq of the values for each indices
iterator subPairs*(t: Tensor, ranges: seq[SuperSlice]): (int, seq[int]) =
  var s = newSeq[int](t.rank)
  var num = 1
  for i, r in ranges:
    s[i] = r.begin
    num *= r.len
  # echo "s start ", s
  # echo "num ", num
  var pos = newSeq[int](t.rank)
  for i in 0 ..< num:
    # echo "i ", i, " s ", s
    yield (i, s)
    s.incrSeq(ranges, pos)


iterator subRange*(ranges: seq[SuperSlice]): seq[int] =
  var s = newSeq[int](ranges.len)
  var num = 1
  for i, r in ranges:
    s[i] = r.begin
    num *= r.len
  var pos = newSeq[int](ranges.len)
  for i in 0 ..< num:
    yield s
    s.incrSeq(ranges, pos)

proc `[]`*(t: Tensor, idxs: varargs[SuperSlice]): Tensor = 
  let ranges = idxs.setDims(t.dims)
  for i in ranges:
    result.dims.add i.len
  for i, s in subPairs(t, ranges):
    # echo i, " ", s
    # echo "t ", t
    # echo "t[s] ", t[s]
    result.data.add t[s]

proc `[]=`*(t: var Tensor, idxs: varargs[SuperSlice], val: Tensor) =
  let ranges = idxs.setDims(t.dims)
  # echo "ranges ", ranges
  for i, s in subPairs(t, ranges):
    # echo i, " ", s
    t[s] = val.data[i]

proc `[]=`*(t: var Tensor, idxs: varargs[SuperSlice], val: t.T) =
  let ranges = idxs.setDims(t.dims)
  # echo "ranges ", ranges
  for i, s in subPairs(t, ranges):
    # echo i, " ", s
    t[s] = val

template `[]=`*(t: var Tensor, idxs: varargs[SuperSlice], f: untyped) =
  let ranges = idxs.setDims(t.dims)
  # echo "ranges ", ranges
  for i, s in subPairs(t, ranges):
    # echo i, " ", s
    t[s] = f(t[s])

proc `[]`*(t: Tensor, idx: seq[int]): Tensor.T =
  assert idx.len == t.rank
  # assert x[i,j,k] == i * x.dims[1] * x.dims[2] + j * x.dims[2] + k
  var pos = idx[^1]
  for i in 1 ..< idx.len:
    var offset = 1
    for j in i ..< t.rank:
      offset *= t.dims[j]
    pos += idx[i - 1] * offset

  # echo "idx ", idx, " pos ", pos
  result = t.data[pos]

proc `[]=`*(t: var Tensor, idx: seq[int], val: Tensor.T) =
  assert idx.len == t.rank
  # assert x[i,j,k] == i * x.dims[1] * x.dims[2] + j * x.dims[2] + k
  var pos = idx[^1]
  for i in 1 ..< idx.len:
    var offset = 1
    for j in i ..< t.rank:
      offset *= t.dims[j]
    pos += idx[i - 1] * offset

  # echo "idx ", idx, " pos ", pos
  t.data[pos] = val

proc flatten*(t: var Tensor) =
  t.dims.keepItIf(it > 1)

proc applySingle*(t: var Tensor, val: Matrix, idxs: varargs[SuperSlice]) =
  let ranges = idxs.setDims(t.dims)
  # echo "ranges \n", ranges

  var temp = t[ranges]
  temp.dims = @[2,2]
  # echo "temp ", temp
  t[ranges] = val * temp
  # echo "after ", t[ranges]

proc apply*(t: var Tensor, val: Matrix, ranges: varargs[SuperSlice]) =
  echo "ranges \n", ranges
  # find if any indices are all
  var allSum: seq[int]
  var num: int = 0
  for i in 0 ..< ranges.len:
    if ranges[i].T == all: 
      allSum.add i
      # should be +=, or largest and call
      # apply recursively for each dimension to loop over
      num = t.dims[i]

  var subRange = ranges.toSeq

  for i in 0 ..< allSum.len:
    let idx = allSum[i]
    let curVal = subRange[idx].b
    subRange[idx] = %(curVal .. curVal) 

  echo " num ", num

  for j in 0 ..< num:
    echo "SubRange ", subRange
    echo "t ", t

    var temp = t[subRange]
    temp.flatten
    echo "temp flattened", temp

    t[subRange] = val * temp
    # echo "after ", t[subRange]

    for i in 0 ..< allSum.len:
      let idx = allSum[i]
      let curVal = subRange[idx].b + 1
      subRange[idx] = %(curVal .. curVal) 


proc `$`*(t: Tensor): string =
  result = $t.dims & "\n"
  var rowSize = 1
  for i in t.dims.len div 2 ..< t.dims.len:
    rowSize *= t.dims[i]

  for i, s in t:
    # echo "i ", i
    if i mod rowSize == 0:
      result &= "[ " & $(t[s])
    elif ((i + 1) mod rowSize) == 0:
      result &= ",\t" & $(t[s]) & " ]\n"
    else: 
      result &= ",\t" & $(t[s]) 


# i_n + (i_0 * d[0] * d[1] * ... * d[n]) + (i_1 * d[1] * ... * d[n]) 
proc indicesToOffset*(dims: seq[int], indices: varargs[int]): int =
  result = indices[^1]
  for i in 1 ..< indices.len:
    var offset = 1
    for j in i ..< dims.len:
      offset *= dims[j]
    result += indices[i - 1] * offset

proc indicesToOffset*(t: Tensor, indices: varargs[int]): int =
  indicesToOffset(t.dims, indices)

template makeBroadcastOp(name, op: untyped): untyped =
  proc name*(a, b: Tensor): Tensor =
    assert a.dims == b.dims
    result = a
    for i in 0 ..< a.len:
      result.data[i] = op(a.data[i], b.data[i])

makeBroadcastOp(`cwiseProd`, `*`)
makeBroadcastOp(`cwiseSum`, `+`)
makeBroadcastOp(`cwiseDiff`, `-`)
makeBroadcastOp(`cwiseDiv`, `/`)

# i.e. 
# 2x2 matrix multiply (2x2) x (1x1) tensor 
proc `*`*(m: Matrix, t: Tensor): Tensor =
  result = t
  # [ a b ] [ e f ] = [ ae + bg, af + bh ]
  # [ c d ] [ g h ]   [ ce + dg, cf + dh ]
  if t.rank == 1:
    assert m.rows == t.dims[0]
    for r in 0 ..< t.dims[0]:
      result[0] = m[0,0] * t.data[0] + m[0,1] * t.data[1]
      result[1] = m[1,0] * t.data[0] + m[1,1] * t.data[1]

  if t.rank == 2:
    assert m.rows == t.dims[1]
    result[@[0,0]] = m[0,0] * t[0,0].value + m[0,1] * t[1,0].value
    result[@[0,1]] = m[0,0] * t[0,1].value + m[0,1] * t[1,1].value
    result[@[1,0]] = m[1,0] * t[0,0].value + m[1,1] * t[1,0].value
    result[@[1,1]] = m[1,0] * t[0,1].value + m[1,1] * t[1,1].value


proc linspaced*[T](start, stop: T, steps: int): Tensor[T] =
  result = tensor[T](steps)
  let diff = (stop - start) / (steps.T-1)
  for i in 0 ..< steps:
    result.data[i] = start + i.T * diff

proc linspaced*[T](t: var Tensor[T], start, stop: T, dims: varargs[SuperSlice]) = 
  let ranges = dims.setDims(t.dims)

  var leng = 0
  # echo ranges
  for i in ranges:
    # echo i.len
    if i.len > 1:
      leng += i.len

  let diff = (stop - start) / (leng.T - 1)
  for i, sub in subPairs(t,ranges):
    # echo "i ", i, ", sub ", sub
    t[sub] = start + i.T * diff


template map*[T](t: Tensor[T], f: untyped): Tensor[T] =
  var res = t
  # echo "ranges ", ranges
  for i in 0..<t.len:
    # echo i, " ", s
    res.data[i] = f(t.data[i])
  res

template mapInPlace*[T](t: var Tensor[T], f: untyped, idxs: varargs[SuperSlice]) =
  let ranges = idxs.setDims(t.dims)
  for i, sub in subPairs(t, ranges):
    t[sub] = f(t[sub])


when isMainModule:
  let m = Identity[float](2)
  var t = tensor[float](4,4)

  var z = tensor[int](2,2,2,2)
  echo z
  for i, s in z:
    echo i, s
    z[s] = i

  echo z[0,0,0,0]
  echo z

  for i, s in t:
    t[s] = i.float
  echo "t\n", t
  
  echo "t range ", t[0..1, 2..3]

  echo "t\n", t
  echo "t range assign"
  t[0..1, 0..1] = m
  echo t

  # echo m
  # echo t
  t.dims = @[2,2,2,2]

  # echo m * t[0..0, 0..0, t.dim(2), t.dim(3)]
  echo m
  

  for i in t:
    echo i, ", ", t[i]
  echo t

  for i,s in t:
    t[s] = i.float
    echo s, ", ", t[s]
  echo t
    
  echo ""
  echo t[0..0, t.dim(1), 0..0, t.dim(3)]

  echo t[t.dim(0), 0..0, t.dim(2), 0..0]

  vargTest(_, _, %0, %0)
  echo t[%0, _, %0, _]
  echo t[_, _, _, _]
  echo t[0, 0, 1, _]
  echo t[1,1,1,1]

  echo t[0..1, _, 0..1, _]

  echo "t apply "
  t.apply( 2.0*m, 0..1, _, 0..1, _)
  t.apply( 2.0*m, 0..1, _, 0..1, _)

  echo t


  echo t[%[0,1], _, %[0,1], _]

  let r = %[0,2]
  echo "r ", r
  echo "r begin ", r.begin
  echo "r end ", r.end
  echo "r len ", r.len
  for i in r:
    echo i

  for i in 0..<r.len:
    echo r[i]

  # for i in %(0..1):
  #   echo i


  for i in 0 ..< t.dims[1]:
    t.apply(2.0 * m, %[0,1], i, %[0,1], i)
  
  echo "t ", t

  t.apply( 3.0 * m, 0..1, _, 0..1, _)
  echo " t ", t
  
  t.apply(1.0/3.0 * m, 0..1, _, 0..1, _)
  echo " t ", t

  t.apply(1.0/2.0 * m, _, 0..1, _, 0..1)
  echo " t ", t

  echo "before v"

  block:

    echo "v"
    var v = tensor[float](2,3)
    echo v
    for i in 0 ..< v.dims[0]:
      v[i, _] = 1.0
    echo v
    
    echo "v"
    v = tensor[float](2, 3)
    echo v
    v[_, _] = 1.0
    echo v

    echo "v "
    v = tensor[float](2,3,4)
    echo v
    v[0,0,_] = -1.0
    echo v

    v[0, 1..2, _] = 2.0
    echo v

    v = constantTensor[float](2,2,2,2, 1.0)
    echo v

    v = identityTensor[float](2,3,2,3)
    echo v


