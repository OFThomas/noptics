# import noptics/io
import noptics/linalg

import math, random
import sequtils

# randomize()

type 
  Graph* = object
    adj*: Matrix[float]

proc makegraph*(adj_matrix: Matrix[float]): Graph = 
  result = Graph(adj: adj_matrix)

proc makegraph*(adj_vals: seq[float]): Graph = 
  let dim = round(sqrt(adj_vals.len.float)).int
  result = Graph(adj: matrix(dim, dim, adj_vals))

proc randomgraph*(dim: int, density: float): Graph = 
  var adj = matrix[float](dim, dim)
  for r in 0 ..< dim:
    for c in r+1 ..< dim:
      if rand(1.0) <= density:
        adj[r,c] = 1.0

  # make adj symmetric 
  adj += adj.Transpose
  result = Graph(adj: adj)

proc subgraph*(graph: Graph, subvertices: seq[int]): Graph =
  let submat = graph.adj[subvertices, subvertices]
  result = makegraph(submat)

proc getdegree*(graph: Graph, vertex: int): int = 
  result = graph.adj.row(vertex).sum.int

proc getvertices*(graph: Graph): seq[int] = 
  result = toSeq 0 ..< graph.adj.rows

proc dim*(graph: Graph): int {.inline.} =
  result = graph.adj.rows


