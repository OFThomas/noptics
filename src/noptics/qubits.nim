import noptics/linalg

import math
import complex

type 
  QubitState*[ScalarT] = object
    numqubits*: int
    vector*: Vector[Complex[ScalarT]]

proc getdim*(state: QubitState): int {.inline.} =
  result = 1 shl state.numqubits

# sets the vacuum (all zeros state)
proc qubitstate*[ScalarT: SomeFloat](numqubits: int): QubitState[ScalarT] =
  result = QubitState[ScalarT](numqubits: numqubits, vector: vector[Complex[ScalarT]](1 shl numqubits))
  result.vector[0] = 1.0.ScalarT.complex

proc rxmatrix*[ScalarT: SomeFloat](theta: ScalarT): Matrix[Complex[ScalarT]] =
  const i = complex(0.0.ScalarT, 1.0.ScalarT)
  let halftheta = theta / 2.0.ScalarT
  result = matrix[Complex[ScalarT]](2,2, 
    @[cos(halftheta).complex, -i * sin(halftheta),
    -i * sin(halftheta), cos(halftheta).complex])

proc rymatrix*[ScalarT: SomeFloat](theta: ScalarT): Matrix[Complex[ScalarT]] =
  let halftheta = theta / 2.0.ScalarT
  result = matrix[Complex[ScalarT]](2,2,
    @[cos(halftheta).complex, -sin(halftheta).complex,
    sin(halftheta).complex, cos(halftheta).complex])

proc rzmatrix*[ScalarT: SomeFloat](theta: ScalarT): Matrix[Complex[ScalarT]] =
  const i = complex(0.0.ScalarT, 1.0.ScalarT)
  const zero = complex(0.0.ScalarT, 0.0.ScalarT)
  let halftheta = theta / 2.0.ScalarT
  result = matrix[Complex[ScalarT]](2,2,
    @[exp(-i*halftheta), zero,
    zero, exp(i*halftheta)])

proc cnotmatrix*[ScalarT: SomeFloat](): Matrix[Complex[ScalarT]] = 
  result = Identity[Complex[ScalarT]](2,2).oplus matrix[Complex[ScalarT]](2,2, 
      @[0.0.ScalarT.complex, 1.0.ScalarT.complex,
      1.0.ScalarT.complex, 0.0.ScalarT.complex])



proc Rx*[ScalarT: SomeFloat](theta: ScalarT, qubit: int): SparseMatrix[Complex[ScalarT]] =
  result = sparseMatrix[Complex[ScalarT]](rxmatrix(theta), @[qubit])

proc Ry*[ScalarT: SomeFloat](theta: ScalarT, qubit: int): SparseMatrix[Complex[ScalarT]] =
  result = sparseMatrix[Complex[ScalarT]](rymatrix(theta), @[qubit])

proc Rz*[ScalarT: SomeFloat](theta: ScalarT, qubit: int): SparseMatrix[Complex[ScalarT]] =
  result = sparseMatrix[Complex[ScalarT]](rzmatrix(theta), @[qubit])

proc Cnot*[ScalarT: SomeFloat](ctrl, targ: int): SparseMatrix[Complex[ScalarT]] =
  result = sparseMatrix[Complex[ScalarT]](cnotmatrix[ScalarT](), @[ctrl, targ])

# 2 1 0
# _______
# 0 0 0 | 0
# 0 0 1 | 1
# 0 1 0 | 2 
# 0 1 1 | 3
# 1 0 0 | 4
# 1 0 1 | 5
# 1 1 0 | 6
# 1 1 1 | 7
#
# if qubit 0 stride = 2^0 = 1, jump = 2
# if qubit 1 stride = 2^1 = 2, jump = 2 
# if qubit 2 stride = 2^2 = 4, jump = ?
proc apply*[ScalarT: SomeFloat](state: var QubitState[ScalarT], mat: SparseMatrix[Complex[ScalarT]]) =
  # get the qubit idx from mat
  if mat.modes.len == 1:
    let stride = 1 shl mat.modes[0]
    for s in countup(0, state.getdim - 1, 2 * stride):
      for r in 0 ..< stride:
        state.vector.apply(mat.matrix, s + r, s + r + stride)

  elif mat.modes.len == 2:
    let ctrlpos = mat.modes[0]
    let targpos = mat.modes[1]

    let highbit = 1 shl max(ctrlpos, targpos)
    let lowbit = 1 shl min(ctrlpos, targpos)
    
    let highincr = highbit shl 1
    let midincr = lowbit shl 1

    let ctrlbit = 1 shr ctrlpos
    let targbit = 1 shl targpos

    for i in countup(0, state.getdim - 1, highincr):
      for j in countup(0, highbit - 1, midincr):
        for k in 0 ..< lowbit:
          # state.vector.apply(mat.matrix, s + r, s + r + stride)
          continue


  else: 
    echo "ERR Not implemented arbitrary qubit operations yets"
    quit(1)

# proc apply*[ScalarT: SomeFloat](state: var QubitState[ScalarT], mat: SparseMatrix[Complex[ScalarT]]) =
#   # get the qubit idx from mat
#   if mat.modes.len == 1:
#     let qidx = mat.modes[0]
#     let pos = 1'u64 shl qidx.uint64
#     # state.applySingleQubit(mat.matrix, pos)
#     for i in 0 ..< state.numqubits:
#       state.vector.apply(mat.matrix, i * pos, (i+1) * pos)

#   elif mat.modes.len == 2:
#     let ctrlpos = 1'u64 shl mat.modes[0].uint64
#     let targpos = 1'u64 shl mat.modes[1].uint64
#     state.applyTwoQubit(mat.matrix, ctrlpos, targpos)
#   else: 
#     echo "ERR Not implemented arbitrary qubit operations yets"
#     quit(1)

when isMainModule:
  echo qubitstate[float](2)
  
  echo rxmatrix(PI/2)
  echo rymatrix(PI/2)
  echo rzmatrix(PI/2)
  echo cnotmatrix[float]()

  echo Rx(PI/2, 0)
  echo Ry(PI/2, 0)
  echo Rz(PI/2, 0)
  echo Cnot[float](0, 1)

  var state = qubitstate[float](3)
  echo state

  state = qubitstate[float](3)
  state.apply Rx(Pi/2, 0)
  echo state

  state = qubitstate[float](3)
  state.apply Rx(Pi/2, 1)
  echo state

  state = qubitstate[float](3)
  state.apply Rx(Pi/2, 2)
  echo state

