import noptics/linalg
import std/sequtils

import std/macros

type Tensor*[T] = object
  data*: seq[T]
  dims*: seq[int]

# type Tensor*[D: Static[int], T] = object
#   data*: seq[T]
#   dims*: Array[D, int]

proc tensor*[T](dims: varargs[int]): Tensor[T] =
  result.dims = dims.toSeq
  var size = 1
  for i in 0 ..< dims.len:
    size *= dims[i]
  result.data = newSeq[T](size)

converter toTensor*[T](m: Matrix[T]): Tensor[T] =
  result.data = m.data
  result.dims = @[m.rows, m.cols]

converter toTensor*[T](v: Vector[T]): Tensor[T] =
  result.data = v.data
  result.dims = @[v.len]

proc len*(t: Tensor): int {.inline.}  =
  t.dims.foldl(a * b)

proc rank*(t: Tensor): int {.inline.} =
  t.dims.len

proc `[]`*(t: Tensor, idx: varargs[int]): Tensor.T =
  assert idx.len == t.rank
  # assert x[i,j,k] == i * x.dims[1] * x.dims[2] + j * x.dims[2] + k
  var pos = idx[^1]
  for i in 1 ..< idx.len:
    var offset = 1
    for j in i ..< t.rank:
      offset *= t.dims[j]
    pos += idx[i - 1] * offset

  # echo "idx ", idx, " pos ", pos
  result = t.data[pos]

proc `[]=`*(t: var Tensor, idx: varargs[int], val: Tensor.T) =
  assert idx.len == t.rank
  # assert x[i,j,k] == i * x.dims[1] * x.dims[2] + j * x.dims[2] + k
  var pos = idx[^1]
  for i in 1 ..< idx.len:
    var offset = 1
    for j in i ..< t.rank:
      offset *= t.dims[j]
    pos += idx[i - 1] * offset

  # echo "idx ", idx, " pos ", pos
  t.data[pos] = val


proc indicesToOffset*(dims: seq[int], indices: varargs[int]): int =
  result = indices[^1]
  for i in 1 ..< indices.len:
    var offset = 1
    for j in i ..< dims.len:
      offset *= dims[j]
    result += indices[i - 1] * offset

proc indicesToOffset*(t: Tensor, indices: varargs[int]): int =
  indicesToOffset(t.dims, indices)


iterator pairs*(idxs: varargs[Slice[int]], dims: seq[int]): int =
  var sum = 0
  for i in 0 ..< idxs.len:
    for j in idxs[i]:
      sum += j * dims[i..<dims.len].foldl(a * b)
      yield sum


proc `[]`*(t: Tensor, idxs: varargs[Slice[int]]): Tensor = 

  let newRank = idxs.len
  
  echo "idxs", idxs
  var dims: seq[int]
  for i in 0 ..< idxs.len:
    let idx = idxs[i]
    echo "lower ", idx.a
    echo "upper ", idx.b
    dims.add idx.b - idx.a + 1

  echo "dims ", dims
  result = tensor[t.T](dims)

  var idx = newSeq[int](idxs.len)

  for i in idxs.pairs(dims):
    echo "i ", i


  let ridx = result.indicesToOffset(0)
  echo " res idx ", ridx
  discard result.data[ridx]


# i.e. 
# 2x2 matrix multiply (2x2) x (1x1) tensor 
proc `*`*(m: Matrix, t: Tensor): Tensor =
  for i in 0 ..< t.rank:
    echo ""

  result = t

import std/macros

macro exloop(loop: ForLoopStmt) = 
  result = newTree(nnkForStmt)
  result.add loop[^3]
  result.add loop[^2][^1]
  result.add newCall(bindSym"echo", loop[0])

for item in exloop([1,2,3]): discard

# given 2 seqs return iterators to them 
let s1 = @[0,1]
let s2 = @[2,3]

for i in s1:
  for j in s2:
    echo "i ", i, " j ", j

let s = s1 & s2
echo "s ", s

let z = zip(s1, s2)
echo z

for item in z:
  echo "z ", z

macro macroloop(loop: ForLoopStmt) =
  echo loop.treeRepr
  result = newTree(nnkForStmt)
  result.add loop[0]
  result.add loop[1][1]
  result.add newCall(bindSym"echo", loop[0])

echo "macro loop"
for item in macroloop(s1): discard

macro macroloop2(x: ForLoopStmt): untyped =
  echo x.treeRepr

  expectKind x, nnkForStmt
  # check if the starting count is specified:
  var countStart = if x[^2].len == 2: newLit(0) else: x[^2][1]
  result = newStmtList()
  # we strip off the first for loop variable and use it as an integer counter:
  result.add newVarStmt(x[0], countStart)
  var body = x[^1]
  if body.kind != nnkStmtList:
    body = newTree(nnkStmtList, body)
  body.add newCall(bindSym"inc", x[0])
  var newFor = newTree(nnkForStmt)
  for i in 1..x.len-3:
    newFor.add x[i]
  # transform enumerate(X) to 'X'
  newFor.add x[^2][^1]
  newFor.add body
  result.add newFor
  # now wrap the whole macro in a block to create a new scope
  result = quote do:
    block: `result`


for a, b in macroloop2(items([1, 2, 3])):
  echo a, " ", b

# without wrapping the macro in a block, we'd need to choose different
# names for `a` and `b` here to avoid redefinition errors
for a, b in macroloop2(10, [1, 2, 3, 5]):
  echo a, " ", b

# proc buildForLoops(num: int, dims: seq[int]): NimNode =

#   var forLoops = nnkForStmt.newTree()
#   var stmtInLoop = newNimNode(nnkNilLit)

#   var index: seq[NimNode]
#   for i in 0 ..< num:
#     index.add genSym(nskforVar, "i" & $i)

#   let innerStmt = quote do:
#     echo "inner ", `index`

#   # echo "dims[i]", dims[0]
#   for i in 0 ..< num:
#     var loop = nnkForStmt.newTree(
#       index[i],
#       nnkInfix.newTree(
#         ident"..<",
#         newLit 0,
#         newLit dims[i]
#       )
#     )
#     if stmtInLoop.kind == nnkNilLit:
#       stmtInLoop = innerStmt
#     else:
#       stmtInLoop = forLoops

#     loop.add stmtInLoop
#     forLoops = loop
#   result = forLoops

# echo "buildForLoops"

# macro m3(): untyped = 
#   result = newStmtList()
#   result.add buildForLoops(2, @[2,3])
  
#   echo result.repr
#   result = quote do:
#     block:
#       `result`


# const f = buildForLoops(1)
# echo f.treeRepr

# m3()

# macro sumloop(loop: ForLoopStmt)

when isMainModule:
  let m = Identity[float](2)
  var t = tensor[float](4,4)
  
  echo t[0..1, 2..3]

  # echo m
  # echo t

  # echo m * t
  


