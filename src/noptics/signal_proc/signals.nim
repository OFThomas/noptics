import gnuplot as gnplt

import math, random, sequtils, os

proc fourier_series(x: float, num_terms: int): float =
  for n in 0 ..< num_terms:
    let k = 2.0 * n.float + 1.0
    result += sin(k * x) / k
  result *= 4.0/PI

proc square_wave(x: float): float = 
  let s = sin(x)
  if 0 <= s and s <= 1.0:
    return 1.0
  elif -1.0 <= s and s < 0.0:
    return -1.0
  else:
    return 0.0

const resolution = 1e3.int
const steps = 10
const max_harmonics = 50

let x = toseq(0..resolution).mapit(2 * PI * it.float/resolution.float)
let y = x.mapit(sin(it))
let y2 = x.mapit(2*sin(it))

proc square_wave_plot() = 
  gnplt.cmd "set key"
  gnplt.cmd "set xrange [-0.1:]"
  gnplt.cmd """set ylabel "Signal" """
  gnplt.cmd """set xlabel "Time" """
  gnplt.cmd "set grid"
  for n in countup(0, max_harmonics, steps):
    let offset = n.float * 0.03
    if n == 0:
      gnplt.plot x, x.mapit(0.1 + square_wave(it))
    else:
      gnplt.plot x, x.mapit(offset + fourier_series(it, n)), "Harmonics = " & $n
  
  discard readchar stdin


proc square_wave_gif() = 
  # gif
  for n in countup(0, max_harmonics, steps):
    let offset = n.float * 0.03
    var ylocal: seq[float]
    if n == 0:
      ylocal = x.mapit(0.1 + square_wave(it))
    else:
      ylocal = x.mapit(offset + fourier_series(it, n))

    let fname = "tmp.tmp" & $n
    let f = open(fname, fmWrite)
    for i in x.low..x.high:
      writeLine f, x[i], " ", ylocal[i]
    f.close

  gnplt.cmd "set terminal gif animate delay 1"
  gnplt.cmd "set output 'tmp.gif'"
  gnplt.cmd "stats 'tmp.tmp0' nooutput"

  gnplt.cmd """do for [i=0:int(STATS_records)-1] {
      set key bottom right
      set grid
      set xrange [-0.05:3.19]
      set yrange [0.5:2.8]
      plot 'tmp.tmp50' every ::0::i w l lc 7 title "Harmonics = 50", \
      'tmp.tmp50' every ::i::i w p pt 5 lc 7 notitle , \
      'tmp.tmp40' every ::0::i w l lc 6 title "Harmonics = 40", \
      'tmp.tmp40' every ::i::i w p pt 5 lc 6 notitle , \
      'tmp.tmp30' every ::0::i w l lc 4 title "Harmonics = 30", \
      'tmp.tmp30' every ::i::i w p pt 5 lc 4 notitle , \
      'tmp.tmp20' every ::0::i w l lc 3 title "Harmonics = 20", \
      'tmp.tmp20' every ::i::i w p pt 5 lc 3 notitle , \
      'tmp.tmp10' every ::0::i w l lc 2 title "Harmonics = 10", \
      'tmp.tmp10' every ::i::i w p pt 5 lc 2 notitle , \
      'tmp.tmp0' every ::0::i w l lc 1 title "Harmonics = 0  ", \
      'tmp.tmp0' every ::i::i w p pt 5 lc 1 notitle , \
  }"""

when ismainmodule:
  square_wave_gif()
