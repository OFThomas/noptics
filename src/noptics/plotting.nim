import noptics/optics
import gnuplot as gnplt

import math, random, sequtils, os

const resolution = 1e2.int

let xs = toseq(0..resolution).mapit(2 * PI * it.float/resolution.float)
let ys = xs.mapit(sin(it))

const fname = "tmp.tmp"
let f = open(fname, fmWrite)
for i in xs.low..xs.high:
  writeLine f, xs[i], " ", ys[i]
f.close

gnplt.cmd "set terminal gif animate delay 10"
gnplt.cmd "set output 'tmp.gif'"
gnplt.cmd "stats 'tmp.tmp' nooutput"

gnplt.cmd """do for [i=0:int(STATS_records)-1] {
    set xrange [0:6.3]
    set yrange [-1:1]
    plot 'tmp.tmp' every ::0::i w l, \
    'tmp.tmp' every ::i::i w p pt 5 lc 1
}"""

# discard readchar stdin
