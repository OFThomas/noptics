import sequtils
import std/random
import math
import algorithm

import complex

import bitops
import json
import strutils

# import noptics/io
import noptics/combinatorics

import weave

# import timeit

# based on the Neo library 
# https://github.com/andreaferretti/neo/blob/master/neo/dense.nim
randomize()

type AnyFloat* = SomeFloat or Complex[SomeFloat]

proc toType*[T](val: T, mytype: typedesc): auto {.inline.} = 
  # echo "my type ", mytype
  # echo "myType.T ", myType.T
  when myType is SomeFloat: val.myType
  elif myType is Complex[SomeFloat]: complex( myType.T(val))

# let val = 1.0'f64
# echo val
# echo val.toType(Complex32)

proc `+=`*[T: SomeFloat](num: var Complex[T], real: T) {.inline.} =
  num.re += real

proc `-=`*[T: SomeFloat](num: var Complex[T], real: T) {.inline.} =
  num.re -= real

proc `*=`*[T: SomeFloat](num: var Complex[T], real: T) {.inline.} =
  num.re *= real
  num.im *= real

proc `/=`*[T: SomeFloat](num: var Complex[T], real: T) {.inline.} =
  num.re /= real
  num.re /= real


type MatrixStorageOrder* = enum
  RowMajorStorage = "RowMajorStorage", ColMajorStorage = "ColMajorStorage"

type 
  Matrix*[T] = object
    storageorder*: MatrixStorageOrder
    rows*, cols*: int
    data*: seq[T]

  Vector*[T] = object
    len*: int
    data*: seq[T]

type
  Sparsematrix*[T] = object
    matrix*: Matrix[T]
    modes*: seq[int]

proc vector*[T: AnyFloat](data: seq[T]): Vector[T] = 
  result = Vector[T](len: data.len, data: data)

proc vector*[T: AnyFloat](size: int): Vector[T] =
  result = Vector[T](len: size, data: sequtils.repeat(0.0.toType(T), size))

proc vectorc*(n: int): Vector[Complex64] =
  result = Vector[Complex64](len: n, data: sequtils.repeat(complex(0.0), n))

proc vectorc*[T: SomeFloat](data: seq[T]): Vector[Complex[T]] = 
  result = Vector[Complex[T]](len: data.len, data: data.mapit(complex(it)))

proc randomvector*[T: AnyFloat](n: int, max: float = 1.0): Vector[T] =
  result = vector[T](newSeq[T](n))
  for i in 0 ..< n:
    result.data[i] = rand(max).toType(T)

proc constantvector*[T: AnyFloat](n: int, val: T): Vector[T] = 
  Vector[T](len: n, data: sequtils.repeat(val, n))

proc matrix*[T: AnyFloat](rows, cols: int): Matrix[T] = 
  result = Matrix[T](storageOrder: RowMajorStorage, rows: rows, cols: cols, data: sequtils.repeat(0.0.toType(T), rows * cols))

# sq matrix 
proc matrix*[T: AnyFloat](n: int, data: seq[T]): Matrix[T] =
  result = Matrix[T](storageOrder: RowMajorStorage, rows: n, cols: n, data: data)

proc matrix*[T: AnyFloat](rows, cols: int, data: seq[T]): Matrix[T] =
  result = Matrix[T](storageOrder: RowMajorStorage, rows: rows, cols: cols, data: data)

# complex valued
proc matrixc*[T: SomeFloat](rows, cols: int, data: seq[T]): Matrix[Complex[T]] = 
  result = Matrix[Complex[T]](storageOrder: RowMajorStorage, rows: rows, cols: cols, data: data.mapit(complex(it)))

proc matrixc*[T: SomeFloat](n: int, data: seq[T]): Matrix[Complex[T]] = 
  result = Matrix[Complex[T]](storageOrder: RowMajorStorage, rows: n, cols: n, data: data.mapit(complex(it)))

proc toComplex*[T: SomeFloat](x: Vector[T]): Vector[Complex[T]] =
  result = vector(x.len)
  for i in 0 ..< x.data.len:
    result.data[i] = x.data[i].complex

proc makematrix*[T: AnyFloat](rows, cols: int, f: proc(r, c: int): T): Matrix[T] =
  result = matrix[T](rows, cols, newSeq[T](rows * cols))
  for r in 0 ..< rows:
    for c in 0 ..< cols:
      result.data[r * cols + c] = f(r, c)

template makematrixtemplate*(T: typedesc, rows, cols: int, f: untyped): auto = 
  var res = matrix[T](rows, cols, newSeq[T](rows*cols))
  for r {.inject.} in 0 ..< rows:
    for c {.inject.} in 0 ..< cols:
      res.data[r * cols + c] = f
  res

proc Identity*[T: AnyFloat](n, m: int) : Matrix[T] = 
  result = makematrixtemplate(T, n, m, if r == c: 1.0.toType(T) else: 0.0.toType(T))

proc Identity*[T: AnyFloat](n: int) : Matrix[T] = 
  Identity[T](n, n)

proc Identityc*(n: int): Matrix[Complex64] =
  result = Identity[Complex64](n)

proc constantmatrix*[T: AnyFloat](rows, cols: int, val: T): Matrix[T] = 
  matrix[T](rows, cols, sequtils.repeat(val, rows * cols))

proc randommatrix*[T: AnyFloat](rows, cols: int, max: float = 1.0): Matrix[T] =
  result = matrix[T](rows, cols, newSeq[T](rows * cols))
  for i in 0 ..< rows * cols:
    result.data[i] = rand(max).toType(T)

proc randommatrix*[T: AnyFloat](n: int, max: float = 1.0): Matrix[T] =
  randommatrix[T](n, n, max)

proc sparsematrix*[T: AnyFloat](mat: Matrix[T], modes: seq[int]): SparseMatrix[T] = 
  result = SparseMatrix[T](matrix: mat, modes: modes)

proc diagonalmatrix*[T: AnyFloat](vec: Vector[T]): Matrix[T] =
  result = matrix[T](vec.len, vec.len)
  for i in 0 ..< vec.len:
    result[i,i] = vec[i]

# Accessing 
proc `[]`*[T: AnyFloat](m: Matrix[T], r, c: SomeInteger): T {.inline.} =
  result = m.data[r.int * m.cols + c.int] 

proc `[]`*[T: AnyFloat](m: var Matrix[T], r, c: int): var T {.inline.} =
  result = m.data[r.int * m.cols + c.int] 

proc `[]=`*[T: AnyFloat](m: var Matrix[T], r,c: int, val: T) {.inline.} =
  m.data[r * m.cols + c] = val

proc `[]`*[T: AnyFloat](v: Vector[T], idx: int): T {.inline.} = 
  result = v.data[idx]

proc `[]`*[T: AnyFloat](v: var Vector[T], idx: int): var T {.inline.} = 
  result = v.data[idx]

proc `[]=`*[T: AnyFloat](v: var Vector[T], idx: int, val: T) {.inline.} =
  v.data[idx] = val

proc `[]`*[T: AnyFloat, A,B: seq[int] | Slice[int]](mat: Matrix[T], rows: A, cols: B): Matrix[T] =
  result = matrix[T](rows.len, cols.len)
  when A is seq[int] and B is seq[int]:
    for ridx, r in rows:
      for cidx, c in cols:
        result[ridx, cidx] = mat[r, c] 
  elif A is seq[int] and B is Slice[int]:
    for ridx, r in rows:
      for cidx, c in cols.toseq:
        result[ridx, cidx] = mat[r, c] 
  elif A is Slice[int] and B is seq[int]:
    for ridx, r in rows.toseq:
      for cidx, c in cols:
        result[ridx, cidx] = mat[r, c] 
  elif A is Slice[int] and B is Slice[int]:
    for ridx, r in rows.toseq:
      for cidx, c in cols.toseq:
        result[ridx, cidx] = mat[r, c] 
  else:
    echo "ERR NOT IMPLEMENTED `[]` for " & $A & " " & $B

# array overload
proc `[]`*[T: AnyFloat, containersize: static[int]](mat: Matrix[T], rows: array[containersize, int], cols: Slice[int]): Matrix[T] {.inline.} =
  mat[@rows, cols]

proc `[]`*[T: AnyFloat, containersize: static[int]](mat: Matrix[T], rows: Slice[int], cols: array[containersize, int]): Matrix[T] {.inline.} =
  mat[rows, @cols]

# array overload
proc `[]`*[T: AnyFloat, containersize : static[int]](mat: Matrix[T], rows, cols: array[containersize, int]): Matrix[T] {.inline.} =
  mat[@rows, @cols]

# Assignment 
proc `[]=`*[T: AnyFloat, A, B: seq[int] | Slice[int]](mat: var Matrix[T], rows: A, cols: B, val: Matrix[T]) = 
  when A is seq[int] and B is seq[int]:
    for ridx, r in rows:
      for cidx, c in cols:
        mat[r, c] = val[ridx, cidx]
  elif A is seq[int] and B is Slice[int]:
    for ridx, r in rows:
      for cidx, c in cols.toseq:
        mat[r, c] = val[ridx, cidx]
  elif A is Slice[int] and B is seq[int]:
    for ridx, r in rows.toseq:
      for cidx, c in cols:
        mat[r, c] = val[ridx, cidx]
  elif A is Slice[int] and B is Slice[int]:
    for ridx, r in rows.toseq:
      for cidx, c in cols.toseq:
        mat[r, c] = val[ridx, cidx]
  else:
    echo "ERR NOT IMPLEMENTED `[]=` for " & $A & " " & $B


# array overload
proc `[]=`*[T: AnyFloat, containersize : static[int]](mat: var Matrix[T], rows, cols: array[containersize, int], val: Matrix[T]) {.inline.} =
  mat[@rows, @cols] = val

proc `[]=`*[T: AnyFloat, containersize: static[int]](mat: var Matrix[T], rows: array[containersize, int], cols: Slice[int], val: Matrix[T]): Matrix[T] {.inline.} =
  mat[@rows, cols] = val

proc `[]=`*[T: AnyFloat, containersize: static[int]](mat: var Matrix[T], rows: Slice[int], cols: array[containersize, int], val: Matrix[T]): Matrix[T]  {.inline.} =
  mat[rows, @cols] = val


proc row*[T: AnyFloat](mat: Matrix[T], r: int) : Vector[T] =
  result = Vector[T](len: mat.cols)
  for c in 0 ..< mat.cols:
    result.data.add mat[r, c]

proc col*[T: AnyFloat](mat: Matrix[T], c: int) : Vector[T] = 
  result = Vector[T](len: mat.rows)
  for r in 0 ..< mat.rows:
    result.data.add mat[r, c]

proc `[]`*[T: AnyFloat, container: seq[int] | Slice[int]](v: Vector[T], indices: container): Vector[T] =
  result = vector[T](indices.len)
  when container is seq[int]:
    for i, idx in indices:
        result[i] = v[idx] 
  else:
    for i, idx in indices.toseq:
        result[i] = v[idx] 


proc `[]`*[T: AnyFloat, containersize: static[int]](v: Vector[T], indices: array[containersize, int]): Vector[T] {.inline.}  =
  v[@indices]

proc `[]=`*[T: AnyFloat, container: seq[int] | Slice[int]](v: var Vector[T], indices: container, val: Vector[T]) =
  when container is seq[int]:
    for i, idx in indices:
      v[idx] = val[i] 
  else:
    for i, idx in indices.toseq:
      v[idx] = val[i] 

proc `[]=`*[T: AnyFloat, containersize: static[int]](v: var Vector[T], indices: array[containersize, int], val: Vector[T]) {.inline.}  =
  v[@indices] = val

# random stuff that is a hack atm
# converter to_complex*[T: SomeFloat](num: T): Complex[T] {.inline.} =
#   complex(num)

proc sum*[T: SomeFloat](v: Vector[T]): T = 
  for i in 0 ..< v.len:
    result += v[i]

proc l1norm*[T: SomeFloat](v: Vector[T]): T = 
  for i in 0 ..< v.len:
    result += v[i]

proc l2norm*[T: SomeFloat](m: Matrix[Complex[T]]): T =
  result = 0.0
  for r in 0 ..< m.rows:
    for c in 0 ..< m.cols:
      result += (m[r,c]).abs2
  return sqrt(result)

proc l2norm*[T: SomeNumber](m: Matrix[T]): T =
  result = 0.0
  for r in 0 ..< m.rows:
    for c in 0 ..< m.cols:
      result += (m[r,c])^2
  return sqrt(result)

proc approx_eq*[T: AnyFloat](a, b: T): bool {.inline.} =
  abs(a - b) <= (1e-8 * max(abs(a), abs(b)))

proc `~`*[T: AnyFloat](a, b: T): bool {.inline.} =
  approx_eq(a, b)

proc `~`*[T: AnyFloat](m1, m2: Matrix[T]): bool =
  # echo "~ l2 norm"
  let v = l2norm(m1 - m2)
  # echo "l2norm( m1 - m2 )=  ", v
  return v <= (1e-5 * max(m1.l2norm, m2.l2norm))

proc `~`*[T: AnyFloat](v1, v2: Vector[T]): bool =
  for c in 0 ..< v1.len:
    if not (v1[c] ~ v2[c]): return false
  return true;

proc `~=`*[T](a, b: T): bool =
  a ~ b


converter toMatrixStorageOrder*(s: string): MatrixStorageOrder = 
  parseEnum[MatrixStorageOrder](s)

converter MatrixStorageOrdertoString*(so: MatrixStorageOrder): string = 
  $so

proc `%`*[T: MatrixStorageOrder](o: T): JsonNode =
  % $o

proc `%`*[T: Complex[SomeFloat]](o: T): JsonNode =
  ## Construct JsonNode from tuples and objects.
  result = newJObject()
  for k, v in o.fieldPairs: result[k] = %v

# TODO We can optimize by removing the converter
# and rather than casting every realt type to complex first, 
# for assignment we can just write the real part in 
# and use std\complex operator overloads for real * complex etc
converter toComplex*[T: SomeFloat](m: Matrix[T]): Matrix[Complex[T]] = 
  result = matrix[Complex[T]](m.rows, m.cols, m.data.mapit(it.complex))

converter toComplex*[T: SomeFloat](m: SparseMatrix[T]): SparseMatrix[Complex[T]] = 
  result = SparseMatrix[Complex[T]](matrix: m.matrix.to_complex(), modes: m.modes)

converter toComplex*[T: SomeFloat](v: Vector[T]): Vector[Complex[T]] =
  result = vector[Complex[T]](v.data.mapit(it.complex))

proc clone*[T: AnyFloat](v: Vector[T]): Vector[T] =
  var dcopy = v.data
  return vector(dcopy)

proc clone*[T: AnyFloat](m: Matrix[T]): Matrix[T] =
  var dcopy = m.data
  return matrix(m.rows, m.cols, dcopy)

# Useful function for processing click patterns
# Converts a click pattern to sequence of modes
# e.g. [0, 2, 1, 0] becomes [1, 1, 2]
proc getRepeatedModes*(clicks: seq[int]): seq[int] =
  let numterms = sum(clicks)
  result = newSeqOfCap[int](numterms)
  for idx in 0 ..< clicks.len:
    for num_ps in 0 ..< clicks[idx]:
      result.add idx

# Matrix ops 
proc Transpose*[T: AnyFloat](m: Matrix[T]): Matrix[T] =
  result = m
  for r in 0 ..< m.rows:
    for c in 0 ..< m.cols:
      result[c, r] = m[r, c]

proc Tinplace*[T: AnyFloat](m: var Matrix[T]) = 
  let mcopy = m.clone
  for r in 0 ..< m.rows:
    for c in 0 ..< m.cols:
      m[c, r] = mcopy[r, c]

proc H*[T: AnyFloat](m: Matrix[T]): Matrix[T] =
  result = m
  for r in 0 ..< m.rows:
    for c in 0 ..< m.cols:
      result[c, r] = conjugate m[r, c]

proc Hinplace*[T: AnyFloat](m: var Matrix[T]) = 
  let mcopy = m.clone
  for r in 0 ..< m.rows:
    for c in 0 ..< m.cols:
      m[c, r] = conjugate mcopy[r, c]

proc conj*[T: AnyFloat](m: Matrix[T]): Matrix[T] = 
  result = m
  for r in 0 ..< m.rows:
    for c in 0 ..< m.cols:
      result[r,c] = result[r,c].conjugate()

proc conjinplace*[T: AnyFloat](m: var Matrix[T]) = 
  for r in 0 ..< m.rows:
    for c in 0 ..< m.cols:
      m[r,c] = m[r,c].conjugate()

proc trace*[T: AnyFloat](m: Matrix[T]): T = 
  result = 0.0.toType(T)
  for i in 0 ..< m.rows:
    result += m[i,i]

proc matrix_power*[T: AnyFloat](m: Matrix[T], power: int): Matrix[T] =
  result = Identity[T](m.rows)
  for i in 1 .. power:
    result = result * m

# sparse matrix ops 
proc Transpose*[T: AnyFloat](m: SparseMatrix[T]): SparseMatrix[T] =
  result = SparseMatrix[T](matrix: m.matrix.Transpose, modes: m.modes)

proc Tinplace*[T: AnyFloat](m: var SparseMatrix[T]) = 
  m.matrix.Tinplace

proc H*[T: AnyFloat](m: SparseMatrix[T]): SparseMatrix[T] =
  result = SparseMatrix[T](matrix: m.matrix.H, modes: m.modes)

proc Hinplace*[T: AnyFloat](m: var SparseMatrix[T]) = 
  m.matrix.Hinplace

proc conj*[T: AnyFloat](m: SparseMatrix[T]): SparseMatrix[T] = 
  result = SparseMatrix[T](matrix: m.matrix.conj, modes: m.modes)

proc conjinplace*[T: AnyFloat](m: var SparseMatrix[T]) = 
  m.matrix.conjinplace

proc abs*[T: SomeFloat](mat: Matrix[Complex[T]]): Matrix[T] = 
  result = matrix[T](mat.rows, mat.cols)
  for r in 0 ..< result.rows:
    for c in 0 ..< result.cols:
      result[r,c] = mat[r,c].abs

proc abs*[T: SomeFloat](mat: SparseMatrix[Complex[T]]): SparseMatrix[T] = 
  result = sparsematrix[T](mat.matrix.abs, mat.modes)

proc chop*[T: AnyFloat](a: T): T {.inline.} =
  when T is SomeFloat:
    if a.abs < T(1e-8): return T(0.0) 
    else: return a
  else: # complex
    let re = a.re.chop
    let im = a.im.chop
    return complex(re, im)

proc chop*[T: AnyFloat](mat: Matrix[T]): Matrix[T] = 
  result = matrix[T](mat.rows, mat.cols)
  for r in 0 ..< result.rows:
    for c in 0 ..< result.cols:
      result[r,c] = mat[r,c].chop

# Arithmetic

# ======== + 
proc `+=`*[T: AnyFloat, Y: AnyFloat](lhs: var Matrix[T], rhs: Matrix[Y]) =
  for r in 0 ..< lhs.rows:
    for c in 0 ..< lhs.cols:
      lhs[r, c] += rhs[r, c]

proc `+`*[T: AnyFloat, Y: AnyFloat](lhs: Matrix[T], rhs: Matrix[Y]): Matrix[T] =
  result = lhs
  result += rhs

proc `+=`*[T: AnyFloat, Y: AnyFloat](lhs: var Vector[T], rhs: Vector[Y]) =
  for idx in 0 ..< lhs.len:
    lhs[idx] += rhs[idx]

proc `+`*[T: AnyFloat, Y: AnyFloat](lhs: Vector[T], rhs: Vector[Y]): Vector[T] =
  result = lhs
  result += rhs

# ======== -
proc `-=`*[T: AnyFloat, Y: AnyFloat](lhs: var Matrix[T], rhs: Matrix[Y]) =
  for r in 0 ..< lhs.rows:
    for c in 0 ..< lhs.cols:
      lhs[r, c] -= rhs[r, c]

proc `-`*[T: AnyFloat, Y: AnyFloat](lhs: Matrix[T], rhs: Matrix[Y]): Matrix[T] =
  result = lhs
  result -= rhs

proc `-`*[T: AnyFloat](lhs: Matrix[T]): Matrix[T] =
  result = lhs
  for r in 0 ..< lhs.rows:
    for c in 0 ..< lhs.rows:
      result[r,c] = -result[r,c]

proc `-=`*[T: AnyFloat, Y: AnyFloat](lhs: var Vector[T], rhs: Vector[Y]) =
  for idx in 0 ..< lhs.len:
    lhs[idx] -= rhs[idx]

proc `-`*[T: AnyFloat](lhs: Vector[T], rhs: Vector[T]): Vector[T] =
  result = lhs
  result -= rhs

proc `-`*[T: AnyFloat](lhs: Vector[T]): Vector[T] =
  result = lhs
  for r in 0 ..< lhs.len:
    result[r] = -result[r]

# ======== *
# matrix matrix product 
proc `*=`*[T: AnyFloat, Y: AnyFloat](lhs: var Matrix[T], rhs: Matrix[Y]) = 
  # lhs *= rhs is lhs = lhs * rhs
  var lhscopy = lhs
  assert lhs.cols == rhs.rows
  # if we must resize
  if rhs.cols > lhs.cols:
    lhs = matrix[T](lhs.rows, rhs.cols)

  for r in 0 ..< lhs.rows:
    # var row_copy = lhs.row(r)
    for c in 0 ..< lhs.cols:
      lhs[r,c] = 0.0.toType(T)
      for k in 0 ..< rhs.rows:
        lhs[r, c] += lhscopy[r, k] * rhs[k, c]

proc `*`*[T: AnyFloat, Y: AnyFloat](lhs: Matrix[T], rhs: Matrix[Y]): Matrix[T] = 
  result = lhs
  result *= rhs

# sparse matrix matrix product operations 
proc `*`*[T: AnyFloat, Y: AnyFloat](lhs: SparseMatrix[T], rhs: Matrix[Y]): Matrix[T] = 
  # lhs * rhs
  #       [a b 0] [e f g]   [ae+bh af+bi ag+bj]
  #     = [c d 0] [h i j] = [ce+dh cf+di cg+dj]
  #       [0 0 1] [k l m]   [  k     l     m  ]

  # take subblocks of lhs
  var rhscopy = rhs[lhs.modes, 0..<rhs.cols]
  var prod = lhs.matrix * rhscopy
  result = rhs
  result[lhs.modes, 0..<rhs.cols] = prod

proc `*`*[T: AnyFloat](lhs: Matrix[T], rhs: SparseMatrix[T]): Matrix[T] =
  # lhs * rhs
  # from the right,
  # [a b c] [j k 0]   [aj+bl ak+vm c]
  # [d e f] [l m 0] = [dj+el dk+em f]
  # [g h i] [0 0 1]   [gj+hl gk+hm i]
  # take subblocks of lhs
  var lhscopy = lhs[0..<lhs.rows, rhs.modes]
  var prod = lhscopy * rhs.matrix
  result = lhs
  result[0..<lhs.rows, rhs.modes] = prod

# sparse vector product 
proc `*=`*[T: AnyFloat, Y: AnyFloat](lhs: var Vector[T], rhs: SparseMatrix[Y]) =
  # v = m * v
  # [a b]  [v0]
  # [c d]  [v1]
  #        [v2]
  # take subblock of v that matches modes and update in place 
  var subv = lhs[rhs.modes]
  subv *= rhs.matrix
  lhs[rhs.modes] = subv

proc `*`*[T: AnyFloat](lhs: Vector[T], rhs: SparseMatrix[T]): Vector[T] = 
  result = lhs
  result *= rhs

# matrix vector product
proc `*=`*[T: AnyFloat, Y: AnyFloat](lhs: var Vector[T], rhs: Matrix[Y]) = 
  var vcopy = lhs
  for r in 0 ..< rhs.rows:
    lhs[r] = 0.0.toType(T)
    for c in 0 ..< rhs.cols:
      lhs[r] += rhs[r, c] * vcopy[c]

# m * v == v *= m
proc `*`*[T: AnyFloat, Y: AnyFloat](lhs: Matrix[T], rhs: Vector[Y]): Vector[Y] = 
  result = rhs
  result *= lhs

# vector matrix product
# v * m = vec == m *= v
proc `*`*[T: AnyFloat](lhs: Vector[T], rhs: Matrix[T]): Vector[T] = 
  result = lhs
  for c in 0 ..< rhs.cols:
    result[c] = 0.0.toType(T)
    for r in 0 ..< lhs.len:
      result[c] += lhs[r] * rhs[r,c]

# vector vector product 
proc `*`*[T: AnyFloat](lhs: Vector[T], rhs: Vector[T]): T = 
  for idx in 0 ..< lhs.len:
    result += lhs[idx] * rhs[idx]

# matrix *= scalar product 
proc `*=`*[T: AnyFloat, Y: AnyFloat](lhs: var Matrix[T], rhs: Y) = 
  for r in 0 ..< lhs.data.len:
    lhs.data[r] = lhs.data[r] * rhs
      # echo "ERR *= not implemented for " & $T & " " & $Y

# # scalar *= matrix product
# proc `*=`*[T: AnyFloat, Y: AnyFloat](lhs: Y, rhs: var Matrix[T]) =
#   for r in 0 ..< rhs.data.len:
#     rhs.data[r] = lhs * rhs.data[r]

# mat * scal
proc `*`*[T: AnyFloat, Y: AnyFloat](mat: Matrix[T], scal: Y): Matrix[T] = 
  result = mat
  result *= scal

# scalar * mat
proc `*`*[T: AnyFloat, Y: AnyFloat](scal: Y, mat: Matrix[T]): Matrix[T] {.inline.} =
  mat * scal

# scalar vector product 
proc `*=`*[T: AnyFloat, Y: AnyFloat](lhs: var Vector[T], rhs: Y) = 
  for i in 0 ..< lhs.len:
      lhs[i] *= T(rhs)

proc `*`*[T: AnyFloat, Y: AnyFloat](lhs: Vector[T], rhs: Y): Vector[T] = 
  result = lhs
  result *= rhs

proc `*=`*[T: AnyFloat, Y: AnyFloat](lhs: Y, rhs: var Vector[T]) = 
  for r in 0 ..< rhs.len:
      rhs[r] *= T(lhs)

proc `*`*[T: AnyFloat, Y: AnyFloat](lhs: Y, rhs: Vector[T]): Vector[T] = 
  result = rhs
  result *= lhs

proc apply*[VecT: AnyFloat, MatT: AnyFloat](lhs: var Vector[VecT], rhs: Matrix[MatT], modes: varargs[int]) = 
  # echo "modes ", modes
  # v = m * v
  # [a b]  [v0]
  # [c d]  [v1]
  #        [v2]
  # take subblock of v that matches modes and update in place 
  let s = toSeq(modes)
  let subv = lhs[s]
  lhs[s] = rhs * subv

proc apply*[T: AnyFloat, Y: AnyFloat](lhs: var Vector[T], rhs: SparseMatrix[Y]) {.inline.} = 
  lhs *= rhs

# ========= /
# scalar matrices
proc `/=`*[T: AnyFloat, Y: AnyFloat](lhs: var Matrix[T], rhs: Y) = 
  for r in 0 ..< lhs.rows:
    for c in 0 ..< lhs.cols:
      lhs[r,c] /= T(rhs)

proc `/`*[T: AnyFloat, Y: AnyFloat](lhs: Matrix[T], rhs: Y): Matrix[T] = 
  result = lhs
  result /= rhs

# scalar vectors
proc `/=`*[T: AnyFloat, Y: AnyFloat](lhs: var Vector[T], rhs: Y) = 
  for r in 0 ..< lhs.len:
    lhs[r] /= T(rhs)

proc `/`*[T: AnyFloat, Y: AnyFloat](lhs: Vector[T], rhs: Y): Vector[T] = 
  result = lhs
  result /= rhs

proc `oplus`*[T: AnyFloat](m1, m2: Matrix[T]): Matrix[T] = 
  let rowdim = m1.rows + m2.rows
  let coldim = m1.cols + m2.cols
  result = Matrix[T](rows: rowdim, cols: coldim,
  data: sequtils.repeat(0.0.toType(T), rowdim * coldim))

  for r in 0 ..< m1.rows:
    for c in 0 ..< m1.cols:
      result[r,c] = m1[r,c]

  for r in 0 ..< m2.rows:
    for c in 0 ..< m2.cols:
      result[r+m1.rows, c+m1.cols] = m2[r, c]

proc kronecker_prod*[T: AnyFloat](m1, m2: Matrix[T]): Matrix[T] = 
  let rowdim = m1.rows * m2.rows
  let coldim = m1.cols * m2.cols
  result = matrix[T](rowdim, coldim)

  # A \otimes B
  # [a b]  [e f] = [a*B b*B]   [
  # [c d]  [g h]   [c*B d*B] = [
  #                            [
  #                            [
  let blkheight = m2.rows
  let blkwidth = m2.cols
  for Arow in 0 ..< m1.rows:
    for Acol in 0 ..< m1.cols:
      let ridx = Arow * blkheight
      let cidx = Acol * blkwidth
      result[ridx..<ridx+blkheight, cidx..<cidx+blkwidth] =  m1[Arow, Acol] * m2
  
proc is_diagonal*[T: AnyFloat](mat: Matrix[T]): bool =
  let eps = 
    when T is float32 or T is Complex32: 1e-5'f32
    elif T is float64 or T is Complex64: 1e-6
  for r in 0 ..< mat.rows:
    for c in 0 ..< mat.cols:
      if r != c:
        if not (abs(mat[r, c]) < eps): return false
  return true

proc is_upper_diagonal*[T: AnyFloat](mat: Matrix[T], eps: float = 1e-7): bool =
  
  for r in 0 ..< mat.rows:
    for c in 0 ..< r:
      when T is float32 or T is Complex32:
        if (abs(mat[r,c]) > eps.float32): return false
      else: 
        if (abs(mat[r,c]) > eps): return false
  return true

proc is_orthogonal*[T: AnyFloat](mat: Matrix[T]): bool =
  let uuT = mat * mat.Transpose
  return uuT ~ Identity[T](uuT.rows)
 
proc is_unitary*[T: AnyFloat](mat: Matrix[T]): bool =
  when T is SomeFloat:
    return is_orthogonal(mat)
  else:
    let uuH = mat * mat.H
    return uuH ~ Identity[T](uuH.rows)

proc isHermitian*(mat: Matrix): bool =
  return mat ~ mat.H

proc getDiagonal*[T: AnyFloat](mat: Matrix[T]): Vector[T] =
  result = vector[T](mat.rows)
  for i in 0 ..< mat.rows:
    result[i] = mat[i,i]

# --------------------------------------------------------------------

# Pretty printing
proc `$`*[T: AnyFloat](v: Vector[T]): string =
  result = ""
  for i in 0 ..< v.len - 1:
    result &= "[ " & $(v[i]) & " ]\n"
  result &= "[ " & $(v[v.len - 1]) & " ]"

proc toStringHorizontal[T](v: Vector[T]): string =
  result = "[ "
  for i in 0 ..< (v.len - 1):
    result &= $(v[i]) & "\t"
  result &= $(v[v.len - 1]) & " ]"

proc `$`*[T: AnyFloat](m: Matrix[T]): string =
  result = ""
  for i in 0 ..< m.rows - 1:
    result &= toStringHorizontal(m.row(i)) & "\n"
  result &= toStringHorizontal(m.row(m.rows - 1)) & ""

#############################################################33

# Sparse matrix implementation 
proc multiply_reduced_left*[T: AnyFloat](lhs: SparseMatrix[T], rhs: var Matrix[T]) = 
  # lhs * rhs
  #       [a b 0] [e f g]   [ae+bh af+bi ag+bj]
  #     = [c d 0] [h i j] = [ce+dh cf+di cg+dj]
  #       [0 0 1] [k l m]   [  k     l     m  ]

  # take subblocks of lhs
  # echo "multiply_reduced_left lhs (Sparsematrix) \n", lhs
  # echo "rhs\n", rhs.rows
  let rhscopy = rhs[lhs.modes, 0 ..< rhs.cols]
  rhs[lhs.modes, 0..<rhs.cols] = lhs.matrix * rhscopy

proc multiply_reduced_right*[T: AnyFloat](lhs: var Matrix[T], rhs: SparseMatrix[T]) = 
  # lhs * rhs
  # from the right,
  # [a b c] [j k 0]   [aj+bl ak+vm c]
  # [d e f] [l m 0] = [dj+el dk+em f]
  # [g h i] [0 0 1]   [gj+hl gk+hm i]
  # take subblocks of lhs
  let lhscopy = lhs[0..<lhs.rows, rhs.modes]
  lhs[0..<lhs.rows, rhs.modes] = lhscopy * rhs.matrix

proc multiply_sparse_left*[T: AnyFloat](rhs: var Matrix[T], lhs: SparseMatrix[T]) = 
  multiply_reduced_left(lhs, rhs)

proc multiply_sparse_right*[T: AnyFloat](lhs: var Matrix[T], rhs: SparseMatrix[T]) = 
  multiply_reduced_right(lhs, rhs)

# proc save*[T](matrix: T, file: string) =
#   writeFile(file, pretty(%*matrix))

# proc load*[T](mat: var T, file: string) = 
#   # echo "load not implemented yet :/"
#   let jsonobj = parseFile(file)
#   mat = jsonobj.to T

# proc load*[T](file: string): T = 
#   # echo "Good luck!"
#   result.load(file)

# read eigen format (QGot)
proc readMatrix*[T](path: string): Matrix[T] =
  result.rows = 0
  for line in lines path:
    result.rows += 1
    let elements = line.splitWhitespace
    for e in elements:
      if result.rows == 1:
        result.cols += 1

      when T is Complex[SomeFloat]:
        let s = e.split(',')
        let re = s[0][1..^1] # skip first as (
        let im = s[1][0..^2] # skip last as )
        let c = complex(re.parseFloat, im.parseFloat)
        result.data.add c
      else:
        {.error: "readMatrix not supported for type " & $T.}


# ## TODO implement an accurate hypot for complex types
# The hypot for complex nums is going to lead to numerical errors 
proc givens_rot*[T: AnyFloat](a, b: T): Matrix[T] = 
  when T is SomeFloat: # real
    let denom = hypot(a,b)
    if denom == T(0.0):
      result = Identity[T](2)
    else:
      result = matrix(2, 2, @[a, b, -b, a])
      result /= denom
  elif T is Complex[SomeFloat]:
    let denom = sqrt(inline_abs2(a) + inline_abs2(b))
    if denom == T.T(0.0): 
      result = Identity[T](2)
    else:
      result = matrix(2, 2, @[conjugate(a), conjugate(b), -b, a])
      result /= complex(denom)

 # https://www.math.usm.edu/lambers/mat610/sum10/lecture9.pdf
proc QR*[T: AnyFloat](mat: Matrix[T]): (Matrix[T], Matrix[T]) =
  var R = mat
  var Qth = Identity[T](mat.rows)
  # r2,c0 -> r1,c0
  # r1,c0 -> r0,c0
  # r2,c1 -> r1,c1
  for c in 0 ..< mat.cols:
    for r in countdown(mat.rows - 1, c+1):

      let idx_to_zero = r
      let idx = r - 1
      # echo "r_to_zero ", idx_to_zero, " col ", c
      
      let sparse_g = SparseMatrix[T](
      matrix: givensrot(R[idx, c], R[idx_to_zero, c]),
      modes: @[idx, idx_to_zero])

      R.multiply_sparse_left( sparse_g)
      Qth.multiply_sparse_left( sparse_g)

  var Q =
    when T is SomeFloat: Qth.Transpose
    elif T is Complex[SomeFloat]: Qth.H

  # assert mat ~ Q*R
  # assert Q.isunitary
  assert R.isupperdiagonal
  return (Q, R)

proc QRunique*[T: AnyFloat](mat: Matrix[T]): (Matrix[T], Matrix[T]) =
  var (Q, R) = mat.QR
  # echo "Q before sign fix\n", Q

  # fix the sign on R
  var d = Identity[T](R.rows)
  for i in 0 ..< R.rows:
    d[i, i] = R[i,i] / abs(R[i,i])

  Q = Q * d
  when T is SomeFloat:
    R = d * R
  elif T is Complex[SomeFloat]:
    R = d.conj * R

  # assert mat ~ Q*R
  # assert Q.isunitary
  assert R.isupperdiagonal
  return (Q, R)

proc Schur*[T: AnyFloat](mat: Matrix[T], max: int = 1000): (Matrix[T], Matrix[T]) =
  # A = U T U^H
  var A = mat
  var U = Identity[T](mat.rows)
  for i in 0 ..< max * mat.rows:
    if i mod 2 == 0:
      if A.is_upper_diagonal(1e-8):
        echo "took ", i, " number of sweeps in Schur"
        break

    let (Q, R) = A.QR
    A = R * Q
    U = U * Q

  # when T is SomeFloat:
  #   assert mat ~ U * A * U.T
  # elif T is Complex[SomeFloat]:
  #   assert mat ~ U * A * U.H

  return (U, A)

proc SchurInverse*[T: AnyFloat](mat: Matrix[T]): Matrix[T] = 

  var (U, T) = mat.Schur
  # echo "\nU", U
  # echo "\nT", T
  assert T.is_diagonal
  for i in 0 ..< T.rows:
    T[i,i] = 1.0 / T[i,i]
  when T is SomeFloat:
    return U * T * U.T
  else: # complex
    return U * T * U.H

# https://en.wikipedia.org/wiki/QR_algorithm
# https://math.stackexchange.com/questions/695093/using-qr-algorithm-to-compute-the-svd-of-a-matrix
proc svd*[T: AnyFloat](mat: Matrix[T]): (Matrix[T], Matrix[T], Matrix[T]) =

  const num_iter = 100
  var Q1, R1, Q2, R2: Matrix[T]
  R1 = mat
  var U = Identity[T](mat.rows)
  var V = Identity[T](mat.rows)
  for i in 0 ..< num_iter:
    (Q1, R1) = QR(R1)
    when T is SomeFloat:
      (Q2, R2) = QR(R1.Transpose)
      R1 = R2.Transpose
    elif T is Complex[SomeFloat]:
      (Q2, R2) = QR(R1.H)
      R1 = R2.H

    U *= Q1
    V *= Q2

  var S = R1
  # var d = Identity[T](S.rows)
  # for i in 0 ..< S.rows:
  #   d[i, i] = S[i,i] / abs(S[i,i])
  # # echo d
  # S *= d
  # V *= d
  # # U *= d

  assert U.isunitary
  assert V.isunitary
  return (U, S, V)
    
# https://arxiv.org/pdf/math-ph/0609050.pdf
proc random_unitary*[T: SomeFloat](n: int): Matrix[Complex[T]] =
  result = Matrix[Complex[T]](rows: n, cols: n)
  var randstate = initRand()
  for r in 0 ..< n:
    for c in 0 ..< n:
      result.data.add complex(gauss(randstate), gauss(randstate))

  var (Q, R) = QR(result)
  var d = Identity[Complex[T]](n)
  for i in 0 ..< n:
    d[i, i] = R[i,i] / abs(R[i,i])

  result = Q * d

proc unitary_decomp*[T: AnyFloat](mat: Matrix[T]): seq[SparseMatrix[T]] = 
  var Gmats: seq[SparseMatrix[T]]

  var m = mat
  for c in 0 ..< m.cols:
    for r in countdown(m.rows - 1, c+1):

      let idx_to_zero = r
      let idx = r - 1
      
      var sparse_g = SparseMatrix[T](
      matrix: givensrot(m[idx, c], m[idx_to_zero, c]),
      modes: @[idx, idx_to_zero])

      m.multiply_sparse_left( sparse_g )

      when T is SomeFloat:
        sparse_g.matrix.Tinplace
        Gmats.add sparse_g
      elif T is Complex[SomeFloat]:
        sparse_g.matrix.Hinplace
        Gmats.add sparse_g

  # don't forget the extra phase that no one tells you about...
  let final_mat = SparseMatrix[T](
    # matrix: m.subblock(m.rows-2, m.cols-2, 2, 2),
    matrix: m[m.rows-2..<m.rows, m.cols-2..<m.cols],
    modes: @[m.rows - 2, m.rows - 1])
  
  Gmats.add final_mat
  # reverse so we can iterate through them properly for an optical circuit
  Gmats.reverse()

  return Gmats


 
proc inverse*[T: AnyFloat](mat: Matrix[T]): Matrix[T] = 
  result = mat.JacobiInverse
  # result = mat.SchurInverse
  # if mat.is_orthogonal: result = mat.Transpose
  # elif mat.is_unitary: result = mat.H
  # elif mat.is_diagonal:
  #   result = mat
  #   for r in 0 ..< result.rows:
  #     result[r, r] = 1.0 / result[r,r]
  # else: # do schur decomp
  #   var (U, T) = mat.Schur
  #   assert T.is_diagonal
  #   for i in 0 ..< T.rows:
  #     T[i,i] = 1.0 / T[i,i]
  #   result = U * T * U.H

proc eigenvalues*[T: AnyFloat](mat: Matrix[T]): seq[T] =
  let (U,S) = mat.JacobiEigenDecomp
  result = S.getDiagonal.data

proc JacobiStep*[T: AnyFloat](m: Matrix[T]): Matrix[T] = 
  # see https://web.stanford.edu/class/cme335/lecture7.pdf
  #
  # [ c s ]T [ a b ] [ c s]   [ e 0 ]
  # [-s c ]  [ b d ] [-s c] = [ 0 f ]
  #
  # tau = (d - a) / 2b
  # t = s/c
  # t = -tau \pm sqrt(1+tau^2) = tan(\theta)
  #
  # need to pick the smallest |t| to ensure |\theta| <= pi/4 for
  # convergence
  # c = 1 / sqrt(1 + t^2), 
  # s = c * t
  when T is SomeFloat: # real
    if m[0,1] == 0.0.T: 
      return Identity[T](2,2)
    let tau = (m[1,1] - m[0,0]) / (2.0.T * m[0,1]) 
    let tplus = -tau + sqrt(1.0.T + tau^2)
    let tminus = -tau - sqrt(1.0.T + tau^2)

    let t = 
      if tminus.abs < tplus.abs: tminus
      else: tplus

    let c = 1.0.T / (sqrt(1.0.T + t^2))
    let s = c * t

    let J = matrix[T](2,2, @[c, -s, s, c])
    return J
  elif T is Complex[SomeFloat]:
    # https://en.wikipedia.org/wiki/Jacobi_method_for_complex_Hermitian_matrices
    # https://arxiv.org/pdf/physics/0607103.pdf
    type realt = T.T
    let delta = 0.5.realt * (m[0,0].re - m[1,1].re)
    # as hermitian so m[0,1] == m[1,0]^H
    let d = sqrt(delta^2 + m[0,1].inline_abs2)

    let denom =
      if delta + d > delta - d: complex(delta + d)
      else: complex(delta - d)
    let t1 = m[0,1] / denom

    let c = 1.0.realt / sqrt(1.0.realt + t1.inline_abs2)
    let J = matrix[T](2,2, @[c.complex, t1*c, -t1.conjugate * c, c.complex])
    return J
  else:
    {.error "ERR JacobiStep not implemented for " & $T .}


# returns m = G S G^T for real or m = G S G^H for complex 
proc JacobiEigenDecomp*[T: AnyFloat](mat: Matrix[T], maxsweeps: int = 2): (Matrix[T], Matrix[T]) =

  var A = mat
  var J = Identity[T](mat.rows, mat.cols)

  for numsweeps in 0 ..< maxsweeps * mat.rows:
    if A.is_upper_diagonal(1e-6): 
      echo "took ", numsweeps, " number of sweeps in JacobiEigenvalue"
      break
    
    for r in 0 ..< mat.rows:
      for c in r+1 ..< mat.cols:

        when T is SomeFloat:
          if A[r,c].abs < 1e-8.T:
            continue 
        elif T is Complex[SomeFloat]:
          if A[r,c].inline_abs2 < T.T(1e-16):
            continue

        let modes = @[r, c]
        let smallJ = A[modes, modes].JacobiStep
        # echo "small A \n", A[modes, modes]

        let sparse_g = SparseMatrix[T](matrix:smallJ, modes: modes)
        # echo sparse_g

        J.multiply_sparse_left sparse_g

        A.multiply_sparse_left sparse_g
        when T is SomeFloat:
          A.multiply_sparse_right sparse_g.Transpose
        else: 
          A.multiply_sparse_right sparse_g.H
        
        # echo "\nJ\n", J
        # echo "A\n", A

  return (J, A)

proc diag_inverse*[T: AnyFloat](mat: Matrix[T]): Matrix[T] =
  result = matrix[T](mat.rows, mat.cols)
  for i in 0 ..< result.rows:
    result[i,i] = 1.0 / mat[i,i]

proc swapBlocksInPlace*[T: AnyFloat](mat: var Matrix[T]) =
  let nrows = mat.rows div 2
  let ncols = mat.cols div 2
  assert (2 * nrows) == mat.rows
  assert (2 * ncols) == mat.cols

  let topleft = mat[0..<nrows, 0..<ncols]
  mat[0..<nrows, 0..<ncols] = mat[0..<nrows, ncols..<2*ncols]
  mat[0..<nrows, ncols..<2*ncols] = topleft

  let botleft = mat[nrows..<2*nrows, 0..<ncols]
  mat[nrows..<2*nrows, 0..<ncols] = mat[nrows..<2*nrows, ncols..<2*ncols]
  mat[nrows..<2*nrows, ncols..<2*ncols] = botleft

proc swapBlocks*[T: AnyFloat](mat: Matrix[T]): Matrix[T] =
  result = mat
  result.swapBlocksInPlace

proc swapRowsInPlace*[T: AnyFloat](mat: var Matrix[T]) =
  let nrows = mat.rows div 2
  assert (2 * nrows) == mat.rows
  let ncols = mat.cols

  let top = mat[0..<nrows, 0..<ncols]
  let bot = mat[nrows..<2*nrows, 0..<ncols]
  mat[0..<nrows, 0..<ncols] = bot
  mat[nrows..<2*nrows, 0..<ncols] = top

proc swapRows*[T: AnyFloat](mat: Matrix[T]): Matrix[T] =
  result = mat
  result.swapRowsInPlace

proc JacobiInverse*[T: AnyFloat](mat: Matrix[T]): Matrix[T] =
  let (J, A) = mat.JacobiEigenDecomp
  when T is SomeFloat:
    return J.Transpose * A.diag_inverse * J
  else: # cmplx
    return J.H * A.diag_inverse * J

# proc GaussElim*[T: SomeFloat](mat: Matrix[T])
 
proc determinant*[T: AnyFloat](mat: Matrix[T]): T =
  let (Q, R) = mat.QR
  result = 1.0.toType(T)
  for i in 0 ..< R.rows:
    result *= R[i,i]

# does a * conjugate(b)
proc conj_mul*[T: Complex[SomeFloat]](a, b: T): T {.inline.} =
  result.re = a.re * b.re + a.im * b.im
  result.im = a.im * b.re - a.re * b.im

proc inline_abs2*[T: SomeFloat](a: Complex[T]): T {.inline.} =
  result = a.re * a.re + a.im * a.im

proc chol_ll_ban_determinant*[T: SomeFloat](mat: Matrix[Complex[T]]): T = 
  result = T(1.0)

  var L = matrix[Complex[T]](mat.rows, mat.rows)
  for r in 0 ..< mat.rows:
    for c in 0 .. r:
      var sum = complex(T(0.0))
      for k in 0 ..< c:
        # sum += L[r, k] * L[c, k].conjugate
        sum += conj_mul(L[r, k], L[c, k])

      const half: T = T(0.5)
      if r == c:
        L[r, r] = sqrt(half * mat[r,r] + half - sum)
        result *= L[r,r].re
      else:
        L[r, c] = (half * mat[r, c] - sum) / L[c, c]

# proc Q_inv_sqrt(num: float32): float32 =
#   var y = cast[float32](0x5f3759df - (cast[uint32](num) shr 1))
#   result = y * (1.5'f32 - (num * 0.5'f32 * y * y))


proc chol_ldl_crout_determinant*[T: SomeFloat](mat: Matrix[Complex[T]]): T = 
  let dim = mat.rows

  var L = matrix[Complex[T]](dim, dim)
  var D = newSeq[T](dim)
  
  result = T(1.0)

  for c in 0 ..< dim:
    var sum = T(0.0)
    for k in 0 ..< c:
      # sum += L[c, k] * conjugate(L[c,k]) * D[k]
      sum += inline_abs2(L[c, k]) * D[k]

    const half: T = T(0.5)
    D[c] = (half * mat[c, c] + 0.5 - sum).re
    result *= D[c]

    for r in c+1 ..< dim:
      var csum = complex(T(0.0))
      for k in 0 ..< c:
        # sum += L[r, k] * conjugate(L[c, k]) * D[k]
        csum += conj_mul(L[r, k], L[c, k]) * D[k]

      L[r,c] = (half * mat[r,c] - csum) / D[c]

proc chol_ldl_crout_determinant*[T: SomeFloat, int_t: SomeInteger](mat: Matrix[Complex[T]], rows_cols: openArray[int_t]): T =
  let dim = rows_cols.len

  var L = matrix[Complex[T]](dim, dim)
  var D = newSeq[T](dim)
  
  result = 1.0

  const zero = T(0.0)

  for c in 0 ..< dim:
    var sum = T(0.0)
    for k in 0 ..< c:
      # sum += L[c, k] * conjugate(L[c,k]) * D[k]
      sum += inline_abs2(L[c, k]) * D[k]

    const half = T(0.5)

    let col_idx = rows_cols[c]
    D[c] = (half * mat[col_idx, col_idx] - sum).re + half
    result *= D[c]

    for r in c+1 ..< dim:
      var csum = complex(T(0.0)) 
      
      for k in 0 ..< c:
        # sum += L[r, k] * conjugate(L[c, k]) * D[k]
        csum += conj_mul(L[r, k], L[c, k]) * D[k]

      let row_idx = rows_cols[r]
      L[r,c] = (half * mat[row_idx, col_idx] - csum) / D[c]

# pass in first half of modes and spatial size 
proc chol_ldl_crout_determinant*[T: SomeFloat, int_t: SomeInteger](mat: Matrix[Complex[T]], rows_cols: openArray[int_t], spacedim: int_t): T =
  let dim = rows_cols.len
  let twodim = 2 * dim

  var L = matrix[Complex[T]](twodim, twodim)
  var D = newSeq[T](twodim)
  
  result = 1.0

  for c in 0 ..< twodim:
    var sum = T(0.0)
    for k in 0 ..< c:
      # sum += L[c, k] * conjugate(L[c,k]) * D[k]
      sum += inline_abs2(L[c, k]) * D[k]

    let col_idx = 
      if c < dim: rows_cols[c]
      else: rows_cols[c - dim] + spacedim

    const half = T(0.5)
    D[c] = (half * mat[col_idx, col_idx] - sum).re + half
    result *= D[c]

    for r in c+1 ..< twodim:
      var csum = complex(T(0.0)) 
      
      for k in 0 ..< c:
        # sum += L[r, k] * conjugate(L[c, k]) * D[k]
        csum += conj_mul(L[r, k], L[c, k]) * D[k]

      let row_idx =
        if r < dim: rows_cols[r]
        else: rows_cols[r - dim] + spacedim

      L[r,c] = (half * mat[row_idx, col_idx] - csum) / D[c]

# pass in first half of modes and spatial size 
proc chol_ldl_crout_determinant_split_for*[T: SomeFloat, int_t: SomeInteger](mat: Matrix[Complex[T]], rows_cols: openArray[int_t], spacedim: int_t): T =
  let dim = rows_cols.len
  let twodim = 2 * dim

  var L = matrix[Complex[T]](twodim, twodim)
  var D = newSeq[T](twodim)

  for c in 0 ..< twodim:
    var sum = T(-0.5)
    for k in 0 ..< c:
      sum += inline_abs2(L[c, k]) * D[k]

    let col_idx = 
      if c < dim: rows_cols[c]
      else: rows_cols[c - dim] + spacedim

    const half = T(0.5)
    D[c] = half * mat[col_idx, col_idx].re - sum

    for r in c+1 ..< twodim:
      var csum = complex(T(0.0)) 
      
      for k in 0 ..< c:
        # sum += L[r, k] * conjugate(L[c, k]) * D[k]
        csum += conj_mul(L[r, k], L[c, k]) * D[k]

      let row_idx =
        if r < dim: rows_cols[r]
        else: rows_cols[r - dim] + spacedim

      L[r,c] = (half * mat[row_idx, col_idx] - csum) / D[c]

  result = 1.0
  for c in 0 ..< twodim:
    result *= D[c]


# 
# a00 a01 a02 a03
# a10 a11 a12 a13
# a20 a21 a22 a23
# a30 a31 a32 a33
#
# if we add x = a03, a13, a23, a33
#
# from 
# https://docs.juliahub.com/NormalHermiteSplines/KDJDk/0.3.0/Updating-Cholesky-Factorization/
# 
# A = L L^H
#
# B =  A  d
#     d^H y
#
# L' =  L
#      e^H a
#
# https://en.wikipedia.org/wiki/Cholesky_decomposition#Updating_the_decomposition
# proc chol_update*[T: SomeFloat](L: Matrix[Complex[T]], X: Vector[Complex[T]]) = 

proc nextsetbit*(num: uint64, pos: int, setbits = 64): int = 
  result = pos
  while result < setbits:
    if num.testbit(result):
      return result
    result += 1


# pass in first half of modes and spatial size 
proc chol_ldl_crout_determinant_packed*[T: SomeFloat](mat: Matrix[Complex[T]], rows_cols, rows_cols_idx: seq[int], spacedim: int): T =
  let dim = rows_cols_idx.len
  let twodim = 2 * dim

  var L = matrix[Complex[T]](twodim, twodim)
  var D = newSeq[T](twodim)
  
  result = 1.0

  for c in 0 ..< twodim:
    var sum = T(0.0)
    for k in 0 ..< c:
      sum += inline_abs2(L[c, k]) * D[k]

    let col_idx = 
      if c < dim: rows_cols[rows_cols_idx[c]]
      else: rows_cols[rows_cols_idx[c - dim]] + spacedim

    const half = T(0.5)
    D[c] = (half * mat[col_idx, col_idx] - sum).re + half
    result *= D[c]

    for r in c+1 ..< twodim:
      var csum = complex(T(0.0)) 
      
      for k in 0 ..< c:
        # sum += L[r, k] * conjugate(L[c, k]) * D[k]
        csum += conj_mul(L[r, k], L[c, k]) * D[k]

      let row_idx = 
        if r < dim: rows_cols[rows_cols_idx[r]]
        else: rows_cols[rows_cols_idx[r - dim]] + spacedim

      L[r,c] = (half * mat[row_idx, col_idx] - csum) / D[c]

# pass in first half of modes and spatial size 
proc chol_ldl_crout_determinant_packed_full*[T: SomeFloat](mat: Matrix[Complex[T]], rows_cols, rows_cols_idx: seq[int]): T =
  let dim = rows_cols_idx.len
  let twodim = dim

  var L = matrix[Complex[T]](twodim, twodim)
  var D = newSeq[T](twodim)
  
  result = 1.0

  for c in 0 ..< twodim:
    var sum = T(0.0)
    for k in 0 ..< c:
      sum += inline_abs2(L[c, k]) * D[k]

    let col_idx = rows_cols[rows_cols_idx[c]]

    const half = T(0.5)
    D[c] = (half * mat[col_idx, col_idx] - sum).re + half
    result *= D[c]

    for r in c+1 ..< twodim:
      var csum = complex(T(0.0)) 
      
      for k in 0 ..< c:
        # sum += L[r, k] * conjugate(L[c, k]) * D[k]
        csum += conj_mul(L[r, k], L[c, k]) * D[k]

      let row_idx = rows_cols[rows_cols_idx[r]]

      L[r,c] = (half * mat[row_idx, col_idx] - csum) / D[c]

proc pow_trace[T: AnyFloat](m: Matrix[T], power: int): T =
  result = 0.0.toType(T) 
  for eig in eigenvalues(m):
    result += pow(eig, power.toType(T))

proc perm_slow*[T: AnyFloat](m: Matrix[T]): T =
  var permutation = toseq(0 ..< m.rows)
  var valid = true
  while valid:
    # echo permutation
    var prod = 1.0.toType(T)

    for j in 0 ..< m.rows:
      # echo "r ", permutation[j], " c ", j
      prod *= m[permutation[j], j]
    # echo " + "
    result += prod
    valid = permutation.nextpermutation()

# RYSER IMPL
proc perm*[T: AnyFloat](mat: Matrix[T]): T =
  # (-1)^n \sum_{S in [1 .. n]} (-1)^|S| \prod^n_{i=1} \sum_{j in S} aij
  # see wikipedia

  let numterms = 1 shl mat.rows
  let modes = toseq[0 ..< mat.rows]
  for sidx in  1 ..< numterms:
    let S = lexi_powerset(modes, sidx.bintogray)
    # echo "S ", S
    var prod = 1.0.toType(T)
    for i in 0 ..< mat.cols:
      var sum = 0.0.toType(T)
      for j in 0 ..< S.len:
        sum += mat[i, S[j]]
      prod *= sum
    if (S.len mod 2) == 0:
      result += prod
    else:
      result -= prod

# pass full matrix and rows, cols separately 
# RYSER IMPL
proc perm*[T: AnyFloat](mat: Matrix[T], row_idx, col_idx: seq[int]): T =
  # (-1)^n \sum_{S in [1 .. n]} (-1)^|S| \prod^n_{i=1} \sum_{j in S} aij
  # see wikipedia

  let numterms = 1 shl row_idx.len
  for sidx in  1 ..< numterms:
    let S = lexi_powerset(col_idx, sidx.bintogray)
    # echo "S ", S
    var prod = 1.0.toType(T)
    for i in row_idx:
      var sum = 0.0.toType(T)
      for j in S:
        sum += mat[i, j]
      prod *= sum
    if (S.len mod 2) == 0:
      result += prod
    else:
      result -= prod


# RYSER IMPL
proc perm_parallel*[T: AnyFloat](mat: Matrix[T]): T =
  # (-1)^n \sum_{S in [1 .. n]} (-1)^|S| \prod^n_{i=1} \sum_{j in S} aij
  # see wikipedia
  let numterms = 1 shl mat.rows
  let modes = toseq[0 ..< mat.rows]

  let halfterms = int(num_terms div 2)
  let Output = newseq[T](half_terms)
  let bufout = cast[ptr UncheckedArray[T]](Output[0].unsafeAddr)

  let S = lexi_powerset(modes, 1.bintogray)
  var prod = 1.0.toType(T)
  for i in 0 ..< mat.cols:
    var sum = 0.0.toType(T)
    for j in 0 ..< S.len:
      sum += mat[i, S[j]]
    prod *= sum
  bufout[0] = -prod

  init(Weave)
  parallelFor idx in 1 ..< halfterms:
    captures: {modes, mat, bufout}

    let S = lexi_powerset(modes, (2*idx).bintogray)
    var prod = 1.0.toType(T)
    for i in 0 ..< mat.cols:
      var sum = 0.0.toType(T)
      for j in 0 ..< S.len:
        sum += mat[i, S[j]]
      prod *= sum
    # bufout[idx] = prod

    let S2 = lexi_powerset(modes, (2*idx+1).bintogray)
    var prod2 = 1.0.toType(T)
    for i2 in 0 ..< mat.cols:
      var sum2 = 0.0.toType(T)
      for j2 in 0 ..< S2.len:
        sum2 += mat[i2, S2[j2]]
      prod2 *= sum2
    bufout[idx] = prod - prod2

  exit(Weave)

  # echo "bufout \n"
  # for i in 0 ..< num_terms div 2:
  #   echo bufout[i]
  for i in 0 ..< halfterms:
    result += bufout[i]


proc perm_adaptive*[T: AnyFloat](mat: Matrix[T]): T {.inline.} =
  if mat.rows <= 13: perm(mat)
  else: perm_parallel(mat)

# pass full matrix and rows, cols separately 
# RYSER IMPL
proc perm_parallel*[T: AnyFloat](mat: Matrix[T], row_idx, col_idx: seq[int]): T =
  # (-1)^n \sum_{S in [1 .. n]} (-1)^|S| \prod^n_{i=1} \sum_{j in S} aij
  # see wikipedia
  let numterms = 1 shl row_idx.len
  let halfterms = int(num_terms div 2)
  let Output = newseq[T](half_terms)
  let bufout = cast[ptr UncheckedArray[T]](Output[0].unsafeAddr)

  let S = lexi_powerset(col_idx, 1.bintogray)
  var prod = 1.0.toType(T)
  for i in 0 ..< row_idx.len:
    var sum = 0.0.toType(T)
    for j in 0 ..< S.len:
      sum += mat[row_idx[i], S[j]]
    prod *= sum
  bufout[0] = -prod

  init(Weave)
  parallelFor idx in 1 ..< halfterms:
    captures: {row_idx, col_idx, mat, bufout}

    let S = lexi_powerset(col_idx, (2*idx).bintogray)
    var prod = 1.0.toType(T)
    for i in 0 ..< row_idx.len:
      var sum = 0.0.toType(T)
      for j in 0 ..< S.len:
        sum += mat[row_idx[i], S[j]]
      prod *= sum
    # bufout[idx] = prod

    let S2 = lexi_powerset(col_idx, (2*idx+1).bintogray)
    var prod2 = 1.0.toType(T)
    for i2 in 0 ..< row_idx.len:
      var sum2 = 0.0.toType(T)
      for j2 in 0 ..< S2.len:
        sum2 += mat[row_idx[i2], S2[j2]]
      prod2 *= sum2
    bufout[idx] = prod - prod2

  exit(Weave)

  # echo "bufout \n"
  # for i in 0 ..< num_terms div 2:
  #   echo bufout[i]
  for i in 0 ..< halfterms:
    result += bufout[i]

proc hafnian_slow*[T: AnyFloat](mat: Matrix[T]): T =
  # https://en.wikipedia.org/wiki/Hafnian
  # 1 / (n! * 2^n) \sum_{k \in S_{2n}} \prod_{j=1}^n A[ k[2j-1],      k[2j] ]
  # where S_{2n} is the symetric group [1,2,...,2n]
  let halfN = mat.rows div 2

  var answer = 0.0.toType(T)
  let factor = (fac(halfN) * 2^halfN).toType(T)

  var permutation = toSeq(0..<2*halfN)
  var valid = true
  while valid:
    var val = 1.0.toType(T)

    for i in 0 ..< halfN:
      val *= mat[permutation[2*i], permutation[2*i + 1]]

    answer += val
    valid = permutation.nextpermutation
  result = answer / factor

proc loop_hafnian_slow*[T: AnyFloat](mat: Matrix[T]): T =
  # https://the-walrus.readthedocs.io/en/latest/loop_hafnian.html
  # like a Hafnian but with diagonal entries (loops)

  result = 0.0.toType(T)

  let n = mat.rows
  let indices = toSeq(0..<n)

  for num_loops in countup(0, n, 2):
    for i in 0..<numCombinations(n, num_loops):
      var Hnew = 1.0.toType(T)
      var loops = lexiSubset(indices, num_loops, i)
      var remaining_verts = indices.filterIt(it notin loops)
      for j in loops:
        Hnew *= mat[j,j]
      Hnew *= hafnian_slow(mat[remaining_verts, remaining_verts])
      result += Hnew

proc hafnian*[T: AnyFloat](mat: Matrix[T]): T =
  # https://en.wikipedia.org/wiki/Hafnian
  # 1 / (n! * 2^n) \sum_{k \in S_{2n}} \prod_{j=1}^n A[ k[2j-1], k[2j] ]
  # where S_{2n} is the symetric group [1,2,...,2n]
  #
  # uses Counting Perfect Matchings as Fast as Ryser
  # Andreas Bjorklund 2011 recursive algorithm
  # also code on code golf,
  # https://codegolf.stackexchange.com/questions/156988/codegolf-the-hafnian
  # https://codegolf.stackexchange.com/questions/157049/calculate-the-hafnian-as-quickly-as-possible
  # Taken from qgot_public
  # https://gitlab.com/dualityqp/qgot_public/-/blob/main/math-code/matrix_ops.hpp

  let n = mat.rows
  if n == 0:
    return 1.0.toType(T)
  elif n == 2:
    return mat[0, 1]

  for i in 1 ..< n:
    # remove row, col i,i and row,col 0,0
    let elements = toSeq(1 ..< i) & toSeq(i+1 ..< n)
    # echo "ele ", elements
    # var cor = toseq(0..<n)
    # cor.delete(i)
    # cor.delete(0)
    # assert cor == elements
    let temp = mat[elements, elements]
    result += mat[0,i] * temp.hafnian
  
proc loop_hafnian*[T: AnyFloat](mat: Matrix[T]): T =
  # same as above but uses the recursive algo

  result = 0.0.toType(T)

  let n = mat.rows
  let indices = toSeq(0..<n)

  for num_loops in countup(0, n, 2):
    for i in 0..<numCombinations(n, num_loops):
      var Hnew = 1.0.toType(T)
      var loops = lexiSubset(indices, num_loops, i)
      var remaining_verts = indices.filterIt(it notin loops)
      for j in loops:
        Hnew *= mat[j,j]
      Hnew *= hafnian(mat[remaining_verts, remaining_verts])
      result += Hnew


proc getAXS*[T: AnyFloat](kept_verts: seq[int], A_mat: Matrix[T]): Matrix[T] =
  var vert_list = getRepeatedModes(kept_verts)
  let n = Amat.rows div 2
  var colElems = vertList.mapit(it + n) & vertList
  var rowElems = vertList & vertList.mapit(it + n)
  result = A_mat[rowElems, colElems]

proc haf_coeffs[T: AnyFloat](mat_in: Matrix[T], n: int): Vector[T] =
  var count = 0.int 
  var comb = matrix[T](2, (n div 2)+1)
  comb[0, 0] = 1.0.toType(T)
  for i in 1 .. (n div 2):
    #var factor = pow_trace(mat_in, i) / 2.0*i.toType(T)
    var factor = trace(matrix_power(mat_in, i)) / (2.0*i.toType(T))
    var powfactor = 1.0.toType(T)
    count = 1 - count
    for l in 0..(n div 2):
      comb[count, l] = comb[1 - count, l]
    for j in 1 .. (n div (2*i)):
      powfactor *= factor / j.toType(T)
      for k in (i*j+1) .. (n div 2 + 1):
        comb[count, k-1] += comb[1-count, (k - i*j - 1)]*powfactor
  result = comb.row(count)

proc hafnian_fast*[T: AnyFloat](mat: Matrix[T]): T =

  # This hafnian calculation method is from Bjorklund et al.:
  # 'A Faster Hafnian Formula...', arXiv:1805.12498
  # This implementation is taken mostly from the Walrus library,
  # github.com/XanaduAI/thewalrus/blob/master/thewalrus/_hafnian.py
  # We use a different method for power traces,
  # and leave out other speed ups for simplicity:
  # the Glynn inclusion-exclusion method and Bulmer 
  # et al.'s method for dealing with collisions.
  result = 0.0.toType(T)
  let n = mat.rows
  let halfN = n div 2

  for j in 0 ..< 2^halfN:
    var bin_seq = j.toBin(halfN).items.toSeq
    var kept_edges: seq[int]
    for edge in bin_seq:
        kept_edges.add(parseInt($edge))

    var AXS = getAXS(kept_edges, mat)
    var prefac = pow(-1.0.toType(T), halfN.toType(T) - sum(kept_edges).toType(T))
    var Hnew = prefac * haf_coeffs(AXS, n)[halfN]
    result += Hnew

proc loop_haf_coeffs[T: AnyFloat](mat_in: Matrix[T], n: int, x_diag: Vector[T]): Vector[T] =
  var count = 0.int 
  var comb = matrix[T](2, (n div 2)+1)
  comb[0, 0] = 1.0.toType(T)
  var diag = vector[T](x_diag.len)
  if x_diag.len > 0:
    diag[0 ..< x_diag.len div 2] = x_diag[(x_diag.len div 2)..<x_diag.len]
    diag[x_diag.len div 2 ..< x_diag.len] = x_diag[0..(x_diag.len div 2)]
  for i in 1 .. (n div 2):
    #var factor = pow_trace(mat_in, i) / 2.0*i.toType(T)
    var factor = trace(matrix_power(mat_in, i)) / (2.0*i.toType(T)) + (x_diag*diag)/2.0.toType(T)
    var powfactor = 1.0.toType(T)
    diag = mat_in*diag
    count = 1 - count
    for l in 0..(n div 2):
      comb[count, l] = comb[1 - count, l]
    for j in 1 .. (n div (2*i)):
      powfactor *= factor / j.toType(T)
      for k in (i*j+1) .. (n div 2 + 1):
        comb[count, k-1] += comb[1-count, (k - i*j - 1)]*powfactor
  result = comb.row(count)

proc loop_hafnian_fast*[T: AnyFloat](mat: Matrix[T]): T =

  # Once again, based on code in the Walrus
  # (although slightly simplified)

  result = 0.0.toType(T)
  let n = mat.rows
  let halfN = n div 2

  for j in 0 ..< 2^halfN:
    var bin_seq = j.toBin(halfN).items.toSeq
    var kept_edges: seq[int]
    for edge in bin_seq:
        kept_edges.add(parseInt($edge))

    var AXS = getAXS(kept_edges, mat)
    var DXS = vector[T](AXS.rows)
    var len_kept = AXS.rows div 2
    for i in 0 ..< len_kept:
      DXS[i] = AXS[i+len_kept, i]
    for i in 0 ..< len_kept:
      DXS[i + len_kept] = AXS[i, i+len_kept]
    var prefac = pow(-1.0.toType(T), halfN.toType(T) - sum(kept_edges).toType(T))
    var Hnew = prefac * loop_haf_coeffs(AXS, n, DXS)[halfN]
    result += Hnew


# when isMainModule:
#   echo "linalg.nim"

#   block real:
#     const dim = 100
#     var m = randommatrix[float](dim, dim)
#     m += m.Transpose
#     timeonce("Eigendecomp " & $dim):
#       var (J, S) = m.JacobiEigenDecomp
#     # echo "J\n", J
#     # echo "S\n", S
#     S -= S.getDiagonal.diagonalmatrix
#     # echo "S\n", S
#     echo "S\n", S.abs.data.max


#     timeonce("Inverse " & $dim):
#       let minv = m.inverse
#     echo m * minv ~= Identity[float](dim, dim)
  
#   block complex:
#     const dim = 100
#     var m = randommatrix[Complex[float]](dim, dim)
#     m += m.H
#     timeonce("Eigendecomp " & $dim):
#       var (J, S) = m.JacobiEigenDecomp
#     # echo "J\n", J
#     # echo "S\n", S
#     S -= S.getDiagonal.diagonalmatrix
#     # echo "S\n", S
#     echo "S\n", S.abs.data.max


#     timeonce("Inverse " & $dim):
#       let minv = m.inverse
#     echo m * minv ~= Identity[float](dim, dim)
  
when isMainModule:
  block elements:
    const dim = 4
    var m = randommatrix[float](dim, dim)
    
    echo "m\n", m
    echo m[@[0,1], @[0,1]]
    echo m[[0,1], [0,1]]

