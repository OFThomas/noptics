import math
import std/sequtils

# TODO: add some error handling for invalid inputs

# This takes a sequence assumed to represent a number in
# base n and iterates it by 1
# Useful in iterating through photon counts
proc countBaseN*[T: SomeInteger](v: var seq[T], n: T) =
  for i in countdown(v.len-1, 0):
    if v[i] == n-1:
      v[i] = 0
    else:
      v[i] += 1
      break

proc bintogray*[T: SomeInteger](num: T): T {.inline.} =
  result = num xor (num shr T(1))

proc lexi_powerset*[T](v: seq[T], j: int): seq[T] {.inline, nosideeffect.} =
  result = newseqofcap[T](v.len)
  for i in 0 ..< v.len:
    let shift = 1 shl i
    let jand = j and shift
    if jand != 0:
      result.add v[i]

proc lexi_powerset_packed*(dim: int, j: int): seq[int] {.inline.} =
  result = newseqofcap[int](dim)
  for i in 0 ..< dim:
    let jand = j and (1 shl i)
    if jand != 0:
      result.add i

proc lexi_powerset_packed_both*(dim: int, j: int, spacedim: int): seq[int] {.inline.} =
  result = newseqofcap[int](2*dim)
  for i in 0 ..< dim:
    let jand = j and (1 shl i)
    if jand != 0:
      result.add i
  for i in 0 ..< dim:
    let jand = j and (1 shl i)
    if jand != 0:
      result.add i + spacedim

# eg 0011 -> 0101
# https://graphics.stanford.edu/~seander/bithacks.html#NextBitPermutation
proc lexiNextBitPermutation*[T: SomeInteger](cur: T): T =
  let t = (cur or (cur - 1)) + 1
  result = t or ((((t and -t) div (cur and -cur)) shr 1) - 1)

proc lexiSubset*[T](v: seq[T], n, j: int): seq[T] =
  var idx = 0
  for i in 0 ..< n:
    idx = idx or (1 shl i)
  # get the idx of the nth subset
  for i in 0 ..< j:
    idx = idx.lexiNextBitPermutation
  result = v.lexiPowerSet(idx)

# Finds n choose k
proc numCombinations*[T: SomeInteger](n, k: T): T =
  n.fac div (k.fac * (n-k).fac)

# Finds how many subsets of size k can be found of the container
proc numCombinations*[T; Y: SomeInteger](container: seq[T], k: Y): Y =
  numCombinations(container.len, k)

when isMainModule:

  var v1 = toseq(0..<4)
  v1.countBaseN(4)
  echo v1

  let v = toseq(0..<4)
  let subsize = 3
  for i in 0 ..< v.numCombinations(subsize):
    echo v.lexisubset(subsize, i)
