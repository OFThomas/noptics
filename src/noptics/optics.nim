import noptics/linalg

import complex
import math

# Matrices 
proc phasematrix*[ScalarT](angle: ScalarT): Matrix[Complex[ScalarT]] =
  const I = complex(0.0, 1.0)
  result = matrix(1, 1, @[exp(I * angle)])

proc bsmatrix*[ScalarT](reflec: ScalarT): Matrix[Complex[ScalarT]] =
  const zero: ScalarT = 0.0
  const ident: ScalarT = 1.0
  const I = complex(zero, ident)
  let r = complex(sqrt(reflec))
  let t = complex(sqrt(ident - reflec))
  result = matrix(2, 2, @[t, I*r, I*r, t])

proc mzimatrix*[ScalarT](reflec, phase: ScalarT): Matrix[Complex[ScalarT]] = 
  let phasem = Identity[Complex[ScalarT]](1).oplus phasematrix(phase)
  result = phasem * bsmatrix(reflec)

proc lossmatrix*[ScalarT](loss: ScalarT): Matrix[ScalarT] = 
  result = matrix(1, 1, @[sqrt(loss)])

proc gainmatrix*[ScalarT](gain: ScalarT): Matrix[ScalarT] = 
  result = matrix(1, 1, @[sqrt(gain)])

# Sparse matrix versions with mode indices
proc Phase*[ScalarT](angle: ScalarT, m1: int): SparseMatrix[Complex[ScalarT]] = 
  result = SparseMatrix[Complex[ScalarT]](matrix: phasematrix(angle), modes: @[m1])

proc BS*[ScalarT](reflect: ScalarT, m1, m2: int): SparseMatrix[Complex[ScalarT]] = 
  result = SparseMatrix[Complex[ScalarT]](matrix: bsmatrix(reflect), modes: @[m1,m2])

proc MZI*[ScalarT](reflect, phase: ScalarT, m1, m2: int): SparseMatrix[Complex[ScalarT]] = 
  result = SparseMatrix[Complex[ScalarT]](matrix: mzimatrix(reflect, phase), modes: @[m1, m2])

proc Loss*[ScalarT](loss: ScalarT, m1: int): SparseMatrix[ScalarT] = 
  result = SparseMatrix[ScalarT](matrix: lossmatrix(loss), modes: @[m1])

proc Gain*[ScalarT](gain: ScalarT, m1: int): SparseMatrix[ScalarT] = 
  result = SparseMatrix[ScalarT](matrix: gainmatrix(gain), modes: @[m1])
  

# x: Vector
# y = M * x

# x: Vector
# xo: Classical optical state, amplitudes of each mode correspond
# to the values of x
# M -> SVD(M) = U * S * V^T/^H
# S is diagonal and is either loss or gain on each mode
proc optical_matrix*[ScalarT](state: Vector[ScalarT], mat: Matrix[ScalarT]): Vector[ScalarT] =

  var v = state
  let (U, S, V) = mat.svd
  assert mat ~ U * S * V.T
  v = U * S * V.T * state

  # echo "U\n", U
  # echo "S\n", S
  # echo "V.T\n", V.T
  
  v *= U * S * V.T
  
  # echo "State\n", v

  var localstate = state

  let vs = unitary_decomp(V.T)
  var vident = Identity[ScalarT](V.rows)
  for idx in 0 ..< vs.len:
    multiply_reduced_left(vs[idx], vident)
    localstate.apply vs[idx]

  assert V.T ~ vident

  local_state *= S

  let us = unitary_decomp(U)
  var uident = Identity[ScalarT](U.rows)
  for idx in 0 ..< us.len:
    multiply_reduced_left(us[idx], uident)
    localstate.apply us[idx]

  return localstate
