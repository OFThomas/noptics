import json
# import strutils

proc save*[ScalarT](matrix: ScalarT, file: string) =
  writeFile(file, pretty(%*matrix))

proc load*[ScalarT](mat: var ScalarT, file: string) = 
  # echo "load not implemented yet :/"
  let jsonobj = parseFile(file)
  mat = jsonobj.to ScalarT

proc load*[ScalarT](file: string): ScalarT = 
  # echo "Good luck!"
  result.load(file)


