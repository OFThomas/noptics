import noptics/linalg
import noptics/quantumoptics
import noptics/combinatorics
import noptics/optics

import sequtils
import math

import unittest

suite "Fock state marginals":
  test "marginals_test":
    const n = 3
    const m = 5
    const sampCount = 50000

    # construct state
    var state = fockstate[float](m)
    var U = randomunitary[float](m)
    var BS1 = BS[float](0.3455, 0, 1)
    var BS2 = BS[float](0.1464, 1, 4)
    var BS3 = BS[float](0.0955, 2, 3)
    state.input.setFirst(n, 1)
    state.apply(BS1)
    state.apply(BS2)
    state.apply(BS3)
    # echo "input state is ", state.input

    var margspec: seq[int]
    var marges = [@[0],@[4],@[0,1],@[2,3],@[0,1,2]] 
    for i in marges:
      var resspec = state.marginalprob(i)
      # echo "test marginal probability for ", i, " is ", resspec

    var samples: seq[ClickPattern] = @[]
    for i in 0..<sampCount:
      samples.add vanillasample(state)
    var talliedsamples = tallysamples(samples)
    # echo "sampled click patterns are:"
    # for i in talliedsamples.uniqueCPs:
      # echo i.photons
    var tals: seq[float] 
    for i in talliedsamples.tallies:
      tals.add float(i)/float(sampCount)
    # echo "associated probabilities are: ", tals
