import noptics/linalg
import noptics/io

import sequtils
import complex

import timeit

import unittest

suite "Linalg":

  test "v_ctors":
    let v = Vector[float](len: 2, data: @[0.0, 1.0])
    assert v[0] == 0.0
    assert v[1] == 1.0

    let v1 = vector[float](2)
    assert v1[0] == 0.0
    assert v1[1] == 0.0

    let v1c = vector[Complex64](2)
    assert v1c[0] == complex(0.0)
    assert v1c[1] == complex(0.0)

    let vectc = vectorc(2)
    assert vectc[0] == complex(0.0)
    assert vectc[1] == complex(0.0)

    let vectorctor = vectorc(@[7.0, 8.0])
    assert vectorctor[0] == complex(7.0)
    assert vectorctor[1] == complex(8.0)

    let mkvector = vector[float](2)
    assert mkvector[0] == 0.0
    assert mkvector[1] == 0.0

    # lol
    discard randomvector[float](2)

    let constvector = constantvector[float](2, 77)
    assert constvector[0] == 77.0
    assert constvector[1] == 77.0

    
  test " m_ctors":
    let mc = Matrix[float](rows: 1, cols: 2, data: @[9.0, 7.0])
    assert mc[0, 0] == 9.0
    assert mc[0, 1] == 7.0

    let mr = Matrix[float](rows: 2, cols: 1, data: @[5.0, 3.0])
    assert mr[0, 0] == 5.0
    assert mr[1, 0] == 3.0

    let m = Matrix[float](rows: 2, cols: 2, data: @[9.0, 7.0, 5.0, 3.0])
    assert m[0, 0] == 9.0
    assert m[0, 1] == 7.0
    assert m[1, 0] == 5.0
    assert m[1, 1] == 3.0

    let zm = matrix[float](2, 2)
    assert zm[0, 0] == 0.0
    assert zm[0, 1] == 0.0
    assert zm[1, 0] == 0.0
    assert zm[1, 1] == 0.0

    # square mat 
    let a = matrix[float](2, @[3.0, 4.0, 5.0, 6.0])
    assert a[0, 0] == 3.0
    assert a[0, 1] == 4.0
    assert a[1, 0] == 5.0
    assert a[1, 1] == 6.0

    let b = matrix[float](2,2, @[3.0, 4.0, 5.0, 6.0])
    assert b[0, 0] == 3.0
    assert b[0, 1] == 4.0
    assert b[1, 0] == 5.0
    assert b[1, 1] == 6.0

    let c = matrixc[float](2, 2, @[5.0, 6, 7, 8])
    assert c[0, 0] == complex(5.0)
    assert c[0, 1] == complex(6.0)
    assert c[1, 0] == complex(7.0)
    assert c[1, 1] == complex(8.0)

    let d = makematrix[float](2, 2, proc(i, j: int): float = (i*2 + j).float)
    assert d[0, 0] == (0.0)
    assert d[0, 1] == (1.0)
    assert d[1, 0] == (2.0)
    assert d[1, 1] == (3.0)

    let m1 = Identity[float](2, 2)
    assert m1[0, 0] == 1.0
    assert m1[0, 1] == 0.0
    assert m1[1, 0] == 0.0
    assert m1[1, 1] == 1.0 

    let m2 = Identity[float](2)
    assert m2[0, 0] == 1.0
    assert m2[0, 1] == 0.0
    assert m2[1, 0] == 0.0
    assert m2[1, 1] == 1.0 

    let m3 = Identityc(2)
    assert m3[0, 0] == complex(1.0)
    assert m3[0, 1] == complex(0.0)
    assert m3[1, 0] == complex(0.0)
    assert m3[1, 1] == complex(1.0)

    let m4 = constantmatrix[float](2, 2, 77)
    assert m4[0, 0] == 77
    assert m4[0, 1] == 77
    assert m4[1, 0] == 77
    assert m4[1, 1] == 77

    let m5 = constantmatrix[Complex64](2, 2, complex(77.0))
    assert m5[0, 0] == complex(77.0)
    assert m5[0, 1] == complex(77.0)
    assert m5[1, 0] == complex(77.0)
    assert m5[1, 1] == complex(77.0)

    discard randommatrix[float](2,2)
    discard randommatrix[float](2)

  test " vector_access":
    var v = vector[float](2)
    assert v[0] == 0
    v[0] = 2
    assert v[0] == 2
    v[1] = 65
    assert v[1] == 65

    let vsub = v[0..<1]
    assert vsub[0] == 2

    let vsub1 = v[1..<2]
    assert vsub1[0] == 65

    let vsec = v[@[0, 1]]
    assert v == vsec


  test " matrix_access":
    var m = matrix[float](2,2)
    assert m[0,0] == 0.0
    m[0,0] = 1
    m[0,1] = 2
    m[1,0] = 3
    m[1,1] = 4
    assert m[0,1] == 2

    let row = m.row(0)
    assert row[0] == m[0,0]
    assert row[1] == m[0,1]

    let col = m.col(1)
    assert col[0] == m[0,1]
    assert col[1] == m[1,1]

    let mr = m[[0], 0..<m.cols]
    assert row.data == mr.data

    let mc = m[0..<m.rows,@[1]]
    assert col.data == mc.data

    let msub = m[0..<1, 0..<1]
    assert msub[0,0] == m[0,0]

    let msub1 = m[1..<2, 1..<2]
    assert msub1[0,0] == m[1,1]

    let msub2 = m[[0,1], [0,1]]
    assert msub2 == m

    let msub3 = m[[0], [0]]
    assert msub3[0,0] == m[0,0]


  test " matrix_operations":
    var m = matrix[float](2,2) 
    m[0,0] = 1
    m[0,1] = 2
    m[1,0] = 3
    m[1,1] = 4

    var mt = m.Transpose
    assert m[0,0] == mt[0,0]
    assert m[0,1] == mt[1,0]
    assert m[1,0] == mt[0,1]
    assert m[1,1] == mt[1,1]

    mt.Tinplace
    assert m == mt

  test " matrix_complex_ops":
    var m = matrix[Complex64](2,2) 
    m[0,0] = complex(1.0)
    m[0,1] = complex(2.0)
    m[1,0] = complex(3.0)
    m[1,1] = complex(4.0)

    var mh = m.H
    assert m[0,0] == mh[0,0]
    assert m[0,1] == mh[1,0]
    assert m[1,0] == mh[0,1]
    assert m[1,1] == mh[1,1]

    mh.Hinplace
    assert m == mh


  test " addition":
    let mat = matrix(2,2, @[1.0, 2.0, 3.0, 4.0])

    var m = mat + mat
    assert m == 2.0 * mat

    m += m
    assert m == 4.0 * mat

    m = mat - mat
    assert m == matrix[float](2,2)

    m -= mat
    assert m == -mat
    
    let vec = vector(@[1.0, 3.0])

    var v = vec + vec
    assert v == 2.0 * vec

    v += v
    assert v == 4.0 * vec

    v = vec - vec
    assert v == vector[float](2)

    v -= vec
    assert v == -vec
    
  test " multiplication":
    let mat = matrix(2,2, @[1.0, 2.0, 3.0, 4.0])
    let antimat = matrix(2,2, @[0.0, 2.0, 3.0, 0.0])

    var m = mat
    m *= 2.0
    assert m == 2.0 * mat

    m = mat
    m *= antimat
    assert m == mat * antimat
    # false!
    # assert m == antimat * mat

    assert antimat * mat != mat * antimat

    let vec = vector(@[3.0, 4.0])
    var v = 2.0 * vec
    assert v == 2.0 * vec

    v = vec
    v *= 2.0
    assert v == 2.0 * vec

    v = vec
    v *= mat
    assert v == mat * vec
    assert v != vec * mat

  test " division":
    let mat = matrix(2,2, @[1.0, 2.0, 3.0, 4.0])

    var m = mat
    m /= 2.0
    assert m == 0.5 * mat

    let vec = vector(@[2.0, 4.0])

    var v = vec / 2.0
    assert v == 0.5 * vec
  

  test " matrix_reorders":
    let m1 = matrix(1,1, @[1.0])
    var mplus = m1.oplus m1
    assert mplus == Identity[float](2,2)


    var m2 = Identity[float](2).kronecker_prod Identity[float](2)
    assert m2 == Identity[float](4)

    var nay_seq = @[1, 3, 0, 0, 1, 5]
    assert getRepeatedModes(nay_seq) == @[0, 1, 1, 1, 4, 5, 5, 5, 5, 5]
 
  test " decompositions":
    let mat = matrix(2,2, @[1.0, 2.0, 3.0, 4.0])

    let (Q, R) = mat.QR
    
    assert Q * R ~= mat

    let (Us, A) = mat.Schur
    assert Us * A * Us.Transpose ~= mat

    let (U, S, V) = mat.svd
    assert U * S * V.Transpose ~= mat

  test " unitary_decomp":
    let mat = random_unitary[float](4)
    var smat = mat.unitary_decomp

    var ident = Identity[Complex64](mat.rows)
    for u in 0 ..< smat.len:
      # # echo smat[u]
      ident = smat[u] * ident

    # # echo "ident\n", ident
    # # echo "mat\n", mat
    assert ident ~= mat

    

  # echo "All of olimat tests past!"


suite "MatrixDecomp":
  test " JacobiEigs":
    const dim = 10
    var m = randommatrix[float](dim, dim)
    m += m.Transpose
    # # echo "m\n", m

    timeonce("Jacobi"):
      let (Q, D) = m.JacobiEigenDecomp
    # # echo "Q\n", Q, "\nD\n", D

    timeonce("Schur"):
      let (U, S) = m.Schur
    # # echo "U\n", U, "\nS\n", S

  test " complex_Jacobi_eigs":
    const dim = 10
    var mc = randommatrix[Complex64](dim, dim)
    mc += mc.H
    # # echo "mc\n", mc

    timeonce("Jacobi"):
      let (Q, D) = mc.JacobiEigenDecomp
    # # echo "Q\n", Q.abs.chop, "\nD\n", D.abs.chop
    # # echo "Q\n", Q.chop, "\nD\n", D.chop


    timeonce("Jacobi inverse"):
      let mcinv = mc.JacobiInverse

    # # echo "\nmc * mcinc\n", (mc * mcinv).abs.chop
    # echo mc * mcinv ~= Identity[Complex64](dim)

    timeonce("Schur inverse"):
      let mcinvS = mc.SchurInverse

    # # echo "\nmc * mcincS\n", (mc * mcinvS).abs.chop
    # echo mc * mcinvS ~= Identity[Complex64](dim)

suite "matrixio":
  test " real_matrices":

    var m = randommatrix[float32](2,2)
    const filename = "mymatrix.tmp"
    m.save(filename)
    m.load(filename)

    let mload = load[Matrix[float32]](filename)
    assert m == mload

  test " complex_mat":
    var m = randommatrix[Complex64](4,4)
    const filename = "complexmymatrix.tmp"
    m.save(filename)
    m.load(filename)

    let mload = load[Matrix[Complex64]](filename)
    assert m == mload

suite "matrixAccess":
  test " elements":
    const dim = 4
    var m = randommatrix[float](dim, dim)
    
    # echo "m\n", m
    # echo m[[0,1], [0,1]]

suite " hafnians": 
  const dim = 6
  test " real":
    var m = randommatrix[float](dim, dim)
    m += m.Transpose

    timeonce("Real hafnian fast"):
      let fast = m.hafnian
    timeonce("Real hafnian slow"):
      let slow = m.hafnian_slow
    timeonce("Real hafnian Bjorklund"):
      let bjork = m.hafnian_fast

    echo "hafnian real ", fast, " hafnian slow ", slow, " bjorklund ", bjork
    assert m.hafnian ~ m.hafnian_slow
    assert m.hafnian_fast ~ m.hafnian_slow
  
  test " complex":
    var m = randommatrix[Complex64](dim, dim)
    m += m.Transpose

    timeonce("Cmplx hafnian fast"):
      let fast = m.hafnian
    timeonce("cmplx hafnian slow"):
      let slow = m.hafnian_slow
    timeonce("cmplx hafnian Bjorklund"):
      let bjork = m.hafnian_fast

    echo "hafnian complex ", fast, " hafnian slow ", slow, " bjorklund ", bjork
    assert m.hafnian ~ m.hafnian_slow
    assert m.hafnian_fast ~ m.hafnian_slow

  test " loop hafnian real":
    var m = randommatrix[float](dim, dim)
    m += m.Transpose

    timeonce("Real lhafnian fast"):
      let fast = m.loop_hafnian
    timeonce("Real lhafnian slow"):
      let slow = m.loop_hafnian_slow
    timeonce("Real lhafnian Bjorklund"):
      let bjork = m.loop_hafnian_fast

    echo "loop hafnian real ", fast, "loop hafnian slow ", slow, " bjorklund ", bjork
    assert m.loop_hafnian ~ m.loop_hafnian_slow
    assert m.loop_hafnian_fast ~ m.loop_hafnian_slow

  test " loop hafnian complex":
    var m = randommatrix[Complex64](dim, dim)
    m += m.Transpose

    timeonce("Cmplx lhafnian fast"):
      let fast = m.loop_hafnian
    timeonce("Cmplx lhafnian slow"):
      let slow = m.loop_hafnian_slow
    timeonce("Cmplx lhafnian Bjorklund"):
      let bjork = m.loop_hafnian_fast

    echo "loop hafnian complex ", fast, "loop hafnian complex ", slow, " bjorklund ", bjork
    assert m.loop_hafnian ~ m.loop_hafnian_slow
    assert m.loop_hafnian_fast ~ m.loop_hafnian_slow

  test " haf_equals_perm_real":
    let m = randommatrix[float](dim, dim)
    var A = matrix[float](2*dim, 2*dim)
    A[0..<dim, dim..<2*dim] = m
    A[dim..<2*dim, 0..<dim] = m.Transpose

    # echo "m.perm ", m.perm, " A.hafnian ", A.hafnian, " A.hafnian_slow ", A.hafnian_slow
    assert m.perm ~ A.hafnian

  test " haf_equals_perm_complex":
    let m = randommatrix[Complex64](dim, dim)
    var A = matrix[Complex64](2*dim, 2*dim)
    A[0..<dim, dim..<2*dim] = m
    A[dim..<2*dim, 0..<dim] = m.Transpose

    # echo "m.perm ", m.perm, " A.hafnian ", A.hafnian, " A.hafnian_slow ", A.hafnian_slow
    assert m.perm ~ A.hafnian

