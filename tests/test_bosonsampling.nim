import noptics/linalg
import noptics/quantumoptics

import math
import unittest

suite "BosonSampling":
  test "state.prob":

    const n = 2
    const m = 4

    var state = fockstate[float](m)
    var U = randomunitary[float](m)

    state.input.setFirst(n, 1)

    var outclicks = clickpattern(m)
    outclicks.setFirst(n, 1)

    var probs: seq[float]

    while true:
      let p = state.prob(outclicks)
      # echo outclicks, " ", p
      probs.add p

      if outclicks.prev() == false:
        break

