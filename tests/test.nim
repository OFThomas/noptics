import noptics/quantumoptics
import noptics/linalg
import noptics/combinatorics
import sequtils
import unittest

suite "NotSure":
  test "getAXS":

    var kept_verts: seq[int]
    kept_verts = @[1, 0, 1]

    var rand_mat = randommatrix[float](6, 6)

    echo rand_mat
    var temp = getAXS(kept_verts, rand_mat)
    echo temp

