import noptics/linalg
import noptics/optics

import math, complex
import unittest

suite "Classical optics": 

  test "matrix_ctors":

    # echo complex(1.0)

    # echo Identity[float](2)
    # echo Identity[Complex32](2)
    # echo Identity[Complex64](2)

    # echo randommatrix[float](2)
    # echo randommatrix[Complex32](2)
    # echo randommatrix[Complex64](2)

    var state = vector[Complex64](4)
    # echo state

    # echo matrix(2, 2, @[7.0, 5.0, 3.0, 1.0])

    # echo matrixc(2,2, @[4.0,4.0,6.0,6.0])

    # echo phasematrix(PI / 2)
    # echo bsmatrix(0.5)

    # echo bsmatrix(0.5) * bsmatrix(0.5).H

    # echo phasematrix(PI/2) * phasematrix(PI/2)
    # echo phasematrix(PI/2) * phasematrix(PI/2).H

    state[0] = complex(1.0)
    # echo state, "\n"

    state.apply phasematrix(PI/2), 0
    # echo state, "\n"

    state.apply bsmatrix(0.5), 0, 1
    # echo state, "\n"

    state.apply bsmatrix(0.5), 0, 3
    # echo state, "\n"

    # echo Identityc(1).oplus Identityc(1), "\n"

    # echo mzimatrix(0.5, PI/2), "\n"

  test "mzi_test":
    # state = randomvector[Complex64](4)
    # # echo state, "\n"

    # # echo state.subblock(1,2), "\n"

    var state = vector[Complex64](1_000_000)
    state[0] = complex(1.0)
    # const maxt = 10_000_000
    const maxt = 10

    for i in 0 ..< maxt:
      state.apply mzimatrix(0.5, PI/2), i mod state.len, (i+1) mod state.len
      # state.apply bsmatrix(0.5), i mod state.len, (i+1) mod state.len

    # so that the compiler doesn't optimize out the loop 
    # echo state[0]


    state = vectorc(2)
    state[0] = complex(1.0)

    # or
    state = vectorc(@[1.0, 0.0])
    
  test "loops_for_plots":
    var xvals: seq[float]
    var amps0: seq[float]
    var amps1: seq[float]

    const numpoints = 100
    for i in 0 ..< numpoints:
      var state = vectorc(@[1.0, 0.0])
      let reflect = 1.0 / numpoints
      state.apply mzimatrix(reflect, PI/2), 0, 1
      xvals.add reflect
      amps0.add state[0].abs
      amps1.add state[1].abs

  test "QR_decomp":
    var m57 = randommatrix[float](2,2)
    m57 = matrix(2,2, @[1.0, 2.0, 3.0, 4.0])
    # echo "m57\n", m57

    var (Q, R) = QR(m57)
    # echo "Q\n", Q
    # echo "R\n", R

    # echo "m57\n", m57
    # echo "Q * R\n", Q * R

    assert m57 ~ Q*R

  test "unitary_decomp":
    let u = random_unitary[float](2)
    # echo "u\n", u

    let gs = unitary_decomp(u)

    var cident = Identityc(u.rows)
    for idx in 0 ..< gs.len:
      # # echo "gs idx\n", gs[idx], ", ", modes[idx], "\n"
      multiply_reduced_left(gs[idx], cident)
      # # echo "\n cident\n", cident
    # echo "cident\n", cident
    assert u ~ cident

  test "random unitary":
    let u = random_unitary[float](2)
    let (Q, R) = u.QR
    # # echo "Q\n", Q
    # # echo "R\n", R
    # # echo "Q QH\n", Q * Q.H
    # echo "Q is unitary ", Q.is_unitary
    
    # echo "Q * R\n", Q*R
    assert u ~ Q*R

    var rm = randommatrix[float](2,2)
    rm += rm.Transpose

    # let rm = matrix(2, @[1.0, 0.0, 0.0, 1.0])
    # let rm = matrixc(2, @[0.0, 1.0, 1.0, 0.0])
    let (Uschur, Tschur) = rm.Schur

    # echo "rm\n", rm
    # echo "Uschur\n", Uschur
    # echo "Tschur\n", Tschur

    # echo "Uschur * T * Uschur\n", Uschur * Tschur * Uschur.Transpose

    assert rm ~ Uschur * Tschur * Uschur.Transpose

  test "svd_test":
    var u = random_unitary[float](2)
    u = u + u.H
    let (U, S, V) = svd(u)

    # echo "U\n", U
    # echo "S\n", S
    # echo "V\n", V

    assert u ~ U*S*V.H

    let (Q, R) = u.QR
    # echo "Q\n", Q
    # echo "R\n", R

    assert u ~ Q*R

    let (A, T) = u.Schur
    # echo "A\n", A
    # echo "T\n", T

    assert u ~ A * T * A.H

  test "optical_matmul":
    # echo "\n Optical matmul"
    const n = 20
    # var v = vector[float](2)
    # v[0] = 1.0
    const v = constantvector(n,1.0)
    var m = random_matrix[float](n,n)

    # echo "v\n", v
    # echo "m\n", m

    let mv = m * v
    # echo "m v \n", mv

    var state = v
    let (U, S, V) = m.svd
    assert m ~ U * S * V.Transpose
    state = U * S * V.Transpose * state
    # state *= U
    # state *= S
    # state *= V.Transpose

    # echo "U\n", U
    # echo "S\n", S
    # echo "V.T\n", V.Transpose
    
    state = v
    state *= U * S * V.Transpose
    
    # echo "State\n", state

    assert mv ~ state

    var testv = v

    testv *= m
    # echo "testv *= m\n", testv
    # echo "mv\n", m*v
    assert testv ~ m*v

    var localstate = v

    let vs = unitary_decomp(V.Transpose)
    var vident = Identity[float](V.rows)
    for idx in 0 ..< vs.len:
      multiply_reduced_left(vs[idx], vident)
      localstate.apply vs[idx]

    assert V.Transpose ~ vident

    local_state *= S

    let us = unitary_decomp(U)
    var uident = Identity[float](U.rows)
    for idx in 0 ..< us.len:
      multiply_reduced_left(us[idx], uident)
      localstate.apply us[idx]

    # # echo "ident\n", ident
    assert U ~ uident

    # local_state *= ident
    # # echo "local state\n", local_state
    # # echo "local state\n", ident * v

    # echo "local state\n", local_state
    assert local_state ~ state

  test "Sparse_testing":
    let phase = Phase(PI/2, 0)
    # echo phase

    let bs = BS(0.5, 0, 1)
    # echo bs

    let mzi = MZI(0.5, PI/2, 0, 1)
    # echo mzi

    let loss = Loss(0.5, 0)
    # echo loss

    let gain = Gain(2.0, 0)
    # echo gain

    var state = vectorc(10)
    state[0] = complex(1.0)

    state.apply mzi
    # echo state

    state.apply loss
    # echo state

    state.apply gain
    # echo state

