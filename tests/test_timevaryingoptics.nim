import noptics/tensors
import noptics/optics

import complex
import math
import random
import unittest

proc gaussian*(x: float, mean: float = 0.0, sigma: float = 1.0): float =
      let argN = -0.5 * (x - mean)^2
      let argD = sigma^2
      let norm = 1.0 / (sigma * sqrt(2.0 * PI))
      result = norm * exp(argN / argD)


suite "Time Varying Optics":
  test "Vector with frequencies":
    
    const spaceDim = 4
    const specDim = 7
    var v = tensor[float](spaceDim, specDim)

    echo v

    v[0..1, _] = 1.0
    echo v

    for i in -3 .. 3:
      echo gaussian(i.float)

    v[0, _] = gaussian
    echo v

    # v[1, _] = 

    var s = newSeq[float](specDim)
    for i in 0..<s.len:
      s[i] = -3.float + i.float

    echo s

    v[1,_] = s.toTensor
    echo v

    v[1, _] = gaussian
    echo v

    echo linspaced[float](-3.0, 3.0, 7)
    v[0,_] = linspaced[float](-3.0, 3.0, 7)
    echo v
    echo v.map(gaussian)

    var y = linspaced(-3.0, 3.0, 7)
    echo y
    echo y[0..1].map gaussian

    echo y
    y.linspaced(0.0, 2.0, _)
    echo y

    var z = tensor[float](5, 3)
    echo "z ", z
    z.linspaced(-1.0, 1.0, _, 1)
    echo z

    z.linspaced(2.0, 3.0, 2, _)
    echo z

    z.mapInPlace(gaussian, _, 1)
    echo z
    
    z[_, _] = rand(1.0)
    echo z

  test "time":

    const timeDim = 3
    const spaceDim = 4
    const specDim = 1
    var state = tensor[Complex64](timeDim, spaceDim, specDim)
    echo state
    state[0, 0, _] = 1.0.complex
    echo state
    for i in 0 ..< spaceDim:
      state[0, i, _] = i.float.complex
    echo state

    state[_,_,_] = 0.0.complex

    state[0,0,_] = 1.0.complex
    echo state

    echo "state apply"
    state.apply(mzimatrix(0.5, 0.0), _, %[0,1], _) 
    echo state
    echo state.abs

