import noptics/io
import noptics/linalg
import noptics/graphs


# import sequtils
import noptics/quantumoptics

import unittest


suite "Graphs":

  test "graphs_states":
    let g = randomgraph(3, 0.7)
    var state = g.toState
    # echostate

  test "graphio": 
    var graph = randomgraph(4, 0.2)
    # echo"\nGraph\n", graph
    graph.save "mygraph.grph"
    # graph.load "mygraph.grph"
    let graph2 = load[Graph]("mygraph.grph")

    assert graph == graph2
    
  test "Graphsmain": 
    # var g = graph(matrix(2,2, @[0.0, 1.0, 1.0, 0.0]))
    var g = makegraph(@[0.0, 1.0, 1.0, 0.0])

    # echog

    let rgraph = randomgraph(4, 0.8)
    # echorgraph

    # echorgraph.subgraph(@[0,1])

    let vertices = rgraph.getvertices
    # echovertices

    # # echo"Graphs and stuff "

  # when isMainModule:
    # Graphsmain()

    var graph = randomgraph(4, 1)
    # echo"g \n", graph

