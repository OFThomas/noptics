import unittest

import noptics/tensors
import complex


suite "tensors":
  test "ctor":
    let t = Tensor[int]()
    # echo t
    assert t == t

  test "varargs":
    let y = tensor[int](2, 3, 1)
    # echo y

  test "seqctor":
    let y = tensor[int](@[2, 3, 1])
    # echo y

  test "arrayctor":
    let y = tensor[Complex64]([2, 3, 1])
    # echo y

  test "len":
    let x = tensor[float](2, 3)
    assert x.len == 2 * 3

  test "[]":
    var x = tensor[int](2,3,4)
    for i in 0..<x.len:
      x.data[i] = i
    
    # echo x
    
    for i in 0 ..< x.dims[0]:
      for j in 0 ..< x.dims[1]:
        for k in 0 ..< x.dims[2]:
          # echo x[i,j,k]
          assert x[i,j,k].value == i * x.dims[1] * x.dims[2] + j * x.dims[2] + k

  test "[]=":
    var x = tensor[int](2,3,4)
    for i in 0..<x.len:
      x.data[i] = i
    
    # echo x
    
    for i in 0 ..< x.dims[0]:
      for j in 0 ..< x.dims[1]:
        for k in 0 ..< x.dims[2]:
          x[i,j,k] = 3
          assert x[i,j,k].value == 3

    # echo x
  test "linspaced":
    var t = linspaced[float](1.0, 2.0, 4)

  test "broadcast ops":
    var t = linspaced[float](1.0, 4.0, 4)
    echo t
    var y = constantTensor[float](4, 1.0)
    echo y

    echo t.cwiseProd y
    echo t.cwiseSum y
