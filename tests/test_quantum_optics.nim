import noptics/combinatorics
import noptics/linalg
import noptics/optics
import noptics/quantumoptics


import sequtils
import math
import complex
import bitops

import timeit
import weave

import std/strformat

import std/unittest

proc double_prec_all_clicks(dim: int) = 
  # echo"\nall_clicks smsqs click probs f64"
  # var dim = 14
  var state = gaussianstate[float64](dim)
  for i in 0 ..< dim:
    state.apply squeezer(2.0, i)
  # echo"vac prob ", state.vacuum_prob

  var thres: Threshold
  thres.clicks.setmask(0 ..< dim)
  # echo"thres ", thres

  var prob: float64
  timeonce("f64"):
    prob = state.threshold(thres)
  # echo"click prob ", prob, " set modes ", getSetModes(thres)

proc double_prec_all_clicks_parallel(dim: int) = 
  # echo"\nall_clicks smsqs click probs f64"
  # var dim = 14
  var state = gaussianstate[float64](dim)
  for i in 0 ..< dim:
    state.apply squeezer(2.0, i)
  # echo"vac prob ", state.vacuum_prob

  var thres: Threshold
  thres.clicks.setmask(0 ..< dim)
  # echo"thres ", thres

  var prob: float64
  timeonce("f64"):
    prob = state.threshold_parallel(thres)
  # echo"click prob ", prob, " set modes ", getSetModes(thres)

proc opticsquantummain() =
  const dim = 2
  var state = gaussianstate[float32](dim)
  # echostate

  # single mode squeezer 
  # echosqueezer(0.1'f32, 0)
  # two mode squeezer 
  # echosqueezer(0.1'f32, 0, 1)

  # squeezing 
  state.apply squeezer(0.1'f32, 0)
  # echo"\n", state.covariance

  state.apply squeezer(0.1'f32, 0,1)
  # echo"\n", state.covariance

  # squeezing with oposite phase cancels out 
  state.apply squeezer(-0.1'f32, 0, 1)
  # echo"\n", state.covariance

  state.apply squeezer(-0.1'f32, 0)
  # echo"\n", state.covariance

  # back at the identity (vacuum)

  # add some sq
  state.apply squeezer(0.3'f32, 0)
  # echo"\n", state.covariance

  # two beamsplitters cancel 
  for i in 0 ..< 2:
    state.apply BS(1.0'f32, 0, 1)
    # echo"\n", state.covariance

  # undo squeezing 
  state.apply squeezer(-0.3'f32, 0)
  # echo"\n", state.covariance

  # identity again?
  #
  state = gaussianstate[float32](dim)
  # echo"type of state ", typeof(state)
  # echo"vac prob ", state.vacuum_prob

  state.apply squeezer(0.2'f32, 0)
  # echo"vac prob ", state.vacuum_prob

  state = gaussianstate[float32](dim)
  state.apply squeezer(0.2'f32, 0, 1)
  # echo"vac prob ", state.vacuum_prob

  var cp = Click_pattern(modes: toseq[0 ..< dim], photons: newSeq[int](dim))
  # echocp
  # # echostate.threshold(cp)

  var thres: Threshold
  for i in 0 ..< 2^dim:
    # echo"thres ", thres

    # echostate.threshold(thres)
    discard thres.next

  block smsq:
    # echo"\nsmsq click probs"
    var state = gaussianstate[float32](dim)
    state.apply squeezer(0.2'f32, 0)
    # echo"vac prob ", state.vacuum_prob

    var thres: Threshold
    for i in 0 ..< 2^dim:
      # echo"thres ", thres

      # echo"click prob ", state.threshold(thres)
      discard thres.next

  block two_smsq:
    # echo"\n2 smsqs click probs"
    var state = gaussianstate[float32](dim)
    state.apply squeezer(0.2'f32, 0)
    state.apply squeezer(0.2'f32, 1)
    # echo"vac prob ", state.vacuum_prob

    var thres: Threshold
    for i in 0 ..< 2^dim:
      # echo"thres ", thres

      # echo"click prob ", state.threshold(thres)
      discard thres.next

  block many_two_smsq:
    # echo"\nmany smsqs click probs"
    var dim = 8
    var state = gaussianstate[float32](dim)
    state.apply squeezer(0.2'f32, 0)
    state.apply squeezer(0.2'f32, 1)
    # echo"vac prob ", state.vacuum_prob

    var thres: Threshold
    for i in 0 ..< 2^dim:
      # echo"thres ", thres

      # echo"click prob ", state.threshold(thres), " set modes ", getSetModes(thres)
      discard thres.next

  block single_all_clicks:
    # echo"\nall_clicks smsqs click probs"
    var dim = 10
    var state = gaussianstate[float32](dim)
    for i in 0 ..< dim:
      state.apply squeezer(2.0'f32, i)
    # echo"vac prob ", state.vacuum_prob

    var thres: Threshold
    thres.clicks.setmask(0 ..< dim)
    # echo"thres ", thres

    var prob: float32
    timeonce("f32"):
      prob = state.threshold(thres)
    # echo"click prob ", prob, " set modes ", getSetModes(thres)

  block single_all_clicksf64:
    # echo"\nall_clicks smsqs click probs f64"
    var dim = 10
    var state = gaussianstate[float64](dim)
    for i in 0 ..< dim:
      state.apply squeezer(2.0, i)
    # echo"vac prob ", state.vacuum_prob

    var thres: Threshold
    thres.clicks.setmask(0 ..< dim)
    # echo"thres ", thres

    var prob: float64
    timeonce("f64"):
      prob = state.threshold(thres)
    # echo"click prob ", prob, " set modes ", getSetModes(thres)


  block benchmarks:
    let dim = 15
    double_prec_all_clicks(dim)

  block repeat_row_col_test:
    let m = makematrix[float](4, 4, proc(i, j: int): float = (i*2 + j).float)
    # echo"m\n", m

    let inpat = @[0,1,3]
    let mcols = m[0..<m.rows, inpat]

    # echo"mcols\n", mcols

    let outpat = @[0,0,3]
    let mcols_rows = mcols[outpat, 0..<mcols.cols]

    # echo"mcols_rows\n", mcols_rows

    var incp = clickpattern(4)
    incp.photons = @[1,1,0,1]
    var outcp = clickpattern(4)
    outcp.photons = @[2,0,0,1]

    let msub = m.repeat_rows_cols(outcp, incp)
    # let msub = m[outcp, incp]
    # echo"msub\n", msub

when ismainmodule:
  const dim = 14

  double_prec_all_clicks(dim)
  double_prec_all_clicks_parallel(dim)


  var fs = fockstate[float64](4)
  # echofs

  fs.apply BS(0.5, 0, 1)
  fs.apply Phase(PI/4, 1)

  fs.apply MZI(0.5, PI/4, 0, 1)

  # echofs

  fs.input.photons = @[1,1,0,0]
  # var outclicks = fs.output
  # echofs.prob(@[1,1,0,0])
  # echofs.prob(@[2,0,0,0])
  # echofs.prob(@[0,2,0,0])

  let u = randomunitary[float](10)
  
  # timeonce("perm slow"):
  #   # echoperm_slow(u)

  timeonce("ryser"):
    discard perm(u)
    # echoperm(u)
  
  timeonce("ryser parallel"):
    discard perm_parallel(u)
    # echoperm_parallel(u)

  block fockprobs:
    const dim = 10 # 20

    var fs = fockstate[float64](dim)
    let urand = sparsematrix(randomunitary[float](dim), toseq[0 ..< dim])
    fs.apply urand

    fs.input.photons.applyit(1)

  block fock_dist:
    timeonce("Whole prob distribution"):
      const dim = 4 # 10
      var fs = fockstate[float64](dim)
      let urand = sparsematrix(randomunitary[float](dim), toseq[0 ..< dim])
      fs.apply urand

      # fs.input.photons.applyit(1)
      for i in 0 ..< fs.input.photons.len:
        fs.input.photons[i] = 1
      

      let numphotons = 4
      let numterms = (numphotons+1) ^ dim
      
      var probs: seq[float]
      var outclicks = fs.input
      for i in 0 ..< numterms:
        if outclicks.numclicks == fs.input.numclicks:
          # probs.add fs.prob(outclicks)
          probs.add fs.prob(outclicks)
        outclicks.photons.countBaseN(numphotons+1)

      # # echoprobs
      # echo"distribution sum ", probs.foldl(a+b)
      # # echofs.prob_parallel
      

  block fock_prob_nocopy:
    const dim = 10 # 20
    var fs = fockstate[float64](dim)
    let urand = sparsematrix(randomunitary[float](dim), toseq[0 ..< dim])
    fs.apply urand
    fs.input.photons.applyit(1)

    let output = fs.input
    timeonce("repeat_rows_cols usub"):
      let usub = fs.matrix.repeat_rows_cols(output, fs.input)
      let p = perm(usub)
      # echop
  
    timeonce("no matrix copy"):
      let (ridx, cidx) = get_repeated_rows_cols(output, fs.input)
      let p1 = perm(fs.matrix, ridx, cidx)
      # echop1

  block fock_prob_nocopy_parallel:
    const dim = 10 # 26
    var fs = fockstate[float64](dim)
    let urand = sparsematrix(randomunitary[float](dim), toseq[0 ..< dim])
    fs.apply urand
    fs.input.photons.applyit(1)

    let output = fs.input
   
    timeonce("PARALLEL repeat_rows_cols usub"):
      let usub = fs.matrix.repeat_rows_cols(output, fs.input)
      let p = perm_parallel(usub)
      # echop
  
    timeonce("PARALLEL no matrix copy"):
      let (ridx, cidx) = get_repeated_rows_cols(output, fs.input)
      let p1 = perm_parallel(fs.matrix, ridx, cidx)
      # echop1
  

  block perm_benchmarks:
    const maxdim = 10 # 26

    for dim in 2 ..< maxdim:
      let u = randomunitary[float](dim)

      timeonce("perm " & $dim): discard perm(u)
      # timeonce("perm_PARALLEL" & $dim): discard perm_parallel(u)

    for dim in 2 ..< maxdim:
      let u = randomunitary[float](dim)

      # timeonce("perm" & $dim): discard perm(u)
      timeonce("perm_PARALLEL " & $dim): discard perm_parallel(u)

    for dim in 2 ..< maxdim:
      let u = randomunitary[float](dim)

      timeonce("perm_ADAPTIVE " & $dim): discard perm_adaptive(u)

  block hafnian_GBS_stats:
    const dim = 10
    var state = gaussianstate[float](dim)
    for i in 0 ..< dim:
      state.apply squeezer(0.2, i)
    # echo"vac prob ", state.vacuum_prob

    var cp = clickpattern(dim)
    cp.photons[0] = 2
    # echo"cp ", cp

    timeonce("f64"):
      let prob = state.pnr(cp)
    # echo"click prob ", prob

    let urand = randomunitary[float](dim)

    let urand_sparse = sparsematrix(urand, toseq[0 ..< dim])
    var state1 = state
    state1.apply urand_sparse
    var state2 = state
    state2.apply urand
    var state3 = state
    state3.apply(urand.oplus urand.conj)

    assert state1 == state2
    assert state2 == state3
    
  # test "maxHaf":
  #   echo "hi"

  #   const path = "/home/oli/qgot/qgot_data/data/maxhaf_data/starting_mat_24.mat"
  #   echo path

  #   var mat = readMatrix[Complex64](path)
  #   echo mat.rows, " ", mat.cols
  #   echo "matrix in\n", mat

  #   let evals = mat.eigenvalues
  #   echo "eigenvalues \n", evals

  #   let maxEval = evals.mapit(abs(it)).max
  #   mat *= 0.5 * (1.0 / maxEval)

  #   let A = oplus(mat, mat.conj)
  #   echo "mathcalA \n", A

  #   let X = Identity[Complex64](A.rows, A.cols).swapBlocks
    
  #   let fullIdent = Identity[Complex64](A.rows, A.cols)
  #   let m = 2.0 * (fullIdent - X * A).inverse() - fullIdent
  #   echo "m \n", m

  #   var state = gaussianState[float](mat.rows)
  #   state.covariance = m
  #   echo state.checkValid

  #   # echo m

suite "Gaussian Statistics":
  test "Squeezers":

    var state = gaussianstate[float](2)
    state.apply squeezer(0.1, 0)

    echo state.vacuumProb
    echo state.threshold(newThreshold([0,0]))
    echo state.pnr(clickPattern([0,0]))


  test "6 single mode squeezers":
    var state = gaussianState[float](6)

    let sqParams = [1.1, 1.1, 1.1, 1.2, 1.2, 1.2]
    for i, val in sqParams:
      state.apply squeezer(val, i)

    echo "vacuum prob {state.vacuumProb}".fmt
    # prints 0.03626597
    echo "all zeros click pnr {state.pnr(clickPattern([0,0,0,0,0,0]))}".fmt
    echo "2,0...,0,2 pnr prob {state.pnr(clickPattern([2,0,0,0,0,2]))}".fmt


  test "single mode squeezer number stats":

    var state = gaussianState[float](1)
    state.apply squeezer(1.1, 0)
    echo "vacuum prob {state.vacuumProb}".fmt
    for i in 0..4:
      echo "{i} prob {state.pnr(clickPattern([i]))}".fmt

